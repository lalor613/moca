<?php
require './administrador/config/bd.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $correo = filter_var($_POST['correo']);
    $contraseña = $_POST['contraseña'];
    $errores = '';
    if (empty($correo) or empty($contraseña)) {
        $errores = '<li> Llena todos los campos </li>';
    } else {
        try {
            $conn;
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
        $statement = pg_prepare($conn, "user_exist", "SELECT * FROM usuarios where correo= \$1 AND contraseña = \$2");
        $contraseña = hash('sha512', $contraseña);
        $statement = pg_execute($conn, "user_exist", array($correo, $contraseña));
        $result = pg_fetch_assoc($statement);
        print_r($result);
        if( $result !== False){
            session_start();
            $_SESSION ['correo'] = $correo;
            $buser = pg_prepare($conn, "tipo_user", "SELECT nombre_user, correo, tipo_id FROM usuarios WHERE correo = \$1 ");
            $buser = pg_execute($conn, "tipo_user", array($correo));
            $result2 = pg_num_rows($buser);
            if($result2 == 1){
                while($row = pg_fetch_assoc($buser)){
                    $_SESSION['nombre_user'] = $row['nombre_user'];
                    $_SESSION['correo'] = $row['correo'];
                    $_SESSION['tipo_id'] = $row['tipo_id'];
                }
            }

            if ($_SESSION['tipo_id'] > 0){
                header("Location: ./secciones/inicio.php");
            }else{
                header("Location: ./index.php");
            }

        } else{
            $errores.='<li> Datos incorrectos </li>';
        }
    }
}

?>
<?php $url = "http://" . $_SERVER['HTTP_HOST'] . "/moca/"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de Sesion</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilo_index.css">
</head>

<body class="fondo-blur">
    <br><br>
    <div class="container">
        
        <div class="row">

            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        INICIAR SESION
                    </div>
                    <div class="card-body">

                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" name="login">
                            <div class="form-group">
                                <label>Correo</label>
                                <input type="email" name="correo" class="form-control" placeholder="@unach.mx">
                            </div>
                            <div class="form-group">
                                <label>contraseña</label>
                                <input type="password" name="contraseña" class="form-control" placeholder="contraseña">
                            </div>

                            <button type="submit" class="btn btn-primary m-2" onclick="login.submit()">Ingresar</button>
                            <?php if (!empty($errores)) : ?>
                                <p class="form-text text-muted">
                                    <?php echo $errores; ?>
                                </p>
                            <?php endif; ?>

                        </form>
                    </div>
                    <a class="form-text text-muted align-self-center" style="color:black;" href="<?php echo $url; ?>restablecer.php">Restablecer contraseña</a>
                    <p class="form-text text-muted align-self-center">
                        ¿Aún no tienes cuenta?
                    </p>
                    <small class="text-muted align-self-center">Comunicate a con tu administrador de la DGDSE</small>
                    <br>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<?php
if(isset($_GET['email']) && isset($_GET['token'])){
   $email = $_GET['email'];
   $token = $_GET['token'];
}else{
    header("Location: ./index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restablecer contraseña</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <br><br>
    <div class="container">
        <div class="row">

            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Ingresa correo para enviar código</h4>
                    </div>
                    <div class="card-body">
                        <form action="./administrador/secciones/verificar_token.php" method="POST">
                            <div class="form-group p-3">
                                <label for="codigo">Codigo</label>
                                <input type="number" class="form-control" name="codigo" id="codigo" aria-describedby="emailHelpId" placeholder="Ingresa El código">
                                <small id="emailHelpId" class="form-text text-muted">Este codigo fue enviado a tu correo UNACH</small>

                                <input type="email" hidden name="correo" id="correo" value="<?php echo $email; ?>">
                                <input type="text" hidden name="token" id="token" value="<?php echo $token; ?>">
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Restablecer</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</body>

</html>
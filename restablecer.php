<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restablecer contraseña</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <br><br>
    <div class="container">
        <div class="row">

            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Correo para restablecer la contraseña</h4>
                    </div>
                    <div class="card-body">
                        <form action="./administrador/secciones/restablecer.php" method="POST">
                            <div class="form-group p-3">
                                <label for="correo">Correo</label>
                                <input type="email" class="form-control" name="correo" id="correo" aria-describedby="emailHelpId" placeholder="Ingresa correo UNACH">
                                <small id="emailHelpId" class="form-text text-muted">Este correo debe estar dado de alta</small>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Restablecer</button>
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</body>

</html>
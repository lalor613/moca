<?php
require("../config/bd.php");
$email = $_POST['correo'];
$token = $_POST['token'];
$codigo = $_POST['codigo'];

$res = pg_query($conn, "SELECT * FROM passwords WHERE email='$email' and token='$token' and codigo='$codigo' ");
$verificar = False;

if (pg_num_rows($res) > 0) {
    $fila = pg_fetch_row($res);
    $fecha = $fila[4];
    $fecha_actual = date("Y-m-d h:m:s");
    $second = strtotime($fecha_actual) - strtotime($fecha);
    $minutos = $second / 60;
    if ($minutos  > 10) {
        echo "Token vencido";
    } else {
        echo "Todo correcto";
    }
    $verificar = True;
} else {
    $verificar = False;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restablecer contraseña</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
</head>

<body>
    <br><br>
    <div class="container">
        <div class="row">

            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Restablecer contraseña</h4>
                    </div>
                    <div class="card-body">
                        <?php if($verificar){ ?>
                        <form action="./cambiar_pw.php" method="POST">
                            <div class="form-group p-3">
                                <label for="password">Nueva contraseña</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Ingresa la nueva contraseña">                                
                            </div>
                            <div class="form-group p-3">
                                <label for="password">Confirmar contraseña</label>
                                <input type="password" class="form-control" name="password2" id="password2" placeholder="Repita la contraseña">                                
                            </div>
                            <input type="email" name="correo" id="correo" hidden value="<?php echo $email ;?>">
                            <button type="submit" class="btn btn-primary btn-sm">Restablecer</button>
                        </form>
                        <?php }else{ ?>
                            <div class="alert alert-danger" role="alert">
                                <strong>Código Incorrecto o vencido</strong>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</body>

</html>
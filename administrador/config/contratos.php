<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';


if(isset($_GET['crear'])){
    $data = json_decode(file_get_contents("php://input"));
    $ciclo = $data->ciclo;
    $depe = $data -> depe;
    $tipo = $data-> tipo;
    $doc = $data->doc;
    $id = null;

    $insert = pg_prepare($conn,"insert_cont", "INSERT INTO contratos(c_ciclo, c_depe, c_tipo, c_doc) VALUES ($1,$2,$3,$4) ");
    if($ciclo and $doc and $tipo){
        $query = pg_query($conn,"SELECT * FROM contratos where c_doc='$doc' and c_ciclo='$ciclo' and c_tipo='$tipo' ");
        $result = pg_fetch_assoc($query);
        if($result == False){
            $insert = pg_execute($conn,"insert_cont",array($ciclo, $depe, $tipo, $doc));
            $consul = pg_query($conn,"SELECT id_cont FROM contratos where c_doc='$doc' and c_ciclo='$ciclo' and c_tipo='$tipo'");
            while($row = pg_fetch_assoc($consul)){
                $id = $row['id_cont'];
            }
            $return = file_get_contents("http://" . $_SERVER['HTTP_HOST']."/moca/administrador/config/api_contratos.php?period=".$ciclo."&faculty=".$depe."&teacher=".$doc."&type=".$tipo."&id=".$id);
            echo json_encode(["success"=>1]);
            
        }
    }
    exit();
}


if(isset($_GET['obtener'])){
    $data = json_decode(file_get_contents("php://input"));
    $depe = $data -> depe;
    $ciclo = $data -> ciclo;
    $query = pg_query($conn, "SELECT * FROM vw_cont_doctipo where c_depe='$depe' and c_ciclo='$ciclo' order by nombre ");
    if(pg_num_rows($query)>0){
        $cont = pg_fetch_all($query,PGSQL_ASSOC);
        echo json_encode($cont);
    }else{
        echo json_encode(["success"=>0]);
    }
}

if(isset($_GET['mostrar'])){
    $data = json_decode(file_get_contents("php://input"));
    $doc = $data -> doc;

    $query = pg_query($conn, "SELECT * FROM vw_cont_doctipo WHERE id='$doc' ");
    if(pg_num_rows($query)>0){
        $cont = pg_fetch_all($query,PGSQL_ASSOC);
        echo json_encode($cont);
    }else{
        echo json_encode(["success"=>0]);
    }
}

if( $_SERVER['REQUEST_METHOD'] =='DELETE' )
{
    $ciclo = null;
    $depe = null;
    $tipo = null;
    $doc = null;

    $cont = $_GET['borrar'];

    $sel = pg_query($conn, "SELECT * FROM contratos where id_cont= $cont");
    while ($row = pg_fetch_assocw($sel))
    {
        $ciclo = $row['c_ciclo'];
        $depe = $row['c_depe'];
        $doc = $row['c_doc'];
        $tipo = $row['c_tipo'];
    }

    $return = file_get_contents("http://" . $_SERVER['HTTP_HOST']."/moca/administrador/config/api_delcontratos.php?period=".$ciclo."&faculty=".$depe."&teacher=".$doc."&type=".$tipo);

    $query = pg_query($conn, "DELETE FROM contratos where id_cont = $cont ");
    echo json_encode(['success'=>1]);
}

?>
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if (isset($_GET['ciclo']) and (isset($_GET['depe'])) ) {
    $data = json_decode(file_get_contents("php://input"));
    $ciclo = $_GET['ciclo'];
    $depe = $_GET['depe'];
    //aqui va la VISTA PARA VER A LOS DOCENTES DEPENDIENDO DEL LA ASDCRIPCION SELECCIONADA
    $query = pg_query($conn, "SELECT * FROM vw_ads_doc WHERE idciclo='$ciclo' AND clave_depe = '$depe' ORDER BY nombre asc ");
    if (pg_num_rows($query)) {
        $ver = pg_fetch_all($query, PGSQL_ASSOC);
        echo json_encode($ver);
    } 
}

if(isset($_GET['obtener'])){
    $data = json_decode(file_get_contents("php://input"));
    $doc = $data -> doc;

    $query = pg_query($conn,"SELECT * FROM vw_ads_doc WHERE id='$doc' ");
    if(pg_num_rows($query)>0){
        $cont = pg_fetch_all($query,PGSQL_ASSOC);
        echo json_encode($cont);
    }else{
        echo json_encode(["success"=>0]);
    }
}

if(isset($_POST['id']) && isset($_POST['ciclo_esc']) && isset($_POST['depe_ua'])  )
{
    $id = $_POST['id'];
    $ciclo = $_POST['ciclo_esc'];
    $depe = $_POST['depe_ua'];

    $query = pg_query($conn,"SELECT * FROM vw_ads_doc where id_doc=$id and idciclo =$ciclo and clave_depe=$depe ");

    $result =  array();
    while($row = pg_fetch_assoc($query))
    {
        $result = $row;
    }
    echo json_encode($result);
}
else
{
    $result['status'] = 200;
    $result['message'] = "Dato Incorrecto, docente no encontrado";
}

if( isset($_POST['id_update']) && isset($_POST['ciclo_update']) && isset($_POST['depe_update'])  )
{
    $id = $_POST['id_update'];
    $ciclo = $_POST['ciclo_update'];
    $depe = $_POST['depe_update'];
    $periodo_ini = $_POST['periodo_ini'];
    $periodo_fin = $_POST['periodo_fin'];


    $query = pg_query($conn,"UPDATE adscripcion SET periodo_ini='$periodo_ini', periodo_fin='$periodo_fin' 
                                WHERE id_doc=$id and clave_depe=$depe and idciclo=$ciclo ");
    echo json_encode(["success" => 1]);

}

?>

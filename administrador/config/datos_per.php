<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';


if(isset($_POST['id'])){
    $id = $_POST['id'];
    $query= pg_query($conn,"SELECT * FROM d_per WHERE d_id='$id' ");
    $result = array();
    while($row = pg_fetch_assoc($query)){
        $result = $row;
    }
    echo json_encode($result);
}else{
    $result['status'] = 200;
    $result['message'] = "No se encuentra el docente";
}

if(isset($_POST['id_update'])){
    $id = $_POST['id_update'];
    $curp = $_POST['curp'];
    $nacionalidad = $_POST['nacionalidad'];
    $cp = $_POST['cp'];
    $colonia = $_POST['colonia'];
    $calle = $_POST['calle'];
    $ciudad = $_POST['ciudad'];

    function fecha_de_nacimiento_desde_curp($var){
        //primero validamos la curp
        if($var){
            $la_curp = substr($var, 4, 6); 
            $anio=substr($la_curp,0,2);
            $mes=substr($la_curp,2,2);
            $dia=substr($la_curp,4,2);
    
            if($anio >= 50){
                $fecha_de_nacimiento = $dia."-".$mes."-19".$anio;
            }else{
                $fecha_de_nacimiento = $dia."-".$mes."-20".$anio;
            }
            
            return calcula_edad($fecha_de_nacimiento);
        }else{
            return null;
        }
    }
    
    function calcula_edad($fecha_de_nacimiento){
        $hoy = date_create(date("Y-m-d"));
        $fecha_curp = date_create($fecha_de_nacimiento);
        $diff = date_diff($fecha_curp,$hoy);
        return $diff->format('%y');
    }
    
    $edad = fecha_de_nacimiento_desde_curp($curp);


    $update = pg_prepare($conn,"update_doc","UPDATE d_per SET curp=$2, nacionalidad=$3, cp=$4, colonia=$5, calle=$6, ciudad =$7, edad =$8 WHERE d_id=$1 ");
    if($id){
        $query = pg_query($conn,"SELECT * FROM d_per WHERE d_id ='$id' ");
        $result = pg_fetch_assoc($query);
        if($result == False){
            $insert = pg_query($conn,"INSERT INTO d_per (d_id, curp, nacionalidad, cp, colonia, calle, ciudad, edad) VALUES ('$id', '$curp', '$nacionalidad','$cp', '$colonia', '$calle', '$ciudad','$edad') ");
            echo json_encode(["success"=>1]);
        }else{
            $update = pg_execute($conn,"update_doc", array($id, $curp, $nacionalidad, $cp, $colonia, $calle, $ciudad,$edad));
            echo json_encode(["success"=>1]);
        }
    }
}

if(isset($_GET['mostrar'])){
    $data = json_decode(file_get_contents("php://input"));
    $doc = $data -> doc;

    $query = pg_query($conn,"SELECT * FROM d_per WHERE d_id='$doc' ");
    if(pg_num_rows($query)>0){
        $cont = pg_fetch_all($query,PGSQL_ASSOC);
        echo json_encode($cont);
    }else{
        echo json_encode(["success"=>0]);
    }
}

?>
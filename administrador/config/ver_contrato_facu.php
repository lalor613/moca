<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, PATCH, DELETE');
header("Allow: GET, POST, OPTIONS, PUT, PATCH , DELETE");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('Content-Type: application/json');


require "../../libreria/vendor/autoload.php";
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;


$respuesta = [
    "success" => false,
    "data" => [],
    "message" => ""
];

$dependencia = $_GET['faculty'];
$ciclo = $_GET['period'];


if( isset($dependencia) && isset($ciclo)){

    $combinado = new Merger;
    $ubic = "contratos/".$ciclo."/".$dependencia."/";
    $carpeta = @scandir($ubic);

    $ubic2 = "cedulas/".$ciclo."/".$dependencia."/";
    $carpeta2 = @scandir($ubic2);


    if( $carpeta == True )
    {
        $directorio = opendir($ubic);
        while ($elemento = readdir($directorio)) {
            if (!is_dir($ubic . $elemento)) {
                $respuesta['success'] = 1;
                // echo $elemento;
                $combinado->addFile($ubic . $elemento);
            }
            else
            {
                $respuesta['message'] = "Error";
            }
        }

        $direcotrio2 = opendir($ubic2);
        while ($row = readdir($direcotrio2)){
            if(!is_dir($ubic2.$row)){
                $combinado->addFile($ubic2.$row);
            }
        }

        $createdPdf = $combinado->merge();
    
        $nombreArchivo = "combinado_" . $dependencia . $ciclo . ".pdf";
    
        header("Content-type:application/pdf");
        header("Content-Disposition: inline; filename=$nombreArchivo");
        header("content-Transfer-Encoding:binary");
        header("Accept-Ranges:bytes");
    
        echo $createdPdf;
    
        exit;
    }
    else
    {
        $respuesta['message'] = "sin elementos que mostrar";
    }
}

?>
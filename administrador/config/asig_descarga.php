<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if ( isset($_GET['insertar'])){
    $data = json_decode(file_get_contents("php://input"));
    $id_desc = $data -> id_desc;
    $id_cont = $data -> id_cont;

    $id_doc = $data -> id_doc;
    $id_ciclo = $data -> id_ciclo;
    $id_depe = $data -> id_depe;
    $id_tipo = $data -> id_tipo;


    $insert = pg_prepare($conn,"asig_desccont", "INSERT INTO asig_descarga (id_descarga, id_contrato) VALUES ($1,$2)");
    if ($id_desc and $id_cont){
        $desc_bus = pg_query($conn, "SELECT * FROM asig_descarga WHERE id_descarga='$id_desc' AND id_contrato='$id_cont' ");
        $result = pg_fetch_assoc($desc_bus);
        if($result == False){
            $insert = pg_execute($conn, "asig_desccont", array($id_desc, $id_cont));
            $return = file_get_contents("http://" . $_SERVER['HTTP_HOST']."/moca/administrador/config/api_contratos.php?period=".$id_ciclo."&faculty=".$id_depe."&teacher=".$id_doc."&type=".$id_tipo."&id=".$id_cont);
            echo json_encode(["success"=>1]);
        }
    }
}

if(isset($_GET['desc_asig'])){
    $data = json_decode(file_get_contents("php://input"));
    $id_cont= $data -> id_cont;

    $desc = pg_query($conn, "SELECT * FROM vw_desc_asig where id_contrato='$id_cont'  ");
    if (pg_num_rows($desc)>0){
        $ver = pg_fetch_all($desc, PGSQL_ASSOC);
        echo json_encode($ver);
    }else{
        echo json_encode(["success"=>0]);
    }
}

if (isset($_GET['delete_asig'])){
    $data = json_decode(file_get_contents("php://input"));
    $id_desc = $data -> id_desc;
    $id_cont = $data -> id_cont;

    $delete = pg_query($conn, "DELETE FROM asig_descarga WHERE id_descarga='$id_desc' AND id_contrato='$id_cont' ");
    echo json_encode(["success"=>1]);
}

?>
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if (isset($_GET['insertar'])) {
    $data = json_decode(file_get_contents("php://input"));
    $nombre = $data-> nombre;
    $puesto = $data-> puesto;
    $f_ocupar = $data->f_ocupar;
    $titulo = $data->titulo;
    $genero = $data->genero;
    $clave_dir = $data->clave_dir;

    // $puesto = mb_convert_encoding(mb_convert_case($puesto, MB_CASE_TITLE,"UTF-8"),"UTF-8");

    $nombre = mb_convert_encoding($nombre,"UTF-8");
    // $nombre = strtolower($nombre);

    $insert = pg_prepare($conn, "insertar_dir", "INSERT INTO directivos (nombre, puesto, f_ocupar, titulo, genero, clave_dir) VALUES($1,$2,$3,$4,$5,$6)");
    if ($clave_dir) {
        $dire_bus = pg_query($conn, "SELECT clave_dir FROM directivos WHERE clave_dir='$clave_dir' ");
        $result = pg_fetch_assoc($dire_bus);
        if ($result == False) {
            $insert = pg_execute($conn, "insertar_dir", array($nombre, $puesto, $f_ocupar, $titulo, $genero, $clave_dir));
            echo json_encode(["success" => 1]);
        }
    }
}

//buscar los directivos
if ($_SERVER["REQUEST_METHOD"] == 'GET') {

    $dir_depe = pg_query($conn, "SELECT * FROM vw_depe_dir");
    if (pg_num_rows($dir_depe) > 0) {
        $direc = pg_fetch_all($dir_depe, PGSQL_ASSOC);
        echo json_encode($direc);
    } else {
        echo json_encode(["success" => 0]);
    }
}

if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $query = pg_query($conn, "SELECT * FROM directivos WHERE id_dir = '$id' ");
    $result = array();
    while ($row = pg_fetch_assoc($query)) {
        $result = $row;
    }
    echo json_encode($result);
} else {
    $result['status'] = 200;
    $result['message'] = "Dato Incorrecto, docente no encontrado";
}

if(isset($_POST['id_update'])){
    $id = $_POST['id_update'];
    $nombre = $_POST['nombre'];
    $puesto = $_POST['puesto'];
    $f_ocupar = $_POST['f_ocupar'];
    $titulo = $_POST['titulo'];
    $genero = $_POST['genero'];

    $query = pg_query($conn, "UPDATE directivos SET nombre='$nombre', puesto='$puesto', f_ocupar='$f_ocupar', titulo = '$titulo', genero='$genero' where id_dir='$id' ");
    echo json_encode(["success"=>1]);
}else {
    $result['status'] = 200;
    $result['message'] = "Dato Incorrecto, docente no encontrado";
}

if($_SERVER["REQUEST_METHOD"] == 'DELETE'){
    $id = $_GET['id_del'];

    $query = pg_query($conn, "DELETE FROM directivos WHERE id_dir='$id' ");
    echo json_encode(["success"=>1]);
}

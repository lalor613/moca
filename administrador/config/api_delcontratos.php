<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, PATCH, DELETE');
header("Allow: GET, POST, OPTIONS, PUT, PATCH , DELETE");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('Content-Type: application/json');

$respuesta = [
    "success" => false,
    "data" => [],
    "message" => ""
];


$dependencia = $_GET['faculty'];
$ciclo = $_GET['period'];
$docente = $_GET['teacher'];
$tipo = $_GET['type'];


if( isset($dependencia) && isset($ciclo) && isset($docente) && isset($tipo) ){
    $respuesta['success'] = 1;
    $coteja = ($tipo == 1) ? "" : "interino";

    if(file_exists("../config/contratos/".$ciclo."/".$dependencia."/contrato_".$coteja.$docente."_".$ciclo.$dependencia.".pdf")){
       if(unlink("../config/contratos/".$ciclo."/".$dependencia."/contrato_".$coteja.$docente."_".$ciclo.$dependencia.".pdf")){
        echo "el archivo se ha eliminado";
       }else{
        echo "error";
       }
    }else{
        echo "No existe archivo";
    }
    //#################################
    if(file_exists("../config/cedulas/".$ciclo."/".$dependencia."/cedula_".$docente.".pdf")){
        unlink("../config/cedulas/".$ciclo."/".$dependencia."/cedula_".$docente.".pdf");
    }else{
        echo "No existe archivo";
    }
}else{
    $respuesta["message"]  =   "Faltan parametros en la petición";
    http_response_code(400);
    echo json_encode($respuesta);
    exit;
}


?>
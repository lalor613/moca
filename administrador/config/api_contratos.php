<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, PATCH, DELETE');
header("Allow: GET, POST, OPTIONS, PUT, PATCH , DELETE");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('Content-Type: application/json');


$dependencia = $_GET['faculty'];
$ciclo = $_GET['period'];
$docente = $_GET['teacher'];
$tipo = $_GET['type'];
$id_contrato = $_GET['id'];

if( isset($dependencia) && isset($ciclo) && isset($docente) && isset($tipo) ){
    $coteja = ($tipo == 1) ? "" : "interino";


    include("generaPDF".$coteja.".php");
}


?>
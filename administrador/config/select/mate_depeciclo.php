<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require '../bd.php';


if (isset($_GET['ciclo']) and (isset($_GET['depe']))) {
    $data = json_decode(file_get_contents("php://input"));
    $ciclo = $_GET['ciclo'];
    $depe = $_GET['depe'];
    
    $query = pg_query($conn, "SELECT id, asignatura, semestre, grupo, total FROM asignaturas WHERE asig_depe='$depe' AND asig_ciclo='$ciclo' EXCEPT SELECT id_asig, asignatura, semestre, grupo, total FROM vw_doc_mate ORDER BY semestre ASC");    
    if (pg_num_rows($query)) {
        $ver = pg_fetch_all($query, PGSQL_ASSOC);
        echo json_encode($ver, JSON_UNESCAPED_UNICODE);
    }else{
        echo json_encode(["success" => "No hay materias vacantes"]);
    }
}

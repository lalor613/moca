<?php
require '../bd.php';

if( (isset($_POST['ciclo'])) AND (isset($_POST['depe'])) ){
    $sql = "SELECT * FROM vw_ads_doc_cortado WHERE idciclo=".$_POST['ciclo']. " AND clave_depe=".$_POST['depe']." AND categoria like '%ASIGNATURA%' ORDER BY apellido" ;
    $result = pg_query($conn, $sql);
    $cadena = "<label class='btn btn-secondary'> Docentes 
                <select class='form-control-sm form-control js-example-basic-single' name='doc' id='doc'>
                    <option value=0 selected>--Elige un Docente-- </option>";
                    while($row = pg_fetch_array($result)){
                        if( $row['ht'] > 0 || $row['hd'] > 0 ){
                            $cadena=$cadena."<option value=".$row['id'].">".$row['apellido']." ".$row['nombre']." - HD:".$row['ht']." - HT:".$row['ht']."</option>";
                        }
                        else{
                            $cadena=$cadena."<option value=".$row['id'].">".$row['apellido']." ".$row['nombre']."</option>";
                        }
                    }
    echo $cadena."</select> </label>";
}
?>
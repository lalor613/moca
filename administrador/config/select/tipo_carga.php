<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require '../bd.php';


if (isset($_POST['doc']) && isset($_POST['depe']) && isset($_POST['ciclo'])) {
    $id = $_POST['doc'];
    $depe = $_POST['depe'];
    $ciclo = $_POST['ciclo'];
    $hi = 0;
    $htd = 0;
    $result = [
        'htd' => 0,
        'hi' => 0
    ];
    $query = pg_query($conn, "SELECT * FROM vw_doc_mate WHERE id_doc=$id and asig_depe=$depe and asig_ciclo=$ciclo");

    while ($row = pg_fetch_assoc($query)) {
        if ($row['id_tipocarga'] == 1) {
            $result['htd'] += $row['total'];
            
        } elseif ($row['id_tipocarga'] == 2) {
            $result['hi'] += $row['total'];
        }
    }
    
    echo json_encode($result);
} else {
    $result['status'] = 200;
    $result['message'] = "se encuentra el docente";
}

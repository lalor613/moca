<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if(isset($_GET['insertar'])){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data ->id;
    $gen = $data ->genero;
    $nombre = $data->nombre;
    $apellido = $data->apellido;
    $plaza = $data->plaza;
    $sindicato = $data ->sindicato;
    $hd_contrato = $data->hd_contrato;
    $ht_contrato = $data->ht_contrato;
    $cate = $data->categoria;
    $hd = $data->hd;
    $ht = $data->ht;
    $htd = $data->htd;
    $hi = $data -> hi;
    $grado_acad = $data->grado_acad;

    $espacios = array("  ");
    $sustituto = array(" ");
    $categoria = str_replace($espacios,$sustituto,$cate);

    $doc_ciclo=$data->doc_ciclo;
    $doc_depe = $data->doc_depe;


    if ($gen == 'M'){
        $genero = "Masculino";
    }elseif($genero = 'F'){
        $genero = "Femenino";
    }else{
        $genero='No especificado';
    }

    $ultimo = substr($doc_ciclo,-1);
    $serie = substr($doc_ciclo,0,4);
    if($hd > 0 or $ht > 0){
        if($ultimo == 1){
            $periodo_ini = $serie."-01-01";
            $periodo_fin = $serie."-07-31";
        }if($ultimo==2){
            $periodo_ini = $serie."-08-01";
            $periodo_fin = $serie."-12-31";
        }
    }else{
        if($ultimo == 1){
            $periodo_ini = $serie."-01-01";
            $periodo_fin = $serie."-06-15";
        }if($ultimo==2){
            $periodo_ini = $serie."-08-01";
            $periodo_fin = $serie."-12-15";
        }
    }
    $docente = pg_query($conn, "SELECT * FROM docentes WHERE id='$id' ");
    $resultado = pg_fetch_assoc($docente);

    $adscripcion = pg_query($conn, "SELECT * FROM adscripcion WHERE clave_depe='$doc_depe' AND idciclo='$doc_ciclo' AND id_doc='$id' ");
    $result_ads = pg_fetch_assoc($adscripcion);
    //prepara la insercion de docentes
    $insertar = pg_prepare($conn,"insert_docentes", "INSERT INTO docentes(id, genero,nombre, apellido, plaza, sindicato, hd_contrato, ht_contrato, categoria, hd, ht, htd, hi, grado_acad) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)");
    
        //si el id docente no se encuentra en la BD
        if($resultado == False){ 
            //Ingresa al nuevo docente
            $insertar = pg_execute($conn,"insert_docentes", array($id, $genero, $nombre, $apellido,$plaza, $sindicato, $hd_contrato, $ht_contrato, $categoria, $hd, $ht, $htd, $hi, $grado_acad));
            //lo añade al ciclo escolar y unidad académica seleccionados
            $ads_ins = pg_query($conn, "INSERT INTO adscripcion (clave_depe, idciclo, id_doc, periodo_ini, periodo_fin) VALUES ('$doc_depe', '$doc_ciclo', '$id', '$periodo_ini', '$periodo_fin')");
            echo json_encode(["success"=>"se añadio el docente"]);
        // En caso de que este adscrito a la misma unidad academica y cilo escolar se corrigen los datos
        }elseif($result_ads == True){
            $query = pg_query($conn, "UPDATE docentes SET nombre='$nombre', apellido='$apellido', genero='$genero' WHERE id='$id' ");
            echo json_encode(["success"=>"se corrigieron los datos"]);
        //en caso de que no esté en la escuela y ciclo escolar seleccionados "corrige"
        }elseif($result_ads == False){
            $ads_ins = pg_query($conn, "INSERT INTO adscripcion (clave_depe,idciclo, id_doc, periodo_ini, periodo_fin) VALUES ('$doc_depe', '$doc_ciclo', '$id', '$periodo_ini', '$periodo_fin') ");
            echo json_encode(["success"=>"se añadio al ciclo escolar: ".$doc_ciclo]);
        }else{
        echo "Qué hay de nuevo?, viejo";
     }
    exit();
}

if(isset($_GET['borrar'])){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data ->id;

    $doc_ciclo=$data->doc_ciclo;
    $doc_depe = $data->doc_depe;

    $query = pg_query($conn, "DELETE FROM adscripcion WHERE id_doc = '$id' and clave_depe='$doc_depe' and idciclo='$doc_ciclo' ");

    if($query)
    {
        echo json_encode(['success'=>true]);
    }
}

<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if( (isset($_GET['vacante'])) ){
    $data = json_decode(file_get_contents("php://input"));
    $depe = $data -> depe;
    $ciclo = $data-> ciclo;
    $query = pg_query($conn, "SELECT id, asignatura, semestre, grupo, plan_est, total FROM asignaturas WHERE asig_depe='$depe' AND asig_ciclo='$ciclo' EXCEPT SELECT id_asig, asignatura, semestre, grupo, plan_est ,total FROM vw_doc_mate ORDER BY semestre ASC");    
    $result = pg_fetch_all($query,PGSQL_ASSOC);
    echo json_encode($result,JSON_UNESCAPED_UNICODE);
}


// print_r(json_decode($_POST["materias"],TRUE));
// $data = [];
if(isset($_GET['cambio'])){
    $data = [];
    foreach($_POST['materias'] as $id){
        $query = pg_query($conn, "SELECT id, asignatura, semestre, grupo, plan_est, total FROM asignaturas WHERE id=$id");
        while($row = pg_fetch_all($query,PGSQL_ASSOC))
        {
            $result =[
            "id" => $row['id'],
            "asignatura" => $row['asignatura'],
            "semestre" => $row['semestre']
            ];
            array_push($data, $result);
        }     
        
    }
    // echo json_encode($_POST['materias']);
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
    
}



// if(isset($_GET['cambio'])){
//     $data = $_POST['materias'];
//     var_dump($data);
// }


?>
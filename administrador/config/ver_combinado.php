<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, PATCH, DELETE');
header("Allow: GET, POST, OPTIONS, PUT, PATCH , DELETE");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('Content-Type: application/json');

require 'bd.php';
require "../../libreria/vendor/autoload.php";
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;


$respuesta = [
    "success" => false,
    "data" => [],
    "message" => ""
];

$dependencia = $_GET['faculty'];
$ciclo = $_GET['period'];
$docente = $_GET['teacher'];
$tipo = $_GET['type'];

$query = pg_query($conn, "SELECT nombre, apellido FROM docentes WHERE id = $docente");
while ($row = pg_fetch_assoc($query)){
    $nombreArchivo = $row['apellido']."_".$row['nombre']."contrato_".$coteja."_".$ciclo.$dependencia.".pdf";
}

if( isset($dependencia) && isset($ciclo) && isset($docente) && isset($tipo)){
    $coteja = ($tipo == 1) ? "" : "interino";
    $combinado = new Merger;
    $ubic = "../config/contratos/".$ciclo."/".$dependencia."/";
    $ubic2 = "../config/cedulas/".$ciclo."/".$dependencia."/";

    $nombre = "contrato_".$coteja.$docente."_".$ciclo.$dependencia.".pdf";
    $nombreced = "cedula_".$docente.".pdf";

    $carpeta = @scandir($ubic);
    if( $carpeta == True )
    {
        $directorio = opendir($ubic);
        if( file_exists($ubic.$nombre) ){
            $combinado ->addFile($ubic.$nombre);
        }
        if(file_exists($ubic2.$nombreced)){
            $combinado ->addFile($ubic2.$nombreced, new Pages('1'));
        }



        $createdPdf = $combinado->merge();

        header("Content-type:application/pdf");
        header("Content-Disposition: inline; filename=$nombreArchivo");
        header("content-Transfer-Encoding:binary");
        header("Accept-Ranges:bytes");
    
        echo $createdPdf;
    
        exit;
    }
    else
    {
        $respuesta['message'] = "sin elementos que mostrar";
    }
}

?>
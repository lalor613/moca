<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['doc_ciclo']) and (isset($_GET['depe']))) {
        $data = json_decode(file_get_contents("php://input"));
        $ciclo = $_GET['doc_ciclo'];
        $depe = $_GET['depe'];
        //aqui va la VISTA PARA VER A LOS DOCENTES DEPENDIENDO DEL LA ASDCRIPCION SELECCIONADA
        $query = pg_query($conn, "SELECT * FROM vw_ads_doc WHERE idciclo='$ciclo' AND clave_depe = '$depe' and categoria like '%ASIGNATURA%' ORDER BY apellido asc ");
        if (pg_num_rows($query)) {
            $ver = pg_fetch_all($query, PGSQL_ASSOC);
            echo json_encode($ver);
        } else {
              echo json_encode(["success"=>0]);
        }
    }
}


if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $query = pg_query($conn, "SELECT * FROM docentes WHERE id = '$id' ");
    $result = array();
    while ($row = pg_fetch_assoc($query)) {
        $result = $row;
    }
    echo json_encode($result);
} else {
    $result['status'] = 200;
    $result['message'] = "Dato Incorrecto, docente no encontrado";
}

if (isset($_POST['id_update'])) {
    $id = $_POST['id_update'];
    $genero = $_POST['genero'];
    $htd = $_POST['htd'];
    $hi = $_POST['hi'];
    $grado_acad = $_POST['grado_acad'];
    $cedula = $_POST['cedula'];
    $otorgado = $_POST['otorgado'];
    $f_otor = $_POST['f_otor'];

    $query = pg_query($conn, "UPDATE docentes SET genero='$genero', htd='$htd', hi='$hi', grado_acad='$grado_acad', cedula='$cedula', otorgado='$otorgado', f_otor='$f_otor' WHERE id ='$id' ");
    echo json_encode(["success" => 1]);
}

if(isset($_GET['mostrar'])){
    $data = json_decode(file_get_contents("php://input"));
    $doc = $data -> doc;

    $query = pg_query($conn,"SELECT * FROM docentes WHERE id='$doc' ");
    if(pg_num_rows($query)>0){
        $cont = pg_fetch_all($query,PGSQL_ASSOC);
        echo json_encode($cont);
    }else{
        echo json_encode(["success"=>0]);
    }
}
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require './bd.php';

if (isset($_GET['insertar'])) {
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;
    $asignatura = $data->asignatura;
    $semestre = $data->semestre;
    $grupo = $data->grupo;
    $subgrupo = $data->subgrupo;
    $hrs_t = $data->hrs_t;
    $hrs_p = $data->hrs_p;
    $id_doc = $data->id_doc;
    $total = $hrs_t + $hrs_p;
    $asig_depe = $data->asig_depe;
    $asig_ciclo = $data->asig_ciclo;
    $plan_est = $data->plan_est;
    $id_inte = $data->id_inte;

    $ins_asig = pg_prepare($conn, "insert_asig", "INSERT INTO asignaturas (id, asignatura, semestre, grupo, subgrupo, hrs_t, hrs_p, total,asig_depe, asig_ciclo, plan_est) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)");
    //  Busco que no estén vacios estas variables
    if ($id and $asig_depe and $asig_ciclo) {
        //Busca que la materia no esté en la BD
        $asig = pg_query($conn, "SELECT * FROM asignaturas WHERE id='$id' AND asig_depe='$asig_depe' AND asig_ciclo='$asig_ciclo' ");
        $r = pg_fetch_assoc($asig);

        if ($r == False) {
            $ins_asig = pg_execute($conn, "insert_asig", array($id, $asignatura, $semestre, $grupo, $subgrupo, $hrs_t, $hrs_p, $total, $asig_depe, $asig_ciclo, $plan_est));
            //Hace la carga dependiendo si es materia normal o interina
            if ($id_inte != NULL) {
                $insert = pg_query($conn, "INSERT INTO carga_asignaturas (id_doc,id_asig, id_tipocarga) VALUES ('$id_inte', '$id',2)");
                echo json_encode(["success" => 1]);
            } else {
                $insert = pg_query($conn, "INSERT INTO carga_asignaturas (id_doc,id_asig, id_tipocarga) VALUES ('$id_doc', '$id',1)");
                echo json_encode(["success" => 1]);
            }
            // $insert = pg_prepare($conn, "insert_tabla", "INSERT INTO carga_asignaturas (id_asig, id_doc) VALUES ($1, $2)");
            // $insert = pg_execute($conn, "insert_tabla", array($id, $id_doc));
        } else {
            if ($id_inte != NULL) {
                $insert = pg_query($conn, "INSERT INTO carga_asignaturas (id_doc,id_asig, id_tipocarga) VALUES ($id_inte, $id,2)");
                echo json_encode(["success" => 1]);
            } else {
                $insert = pg_query($conn, "INSERT INTO carga_asignaturas (id_doc,id_asig, id_tipocarga) VALUES ($id_doc, $id,1)");
                echo json_encode(["success" => 1]);
            }
        }
    }
    exit();
}


if (isset($_GET['borrar'])) {
    $data = json_decode(file_get_contents("php://input"));
    $id_mate = $data->id;
    $id_doc = $data->id_doc;
    $id_inte = $data->id_inte;

    $asig_depe = $data->asig_depe;
    $asig_ciclo = $data->asig_ciclo;

//     // $asig = pg_query($conn, "SELECT * FROM carga_asignaturas WHERE id_asig='$id' ");
//     // $r = pg_fetch_assoc($asig);
    $titu = is_null($id_inte) ? $id_doc : $id_inte;
//     // $docente = "DELETE FROM adscripcion WHERE id_doc='$titu' WHERE clave_depe = '$asig_depe' and idciclo='$asig_ciclo' ";

    $delete = pg_query($conn,"DELETE FROM carga_asignaturas WHERE id_asig=$id_mate and id_doc=$titu ");
//    
    if($delete){
        $delete_asig = pg_query($conn, "DELETE FROM asignaturas WHERE id='$id_mate' and asig_depe='$asig_depe' and asig_ciclo='$asig_ciclo' ");
        echo json_encode(["success" => true ]);
    }else{
        echo json_encode(["¿qué onda="]);
    }
//  

}



if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;
    $doc = $data->doc;
    $tipo = $data->tipo;

    $query = pg_query($conn, "DELETE FROM carga_asignaturas WHERE id_doc='$doc' AND id_asig='$id' AND id_tipocarga ='$tipo' ");
    echo json_encode(["success" => 1]);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;
    $doc = $data->doc;
    $tipo = $data->tipo;
    $query = pg_query($conn, "INSERT INTO carga_asignaturas(id_doc,id_asig,id_tipocarga) VALUES ('$doc','$id','$tipo')");
    echo json_encode(["success" => 1]);
}

<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if($_SERVER['REQUEST_METHOD']=='POST'){
    $data = json_decode(file_get_contents("php://input"));
    $ac_depe = $data->ac_depe;
    $ac_ciclo = $data->ac_ciclo;

    $insertar = pg_prepare($conn, "insert_acdep","INSERT INTO apertura_ciclos(ac_depe,ac_ciclo) VALUES($1,$2)");
    if($ac_depe and $ac_ciclo){
        $query = pg_query($conn,"SELECT * FROM apertura_ciclos WHERE ac_depe='$ac_depe' AND ac_ciclo='$ac_ciclo' ");
        $result = pg_fetch_assoc($query);
        if($result == False){
            $insertar = pg_execute($conn, "insert_acdep", array($ac_depe, $ac_ciclo));
            echo json_encode(["success"=>1]);
        }
    }
    exit();
}

if($_SERVER['REQUEST_METHOD']=='GET'){
    $depe_aper = pg_query($conn, "SELECT clave, direccion, id_ciclo FROM vw_depe_cicloesc ORDER BY id_ciclo DESC ");
    if(pg_num_rows($depe_aper)){
        $depe = pg_fetch_all($depe_aper, PGSQL_ASSOC);
        echo json_encode($depe);
    }else{
        echo json_encode(["success"=>0]);
    }
}

?>
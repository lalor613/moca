<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if ((isset($_GET['ciclo'])) and (isset($_GET['depe'])) and (isset($_GET['doc'])) and (isset($_GET['tipo']))){
    $data = json_decode(file_get_contents("php://input"));
    $doc = $_GET['doc'];
    $ciclo = $_GET['ciclo'];
    $depe = $_GET['depe'];
    $tipo = $_GET['tipo'];

    $query = pg_query($conn, "SELECT * FROM vw_doc_mate where asig_depe='$depe' AND asig_ciclo='$ciclo' AND id_doc='$doc' AND id_tipocarga='$tipo' ORDER BY semestre ASC ");
    if (pg_num_rows($query)>0) {
        $ver = pg_fetch_all($query, PGSQL_ASSOC);
        echo json_encode($ver);
    }else{
        echo json_encode(["success"=>0]);
    }
}

if (isset($_POST['id_mate'])){
    $id_mate = $_POST['id_mate'];

    $buscar_m = pg_query($conn, "SELECT * FROM asignaturas where id='$id_mate' ");
    $result = array();
    while ($row = pg_fetch_assoc($buscar_m)){
        $result = $row;
    }
    echo json_encode($result);
}

if(isset($_POST['id_update'])){
    $id_mate = $_POST['id_update'];
    $horas = $_POST['horas'];
    $update = pg_prepare($conn,"update_materia", "UPDATE asignaturas SET total=$2 WHERE id=$1 ");
    if($id_mate){
        $buscar = pg_query($conn,"SELECT * FROM vw_doc_mate WHERE id_asig='$id_mate' ");
        $update = pg_execute($conn, "update_materia",array($id_mate, $horas));
        echo json_encode(["success"=>1]);
    }else{
        echo json_encode(["success"=>0]);
    }
}
?>

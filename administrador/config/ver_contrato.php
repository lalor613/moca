<?php 

$respuesta = [
    "success" => false,
    "data" => [],
    "message" => ""
];


$_Faculty = $_GET['faculty'];
$_Ciclo = $_GET['period'];
$_Teacher = $_GET['teacher'];
$_Type = $_GET['type'];


if( isset($_Faculty) && isset($_Teacher) && isset($_Ciclo) && isset($_Type)){

    $coteja = ($_Type == 1) ? "" : "interino";

    if( file_exists("../config/contratos/".$_Ciclo."/".$_Faculty."/contrato_".$coteja.$_Teacher."_".$_Ciclo.$_Faculty.".pdf") ){
        header("Content-type:application/pdf");
        $nombreArchivo = "contrato_".$coteja.$_Teacher."_".$_Ciclo.$_Faculty.".pdf";
        header("Content-Disposition: inline; filename=$nombreArchivo");
        readfile("../config/contratos/". $_Ciclo."/".$_Faculty."/contrato_".$coteja.$_Teacher."_".$_Ciclo.$_Faculty.".pdf");
    }else{
        echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
}else{
    $respuesta["message"]  =   "Faltan parametros en la petición";
    http_response_code(400);
    echo json_encode($respuesta);
    exit;
}



?>
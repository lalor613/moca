<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if($_SERVER['REQUEST_METHOD']=='GET'){
    $query = pg_query($conn, "SELECT * FROM vw_desc_docmoti ORDER BY id_descciclo DESC");
    if (pg_num_rows($query)>0){
        $desc = pg_fetch_all($query, PGSQL_ASSOC);
        echo json_encode(($desc));
    } else {
        echo json_encode(["success"=>0]);        
    }
}

if($_SERVER['REQUEST_METHOD']=='POST'){
    $data = json_decode(file_get_contents("php://input"));
    $depe = $data->depe;
    $ciclo = $data ->ciclo;
    $doc = $data->doc;
    $moti = $data->moti;

    $insert = pg_prepare($conn,"insert_desc", "INSERT INTO descargas (id_descdoc, clave_descdepe, id_descciclo, id_moti) VALUES ($1,$2,$3,$4)");
    if($depe and $ciclo and $doc){
        $query = pg_query($conn,"SELECT * FROM descargas where id_descciclo='$ciclo' and clave_descdepe='$depe' and id_descdoc='$doc' ");
        $result = pg_fetch_assoc($query);
        if($result == False){
            $insert = pg_execute($conn, "insert_desc", array($doc,$depe,$ciclo,$moti));
            echo json_encode(["success"=>1]);
        }
    }
    exit();
    
}


if($_SERVER["REQUEST_METHOD"]=='DELETE'){
    $id= $_GET['id_del'];

    $query = pg_query($conn,"DELETE FROM descargas where id_desc='$id' ");
    echo json_encode(["success"=>1]);
}

?>

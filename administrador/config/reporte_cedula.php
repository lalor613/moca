<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte contrato</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style type="text/css">
        @page {
            margin: 0cm 1cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
            font-size: 16px;

            text-align: justify;
        }

        @media print {}

        body {
            margin-top: 1.5cm;
            margin-left: 2cm;
            margin-right: 3rem;
            margin-bottom: 2cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        .fechas{
            /* display: flex; */
            text-align: right;
 
        }
        .hijo1{
            margin: 0;
        }
        .hijo2{
            margin-top:auto;
        }
        .director{
            text-align: left;
            height: auto;
            font-weight: bold;
            line-height: 1em;
            width: 60%;
            font-size: 14px;
        }
        div.contenedor4{
            display: flex;
            position:relative;
            margin-top: 2rem;
            margin-bottom: auto;
            align-items: center;
            align-self: center;
            /* border: 1px solid black; */
            /* align-self: flex-start; */
            /* background-color: #eeeeee; */
            font-size: 14px;
            height: auto;
            page-break-inside: avoid;
        }
        div.hijo4{
            line-height: 1em;
            height: 50%;
            width: 60%;
            align-self: center;
            text-align: center;
            margin:0px auto;
            /* border: 1px solid black; */
        }
    </style>
</head>

<body>
   <?php include("dataced.php"); ?>
    <header>
        <div class="fechas">
            <!-- <p class="hijo1">Tuxtla Gutiérrez, Chiapas;</p> -->
            <p class="hijo2"><?php echo "a ".fechaEs($periodo_ini) ?></p>
            <br><br>
        </div>
  

    </header>

    <main>
        <div class="director">
            <p>
                <?php echo strtoupper($titulodirec) . ". " . mb_strtoupper($nombredirec, "UTF-8"); ?> <br>
                <?php echo mb_strtoupper($puestodirec);?> EN <?php echo mb_strtoupper(gen_depe($dirdir)); ?> <br>
                P R E S E N T E
            </p>
        </div>
        <br><br>
        <div>
            <p>
                Quien suscribe, <?php echo nombres_compuestos($nombredoc) . " " . nombres_compuestos($apellidodoc); ?>, en mi carácter de <?php echo $categoria; ?>,
                con identificación oficial con fotografía expedida por el Instituto Nacional Electoral anexa,
                expreso mi compromiso para entregar copia simple de mi cédula de <?php echo titulos_tipo($grado_acad); ?>, 
                que acredita mis estudios <?php echo titulos_cedula($grado_acad); ?>, antes del <?php echo fechaEs($periodo_fin); ?>, con la finalidad de integrar
                mi expediente y para comprobar que cuento con los conocimientos profesionales para ejercer satisfactoriamente
                las actividades de docencia  dentro de esta Institución Educativa.
            </p>
            <p>
                Sin más por el momento, agradezco la atención al presente oficio.
            </p>
        </div>
        <br><br><br>
        <div class="contenedor4">
            <div class="hijo4">
                <p><b>ATENTAMENTE</b></p> <br><br><br><br>
                <p>
                    <b>
                        <?php echo mb_strtoupper($nombredoc, "UTF-8") . " " . mb_strtoupper($apellidodoc, "UTF-8"); ?><br>
                        <?php echo $categoria; ?> <br>
                        ADSCRITO EN <?php echo mb_strtoupper(gen_depe($dirdir)); ?> <br>
                        PLAZA <?php echo $plaza; ?>
                    </b>
                </p>

            </div>
        </div>
    </main>

</body>

</html>
<?php

$html = ob_get_clean();
// echo $html;

require_once '../../libreria/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;

$dompdf = new Dompdf();

// $dompdf->getOptions();
$options = new Options();
$options->set('isRemoteEnabled', true);

$dompdf->setOptions($options);

$dompdf->loadHtml($html);

// $dompdf->setPaper('letter');
$dompdf->setPaper('A4', 'portrait');

$dompdf->render();

//PARAMETROS
$x          = 505;
$y          = 790;
$text       = "";
$font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
$size       = 10;
$color      = array(0, 0, 0);
$word_space = 0.0;
$char_space = 0.0;
$angle      = 0.0;

$dompdf->getCanvas()->page_text(
    $x,
    $y,
    $text,
    $font,
    $size,
    $color,
    $word_space,
    $char_space,
    $angle
);


// $dompdf->stream("cedula.pdf", array("Attachment" => false));
$output = $dompdf->output();
$directorio = "cedulas/".$ciclo."/".$dependencia."/";
if(!is_dir($directorio)){
    $crear = mkdir($directorio, 0777, true);
}
file_put_contents( "cedulas/".$ciclo."/".$dependencia."/cedula_".$docente.".pdf", $output );

?>
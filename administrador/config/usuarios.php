<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';

if (isset($_GET['insertar'])) {
    $data = json_decode(file_get_contents("php://input"));
    $nombre = $data->nombre;
    $mail = $data->correo;
    $contraseña = $data -> contraseña;
    $numero = $data->numero;
    $depe = $data->depe;
    $tipo_id = 2;

    $contraseña = hash('sha512', $contraseña);
    $correo = mb_strtolower(filter_var($mail, FILTER_SANITIZE_EMAIL));

    $insertar = pg_prepare($conn, "insert_user", "INSERT INTO usuarios(nombre_user, numero, correo, contraseña, tipo_id) VALUES ($1,$2,$3,$4,$5) ");
    if ($correo) {
        $bus_user = pg_query($conn, "SELECT correo FROM usuarios WHERE correo='$correo' ");
        $resultado = pg_fetch_assoc($bus_user);
        if ($resultado == False) {
            $insertar = pg_execute($conn, "insert_user", array($nombre, $numero, $correo,$contraseña, $tipo_id));

            $b_user = pg_query($conn, "SELECT id_u from usuarios WHERE correo='$correo' ");
            $id_u = pg_fetch_array($b_user);
            $id = $id_u['id_u'];

            $asig_insert = pg_prepare($conn, "asig_insert", "INSERT INTO user_asig(id_user,clave_user) VALUES ($1,$2)");
            if ($id_u and $depe) {
                $b_asig = pg_query($conn, "SELECT * FROM user_asig WHERE id_user='$id' AND clave_user='$depe' ");
                $resultado2 = pg_fetch_assoc($b_asig);
                if ($resultado2 == False) {
                    $asig_insert = pg_execute($conn, "asig_insert", array($id, $depe));
                    echo json_encode(["success" => 1]);
                }
            }

            // echo json_encode(["success" => 1]);
        } else {
            echo json_encode(["success" => 0]);
        }
    }
}


if (isset($_GET['obtener'])) {

    $user = pg_query($conn, "SELECT id_u,nombre_user, numero, correo, direccion FROM view_usuarios_tipo WHERE tipo_id=2");
    if (pg_num_rows($user) > 0) {
        $usuarios = pg_fetch_all($user, PGSQL_ASSOC);
        echo json_encode($usuarios);
    } else {
        echo json_encode(["success" => 0]);
    }
}

if ($_SERVER["REQUEST_METHOD"] == 'DELETE') {
    $id = $_GET['id_del'];

    $del_user = pg_prepare($conn, "delete_user", "DELETE FROM usuarios WHERE id_u= $1 ");
    $b_user = pg_query($conn, "SELECT id_u FROM usuarios WHERE id_u='$id' ");
    if (pg_num_rows($b_user) == 1) {
        $del_asig = pg_query($conn, "DELETE FROM user_asig WHERE id_user='$id' ");
        $del_user = pg_execute($conn, "delete_user", array($id));
        echo json_encode(["success" => 1]);
    } else {
        echo json_encode(["success" => 0]);
    }
}

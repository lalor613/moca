<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET,POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require './bd.php';


if (isset($_GET['insertar'])) {
    $data = json_decode(file_get_contents("php://input"));
    $clave = $data->clave;
    $direccion = $data->direccion;

    $insertar = pg_prepare($conn, "Insertar_depes", "INSERT INTO dependencias VALUES($1, $2) ");
    if ($clave) {
        $bdepe = pg_query($conn, "SELECT clave FROM dependencias WHERE clave='$clave' ");
        $resultado = pg_fetch_assoc($bdepe);
        if ($resultado == False) {
            $insertar = pg_execute($conn, "Insertar_depes", array($clave, $direccion));
            echo json_encode(["success" => 1]);
        }
    }
    exit();
}

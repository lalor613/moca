<?php
   

// Varios destinatarios
$para  = $email; // atención a la coma


// título
$título = 'Restablecer contraseña MOCA';
$codigo = rand(1000,9999);
// mensaje
$mensaje = '
<html>
<head>
  <title>Restablecer contraseña</title>
</head>
<body>
  <h1>Universidad Autónoma de Chiapas</h1>
  <div style="text-align:center; background-color:#ccc">
    <p> Código para restablecer contraseña </p>
    <h3>'.$codigo.'</h3>
    <p>Para restablecer da <a href="http://' . $_SERVER['HTTP_HOST'] . '/moca/reset_pw.php?email='.$email.'&token='.$token.'">clic aquí<a/></p>
    <p><small>Si usted no envió este código, favor de omitir</small></p>
  </div>
</body>
</html>
';

// Para enviar un correo HTML, debe establecerse la cabecera Content-type
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'From: Your name <info@address.com>' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

// Cabeceras adicionales
// $cabeceras .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
// $cabeceras .= 'From: Recordatorio <cumples@example.com>' . "\r\n";
// $cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
// $cabeceras .= 'Bcc: birthdaycheck@example.com' . "\r\n";

// Enviarlo
$enviado = False;
if(mail($para, $título, $mensaje, $cabeceras)){
  $enviado = True;
}

  

?>
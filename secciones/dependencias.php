<?php 
$page_title ='Dependencias';
session_start();
if ($_SESSION['tipo_id']==1){
    include('../templates/cabecera.php');
}elseif($_SESSION['tipo_id']==2){
    header("Location: ../secciones/inicio.php");
}else{
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$depe = pg_query($conn, "SELECT * FROM dependencias");
$contador = 0;
?>
<div class="card">
    <div class="card-header">
        Añadir dependencia (s)
    </div>
    <div class="card-body align-items-start">
        <form action="javascript:void(0)" method="post">
            <div class="col-md-6 hstack gap-2">
                <div class="me-auto">
                    <div class="form-group m-1">
                        <label for="excel-input">Agregar documento dependencias</label>
                        <input type="file" class="form-control" name="excel-input" id="excel-input">
                    </div>
                    <div class="col-3 col-md-6 m-1">
                        <button type="submit" id="btn_guardar" onclick="excelInput.agregar();" class="btn btn-success form-control">Guardar</button>
                    </div>
                </div>
                <d1v class="me-auto">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Agregar</button>
                </d1v>
            </div>
        </form>
    </div>

</div>
<div class="card-footer text-muted justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
    <table class="table table-bordered table-inverse table-responsive" id="tabla-excel">
        <thead class="thead-inverse">
            <tr>
                <th scope="col">Prog.</th>
                <th>Dirección</th>
                <th>Clave</th>
            </tr>
        </thead>
        <tbody>
            <?php while ($row = pg_fetch_assoc($depe)) :
                $contador += 1;
            ?>
                <tr>
                    <td><?php echo $contador ?></td>
                    <td><?php echo $row['direccion'] ?></td>
                    <td><?php echo $row['clave'] ?></td>
                </tr>
            <?php endwhile ?>
        </tbody>
    </table>
</div>
</div>

<script type="text/javascript" src="../js/dependencias_excel.js"></script>


<?php include('../templates/pie.php'); ?>
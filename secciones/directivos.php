<?php
$page_title = 'Directorio';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    header("Location: ../secciones/inicio.php");
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';

$depe = pg_query($conn, "SELECT * FROM dependencias ORDER BY direccion ASC");
$depeedit = pg_query($conn, "SELECT * FROM dependencias ORDER BY direccion ASC");
$dir_depe = pg_query($conn, "SELECT * FROM vw_depe_dir");
$func = pg_query($conn, "SELECT * FROM funcionarios");

?>

<div class="card">
    <div class="card-header">
        Directivos
    </div>
    <div class="card-body">

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#modal_dir">
            Agregar Directivo
        </button>

        <!-- Modal -->
        <div class="modal fade" id="modal_dir" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Asignar directivo</h5>
                        <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="javascript:void(0);" method="post">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 container-fluid">
                                    <div class="form-group">
                                        <label for="clave_dir">Dependencia cargo</label>
                                        <input list="depe_choose" class="form-control form-control-sm" id="clave_dir" placeholder="--Elige Dependencia--">
                                        <datalist id="depe_choose">

                                            <select class="form-control form-control-sm" name="clave_dir" id="clave_dir">
                                                <?php
                                                while ($row = pg_fetch_assoc($depe)) {
                                                    echo "<option value=" . $row['clave'] . ">" . $row['direccion'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </datalist>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="titulo">Titulo</label>
                                    <select class="form-control form-control-sm" name="tit" id="tit">
                                        <option value="0" selected>--Elige un titulo--</option>
                                        <option value="Lic">Licenciado(a)</option>
                                        <option value="Mtro">Maestro</option>
                                        <option value="Mtra">Maestra</option>
                                        <option value="Dr">Doctor</option>
                                        <option value="Dra">Doctora</option>
                                        <option value="Ing">Ingeniero(a)</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="genero">genero</label>
                                    <select class="form-control form-control-sm" name="genero" id="genero">
                                        <option value="0" selected>--Elige un género--</option>
                                        <option value="Masculino">Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                    </select>
                                </div>
                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control form-control-sm" name="nombre" id="nombre" placeholder="Ingresa el nombre completo">
                                    </div>
                                </div>
                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <label for="puesto">Cargo</label>
                                        <select class="form-control form-control-sm" require name="puesto" id="puesto">
                                            <option selected>--Elegir cargo--</option>
                                            <option value="COORDINADORA">COORDINADORA</option>
                                            <option value="COORDINADOR">COORDINADOR</option>
                                            <option value="COORDINADORA ACADÉMICA">COORDINADORA ACADÉMICA</option>
                                            <option value="COORDINADOR ACADÉMICO">COORDINADOR ACADÉMICO</option>
                                            <option value="COORDINADORA GENERAL">COORDINADORA GENERAL</option>
                                            <option value="COORDINADOR GENERAL">COORDINADOR GENERAL</option>
                                            <option value="DIRECTORA">DIRECTORA</option>
                                            <option value="DIRECTOR">DIRECTOR</option>
                                            <option value="ENCARGADA DE LA DIRECCIÓN">ENCARGADA DE LA DIRECCIÓN</option>
                                            <option value="ENCARGADO DE LA DIRECCIÓN">ENCARGADO DE LA DIRECCIÓN</option>
                                            <option value="ENCARGADA DE LA COORDINACIÓN GENERAL">ENCARGADA DE LA COORDINACIÓN GENERAL</option>
                                            <option value="ENCARGADO DE LA COORDINACIÓN GENERAL">ENCARGADO DE LA COORDINACIÓN GENERAL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <label for="f_ocupar">Fecha de nombramiento</label>
                                        <input type="date" class="form-control form-control-sm" name="f_ocupar" id="f_ocupar">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" id="btn_agregar" onclick="agregar_dir();" class="btn btn-primary">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <div class="card-footer text-muted table-wrapper-scroll-x my-custom-scrollbar">

        <table class="table table-bordered table-inverse table-responsive" id="tabla_dir">
            <thead class="thead-inverse">
                <tr>
                    <th>Prog.</th>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Dependencia</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>

    </div>
</div>
<div class="dropdown-divider"></div>
<div class="card">
    <div class="card-header">
        Funcionarios
    </div>
    <div class="card-body">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#modal_fun">
            Agregar Funcionario
        </button>

        <!-- Modal -->
        <div class="modal fade" id="modal_fun" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Añadir Funcionario</h5>
                        <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="javascript:void(0);" method="post">
                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="form-group">
                                    <label for="nombre1">Nombre</label>
                                    <input type="text" class="form-control form-control-sm" name="nombre1" id="nombre1" placeholder="Ingresa el nombre completo">
                                </div>

                                <div class="form-group">
                                    <label for="direccion1">Direccion</label>
                                    <input type="text" class="form-control form-control-sm" name="direccion1" id="direccion1">
                                </div>

                                <div class="form-group">
                                    <label for="puesto1">Puesto</label>
                                    <input type="text" class="form-control form-control-sm" name="puesto1" id="puesto1">
                                </div>

                                <div class="form-group">
                                    <label for="f_nom">Fecha de Nombramiento</label>
                                    <input type="date" class="form-control form-control-sm" name="f_nom" id="f_nom">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="btn_guardar_fun" onclick="agregar_fun();" class="btn btn-primary">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-muted table-wrapper-scroll-x my-custom-scrollbar">
        <table class="table table-sm table-inverse table-responsive" id="tabla_fun">
            <thead class="thead-inverse">
                <tr>
                    <th scope="row">Nombre</th>
                    <th>Puesto</th>
                    <th>Fecha nombramiento</th>
                    <th>Direccion</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = pg_fetch_assoc($func)) : ?>
                    <tr>
                        <td><?php echo $row['nombre'] ?></td>
                        <td><?php echo $row['puesto'] ?></td>
                        <td><?php echo $row['f_nom'] ?></td>
                        <td><?php echo $row['direccion'] ?></td>
                    </tr>
                <?php endwhile ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_editarDir" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Directivo</h5>
                <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0)" method="post">
                <div class="modal-body">
                    <div class="col-sm-12" hidden>

                        <div class="form-group">
                            <label for="up_id">ID</label>
                            <input type="number" class="form-control form-control-sm" name="up_id" id="up_id" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 container-fluid">
                            <div class="form-group">
                                <label for="upclavedir">Dependencia cargo</label>
                                <input list="depe_edit" class="form-control form-control-sm" id="upclavedir" readonly placeholder="--Elige Dependencia--">
                                <datalist id="depe_edit">
                                    <select class="form-control form-control-sm" name="upclavedir" id="upclavedir">
                                        <?php
                                        while ($row = pg_fetch_assoc($depeedit)) {
                                            echo "<option value=" . $row['clave'] . ">" . $row['direccion'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </datalist>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="uptitulo">Titulo</label>
                            <select class="form-control form-control-sm" name="uptitulo" id="uptitulo">
                                <option selected value="0">--Elige un titulo--</option>
                                <option value="Lic">Licenciado(a)</option>
                                <option value="Mtro">Maestro</option>
                                <option value="Mtra">Maestra</option>
                                <option value="Dr">Doctor</option>
                                <option value="Dra">Doctora</option>
                                <option value="Ing">Ingeniero(a)</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="upgenero">Genero</label>
                            <select class="form-control form-control-sm" name="upgenero" id="upgenero">
                                <option value="0" selected>--Elige un género--</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Femenino">Femenino</option>
                            </select>
                        </div>
                        <div class="col-sm-12">

                            <div class="form-group">
                                <label for="upnombre">Nombre</label>
                                <input type="text" class="form-control form-control-sm" name="upnombre" id="upnombre" placeholder="Ingresa el nombre completo">
                            </div>
                        </div>
                        <div class="col-sm-12">

                            <div class="form-group">
                                <label for="uppuesto">Cargo</label>
                                <select class="form-control form-control-sm" required name="uppuesto" id="uppuesto">
                                    <option selected>--Elegir cargo--</option>
                                    <option value="COORDINADORA">COORDINADORA</option>
                                    <option value="COORDINADOR">COORDINADOR</option>
                                    <option value="COORDINADORA ACADÉMICA">COORDINADORA ACADÉMICA</option>
                                    <option value="COORDINADOR ACADÉMICO">COORDINADOR ACADÉMICO</option>
                                    <option value="COORDINADORA GENERAL">COORDINADORA GENERAL</option>
                                    <option value="COORDINADOR GENERAL">COORDINADOR GENERAL</option>
                                    <option value="DIRECTORA">DIRECTORA</option>
                                    <option value="DIRECTOR">DIRECTOR</option>
                                    <option value="ENCARGADA DE LA DIRECCIÓN">ENCARGADA DE LA DIRECCIÓN</option>
                                    <option value="ENCARGADO DE LA DIRECCIÓN">ENCARGADO DE LA DIRECCIÓN</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">

                            <div class="form-group">
                                <label for="upf_ocupar">Fecha de nombramiento</label>
                                <input type="date" class="form-control form-control-sm" name="upf_ocupar" id="upf_ocupar">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" onclick="actualizarDir();" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/directivos.js"></script>


<?php include('../templates/pie.php'); ?>
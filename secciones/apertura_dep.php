<?php 
$page_title ='Apertura';
session_start();
if ($_SESSION['tipo_id']==1){
    include('../templates/cabecera.php');
}elseif($_SESSION['tipo_id']==2){
    header("Location: ../secciones/inicio.php");
}else{
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$dependencias = pg_query($conn, "SELECT clave, direccion FROM dependencias order by direccion asc");
$depe_sel = pg_query($conn, "SELECT clave, direccion FROM dependencias order by direccion asc");
$ciclos = pg_query($conn, "SELECT id_ciclo, descripcion FROM ciclos_escolares");

?>


<div class="card">
    <div class="card-header">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h6 class="display-6">Apertura de Unidades Académicas</h6>
                
                <p class="lead">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_depe">
                        Crear
                    </button>
                </p>
            </div>
        </div>
    </div>
    <div class="card-body">

        buscar por UA

    </div>
    <div class="card-footer text-muted">
        <h4 class="card-title">Aperturas de ciclos</h4>
        <table id="tabla_aperturas" class="table table-hover table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Ciclo</th>
                    <th>Unidad académica</th>
                    <th>Clave</th>
                </tr>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_depe" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Apertura de dependencia en el ciclo escolar</h5>
                <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="javascript:void(0);" method="post">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-12 container-fluid">
                            <div class="form-group">
                                <label for="ac_depe">Unidad Académica</label>
                                <input list="depe_choose" class="form-control form-control-sm" id="ac_depe">
                                <datalist id="depe_choose">
                                    <select name="ac_depe" id="ac_depe">
                                        <?php while ($row = pg_fetch_assoc($depe_sel)) {
                                            echo "<option value=" . $row['clave'] . ">" . $row['direccion'] . "</option>";
                                        } ?>
                                    </select>
                                </datalist>
                            </div>
                        </div>

                        <div class="col-sm-12">

                            <div class="form-group">

                                <label for="ac_ciclo">Ciclo Escolar</label>
                                <select class="custom-select form-control form-control-sm" name="ac_ciclo" id="ac_ciclo">
                                    <option value="0" selected>--Elige Ciclo Escolar--</option>
                                    <?php while ($row = pg_fetch_assoc($ciclos)) {
                                        echo "<option value=". $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-success" id="btn_crear" onclick="agregarApertura();"> Crear</button>
                    </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/apertura_dep.js"></script>

<?php include('../templates/pie.php'); ?>
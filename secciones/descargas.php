<?php
$page_title = 'Descargas';
session_start();
if ($_SESSION['tipo_id']==1){
    include('../templates/cabecera.php');
}elseif($_SESSION['tipo_id']==2){
    header("Location: ../secciones/inicio.php");
}else{
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
$motivo = pg_query($conn, "SELECT * FROM motivo_desc");
?>

<div class="p-4">
    <div class="card">
        <div class="card-header">


            <div class="row p-3">
                <div class="col-sm-6">

                    <h4 class="card-title">Descargas Académicas</h4>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-md btn-block" data-toggle="modal" data-target="#modal_descarga">Crear</button>
                </div>

            </div>
        </div>
        <div class="card-body">
            <table class="table table-sm table-inverse table-responsive" id="tabla_desc">
                <thead class="thead-inverse thead-dark">
                    <tr>
                        <th>Ciclo Escolar</th>
                        <th>Unidad Académica</th>
                        <th>Docente</th>
                        <th>Motivo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer text-muted">
            Footer
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_descarga" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear Descarga</h5>
                <button type="button" class="close btn-danger" onclick="limpiar();" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0);" method="post">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="ciclo_esc">Ciclo escolar</label>
                                        <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                                            <option value="0" selected>--Elige Ciclo escolar--</option>
                                            <?php while ($row = pg_fetch_assoc($ciclo)) {
                                                echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group" id="doc_depe">

                                </div>
                            </div>
                            <div id="docentes">

                            </div>
                            <div class="form-group">
                                <label for="motivo">Motivo de descarga</label>
                                <select class="form-control form-control-sm" name="motivo" id="motivo">
                                    <option value='0' selected>--Selecciona un motivo--</option>
                                    <?php while($row = pg_fetch_assoc($motivo)){
                                        echo "<option value=".$row['id_motivo'].">".$row['nombre_motivo']."</option>";
                                    } ?>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="limpiar();" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="CrearDescarga();" class="btn btn-success">Crear</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/descargas.js"></script>
<?php include('../templates/pie.php'); ?>
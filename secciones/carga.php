<?php
$page_title = 'Carga Académica';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>
<div class="card">
    <div class="card-header">
        <div class="form-group">
            <form action="javascript:void(0);" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="ciclo esc">Ciclo escolar</label>
                            <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                                <option value="0" selected>--Elige ciclo escolar--</option>
                                <?php while ($row = pg_fetch_assoc($ciclo)) {
                                    echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group" id="doc_depe">

                    </div>
                </div>
                <div class="form-group m-3">
                    <div class="row">
                        <button type="button" onclick="general();" class="btn btn-primary btn-md btn-block">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6" id='direccion'>
                DEPENDENCIA Y CICLO
            </div>
            <div class="col-md-6">
                <div class="row form-control" id="docente">

                </div>
                <br>

                <div class="row">
                    <div class="btn-group" data-toggle="radio">

                        <label class="btn btn-secondary form-check-label"> HTD
                            <input class="form-check-input" type="radio" name="tipo" id="tipo" onclick=" MateriasCargadas();" checked value="1">
                        </label>


                        <label class="btn btn-secondary form-check-label"> HI
                            <input class="form-check-input" type="radio" name="tipo" id="tipo" onclick="MateriasCargadas();" value="2">
                        </label>

                        <div class="form-check form-check-inline ">
                            <button id="btn_crear" type="button" onclick="crearContrato();" class="btn btn-success btn-md btn-block">Crear</button>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <p class="me-auto">
                            Materias
                        </p>
                    </div>

                    <div class="table table-sm table-responsive justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
                        <table class="table" id="tabla_materias">
                            <thead class="thead-inverse">
                                <tr>
                                    <th></th>
                                    <th>Materia</th>
                                    <th>Semestre</th>
                                    <th>grupo</th>
                                    <th>HSM</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <!-- hstack ESTE SIRVE PARA HACER LA SIMULACIN DE SPACEBETWEEN -->
                        <p class="me-auto">
                            Materias Asignadas
                        </p>
                        <div class="row">

                            <div class="col">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="ml-auto">
                                            HTD:
                                        </p>
                                    </div>
                                    <div class="col">
                                        <div id="htd" class="me-auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="ml-auto">
                                            HI:
                                        </p>
                                    </div>
                                    <div class="col">
                                        <div id="hi" class="me-auto"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="table table-sm table-inverse table-responsive justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
                        <table class="table" id="tabla_materias_asig">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th>Materia</th>
                                    <th>Semestre</th>
                                    <th>grupo</th>
                                    <th>HSM</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<br>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="modal_editarmate" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Carga Horaria</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" method="post">

                    <div class="row">
                        <div class="col-sm-6">
                            <input hidden type="number" id="id_mate">
                            <strong>
                                <label for="">Asignatura</label>
                            </strong>
                            <br>
                            <div id="asignatura"></div>
                        </div>
                        <div class="col-sm-2">
                            <label for="hrs_t">HT</label>
                            <input type="number" name="hrs_t" id="hrs_t" class="form-control" readonly>
                        </div>
                        <div class="col-sm-2">
                            <label for="hrs_p">HP</label>
                            <input type="number" name="hrs_p" id="hrs_p" class="form-control" readonly>
                        </div>
                        <div class="col-sm-2">
                            <label for="total">TOTAL</label>
                            <input type="number" name="total" min="0" pattern="^[0-9]+" id="total" class="form-control">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 m-3">
                        <!-- <button type="button" class="btn btn-md btn-warning">
                            <span class="material-symbols-outlined">
                                restart_alt
                            </span>
                        </button> -->
                    </div>
                    <div class="col-md-5 ml-md-auto m-3">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" onclick="modificarcarga();" class="btn btn-success">Guardar</button>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<script src="../js/carga.js"></script>
<?php include('../templates/pie.php'); ?>
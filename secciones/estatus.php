<?php
$page_title = 'Añadir docentes';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Estatus por Unidad Académica</h4>
        <form action="" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ciclo esc">Ciclo escolar</label>
                        <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                            <option value="0" selected>--Elige ciclo escolar--</option>
                            <?php while ($row = pg_fetch_assoc($ciclo)) {
                                echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 form-group" id="doc_depe">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" onclick="obtener_docentes();" class="btn btn-primary btn-md btn-block">Buscar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">

    </div>
    <div class="card-footer text-muted">
        Footer
    </div>
</div>

<script src="../js/estatus.js"></script>

<?php include('../templates/pie.php'); ?>
<?php
$page_title = 'Contratos';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php
require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
$descarga = pg_query($conn, "SELECT * FROM vw_desc_docmoti");
?>

<div class="card">
    <div class="card-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="card-title">Busqueda de contratos por ciclo escolar y Unidad Académica</h4>
                </div>
                <div class="col">
                    <a name="" id="btn_faculty" class="btn btn-primary" href="" role="button" target="_blank">Ver Contratos</a>
                </div>
            </div>
        </div>
    
      
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="ciclo esc">Ciclo escolar</label>
                    <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                        <option value="0" selected>--Elige Ciclo escolar--</option>
                        <?php while ($row = pg_fetch_assoc($ciclo)) {
                            echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 form-group" id="doc_depe">

            </div>
            <strong>
                <div class="col-sm-12 m-3" id="direccion">

                </div>
            </strong>
        </div>
        <div class="form-group m-3">
            <div class="row">
                <button class="btn btn-primary" id="btn_buscar" onclick="listaContratos();" type="submit">Buscar</button>
            </div>
        </div>
    </div>
    <div class="card-body justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
        <table class="table table-hover table-responsive" cellspacing="7" id="tabla_docente">
            <thead class="thead-inverse">
                <tr>
                    <th>Prog.</th>
                    <th>Nombre</th>
                    <th>Tipo Contrato</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>

                </tr>
            </tbody>
        </table>
    </div>
</div>



<!-- Modal asignar descarga -->
<div class="modal fade" id="modal_asigdes" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar Titular</h5>
                <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <form action="javascript:void(0)" method="post">
                        <div class="card-header">
                            <h4 class="card-title">Asignar una descarga al contrato de:</h4>
                            <div class="col-sm-6">
                                <input class="form-control form-control-sm" type="text" readonly name="nombre" id="nombre">
                                <input class="form-control form-control-sm" type="number" hidden name="id_cont" id="id_cont">
                                <input class="form-control form-control-sm" type="number" hidden name="id_doc" id="id_doc">
                                <input class="form-control form-control-sm" type="number" hidden name="id_ciclo" id="id_ciclo">
                                <input class="form-control form-control-sm" type="number" hidden name="id_depe" id="id_depe">
                                <input class="form-control form-control-sm" type="number" hidden name="id_tipo" id="id_tipo">
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="javascript:void(0);" method="post">
                                
                                <div id="descarga">

                                </div>
                                <button type="button" id="btn_asig" onclick="asignar_descarga();" class="btn btn-success btn-sm btn-block">Asignar</button>

                            </form>
                        </div>
                        <div class="card-footer text-muted">
                            <table id="tabla_descargasasig" class="table table-inverse table-inverse table-responsive">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Prog</th>
                                        <th>Docente</th>
                                        <th>Motivo</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="../js/contratos.js"></script>

<?php include('../templates/pie.php'); ?>
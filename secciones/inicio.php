<?php
$page_title = 'Inicio';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<style>
    img {
        display: block;
        margin: auto;
    }
</style>
<div class="col-sm-12">

    <div class="card justify-content-center" style="background-color: #fff; color:black;">

        <div class="card-body" style="color: black;">
            <div id="carouselId" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselId" data-slide-to="1"></li>
                    <li data-target="#carouselId" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img src="../img/LOGO DGDSE UNACH.jpg" width="180 px" height="180 px" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3>¡Entérate de nuestros próixmos eventos!</h3>
                            <p style="color: black;">Ver próximos eventos</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../img/LOGO DGDSE UNACH.jpg" width="180 px" height="180 px" alt="Second slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Minuta de evaluación para las próximas publicaciones</h3>
                            <p style="color: black;">Ver Minuta</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../img/LOGO DGDSE UNACH.jpg" width="180 px" height="180 px" alt="Third slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Revistas recomendadas</h3>
                            <p style="color: black;">Ver Revistas</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" style="color: black;" href="#carouselId" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">
                        <span class="material-symbols-outlined">
                            arrow_back_ios
                        </span>
                    </span>
                </a>
                <a class="carousel-control-next" style="color: black;" href="#carouselId" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">
                        <span class="material-symbols-outlined">
                            arrow_forward_ios
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>



</div>


<?php include('../templates/pie.php'); ?>
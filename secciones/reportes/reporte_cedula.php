<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte contrato</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style type="text/css">
        @page {
            margin: 0cm 1cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
            font-size: 16px;

            text-align: justify;
        }

        @media print {}

        body {
            margin-top: 1.5cm;
            margin-left: 1cm;
            margin-right: 2rem;
            margin-bottom: 2cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        .fechas{
            /* display: flex; */
            text-align: right;
 
        }
        .hijo1{
            margin: 0;
        }
        .hijo2{
            margin-top:auto;
        }
        .director{
            text-align: left;
            font-weight: bold;
            line-height: 0.3em;
        }
        div.contenedor4{
            display: flex;
            position:relative;
            margin-top: auto;
            margin-bottom: auto;
            /* border: 1px solid black; */
            /* align-self: flex-start; */
            font-size: 14px;
            height: auto;
            page-break-inside: avoid;
        }
        div.hijo4{
            height: 50%;
            margin-top: auto;
            text-align: center;
        }
    </style>
</head>

<body>
   
    <header>
        <div class="fechas">
            <!-- <p class="hijo1">Tuxtla Gutiérrez, Chiapas;</p> -->
            <p class="hijo2">fecha del periodo de una cadena</p>
            <br><br>
        </div>
  

    </header>

    <main>
        <div class="director">
            <p>PERSONA QUE ESTÁ AL FRENTE DE LA UA</p>
            <p>CARGO QUE OCUPA</p>
            <p>ESCUELA Y CAMPUS COMO DIRECTIVO</p>
            <p>P R E S E N T E</p>
        </div>
        <br><br>
        <div>
            <p>
                Quien suscribe, _______________ ___________ _____ ______, en mi carácter de CATEGORIA DEL PROFESOR,
                con identificación oficial con fotografía expedida por el Instituto Nacional Electoral anexa,
                expreso mi compromiso para entregar copia simple de mi Cédula (profesional o de grado), 
                que acredita mis estudios de __________________, antes del 31 de julio de 2023, con la finalidad de integrar
                mi expediente y para comprobar que cuento con los conocimientos profesionales para ejercer satisfactoriamente
                las actividades de docencia  dentro de esta Institución Educativa.
            </p>
            <p>
                Sin más por el momento, agradezco la atención al presente oficio.
            </p>
        </div>
        <br><br>
        <div class="contenedor4">
            <div class="hijo4">
                <p><b>ATENTAMENTE</b></p> <br><br>
                <p>
                    <?php // echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
                </p>
            </div>
        </div>
    </main>

</body>

</html>
<?php

$html = ob_get_clean();
// echo $html;

require_once '../../libreria/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;

$dompdf = new Dompdf();

// $dompdf->getOptions();
$options = new Options();
$options->set('isRemoteEnabled', true);

$dompdf->setOptions($options);

$dompdf->loadHtml($html);

// $dompdf->setPaper('letter');
$dompdf->setPaper('A4', 'portrait');

$dompdf->render();

//PARAMETROS
$x          = 505;
$y          = 790;
$text       = "";
$font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
$size       = 10;
$color      = array(0, 0, 0);
$word_space = 0.0;
$char_space = 0.0;
$angle      = 0.0;

$dompdf->getCanvas()->page_text(
    $x,
    $y,
    $text,
    $font,
    $size,
    $color,
    $word_space,
    $char_space,
    $angle
);


// $dompdf->stream("cedula.pdf", array("Attachment" => false));
$output = $dompdf->output();
$directorio = "cedulas/".$_Ciclo."/".$_Faculty."/";
if(!is_dir($directorio)){
    $crear = mkdir($directorio, 0777, true);
}
file_put_contents( "cedulas/".$_Ciclo."/".$_Faculty."/cedula_".$_Teacher.".pdf", $output );

?>
<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte contrato Interino</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style type="text/css">
        @page {
            margin: 0cm 1cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
            font-size: 16px;

            text-align: justify;
        }

        body {
            margin-top: 3.5cm;
            margin-left: 1cm;
            margin-right: 2rem;
            margin-bottom: 2cm;
            font-family: 'Arial Narrow', Arial, sans-serif;
        }

        header {
            position: fixed;
            top: 1cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;

        }

        footer {
            position: absolute;
            bottom: -1cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }

        #page::after {
            display: inline;

            content: counter(page);
            font-style: bold
        }

        table {
            border: unset 1px #000000;
            font-family: 'Arial Narrow', Arial, sans-serif;
            font-size: x-small;
            text-align: justify;

        }

        tr {
            border: solid .5px #000000;
        }

        th {
            border: solid .5px #000000;
            background-color: #C2C2C2;
            text-align: center;
            border-collapse: collapse;
            margin: 7px;
            padding: 5px;
            /* margin-bottom: 25px;
            padding-bottom: 20px; */
        }

        td {
            border: solid .5px #000000;
            font-size: xx-small;
            border-collapse: separate;
            margin-top: 1px;
            padding-bottom: 5px;
            padding-left: 5px;
        }

        td#gyg {
            text-align: center;
        }

        td#alineado {
            text-align: left;
        }

        td#total {
            text-align: center;
            font-weight: bold;
        }

        p#titulo {
            text-align: center;
        }

        div {
            line-height: 115%;
            font-family: 'Arial Narrow', Arial, sans-serif;
        }

        div.firmas {
            font-size: xx-small;
        }

        div.incisos {
            margin-right: 0px;
            margin-top: -20px;
            /* margin-bottom: 5px; */
            text-indent: -20px;
            position: static;
            padding-left: 45px;
            /* padding-right: 20px; */
            text-align: justify;
        }

        div.contenedor_incisos {
            width: auto;
            height: auto;
            /* padding-left: 20px; */
            /* padding-right: 20px; */
            text-align: justify;
            /* border: 1px solid black; */
        }

        /* div.padding1 {
            border: 1px solid black;
            width: 40%;
            margin-top: 30px;
            margin-bottom: -10px;
            margin-right: 5rem;
            /* margin-inline-end: 3rem; */
        /* margin-bottom:-1 5px; */
        /* position: relative;
            padding-left: 3rem; */
        /* padding-top: 1rem; */
        /* justify-content: center;*/
        /* text-align: center; */
        /* font-size: 14px; */
        /* } */

        /* div.padding2 {
            height: auto;
            width: 40%;
            border: 1px solid black;
            margin-top: -20px;
            margin-bottom: -10px;
            padding-top: -1rem;
            padding-left: 22rem;
            /* margin-inline-end: 3rem; */
        /* margin-bottom: 5px; */
        /* position: unset; */
        /* text-align: center; */
        /* justify-content: center;*/
        /* font-size: 14px; */
        /* }  */
        /* div.supercontenedor {
            display: flex;
            border: 1px solid black;
            height:50rem;
        } */

        div.contenedor {
            position:relative;
            display: flex;
            margin-top: 1rem;
            margin-bottom: auto;
            /* height: 60px; */
            /* border: 10px solid black; */
            font-size: 14px;
            height: 6rem;
        }

        div.contenedor2 {
            position:relative;
            display: flex;
            margin-top: 1rem;
            margin-bottom: auto;
            font-size: 14px;
            height: 6rem;
        }

        div.contenedor3 {
            display: flex;
            position:relative;
            margin-top: auto;
            margin-bottom: auto;
            /* border: 1px solid black; */
            /* align-self: flex-start; */
            font-size: 14px;
            height: 6rem;
            page-break-inside: avoid;
        }
        div.contenedor4{
            display: flex;
            position:relative;
            margin-top: auto;
            margin-bottom: auto;
            /* border: 1px solid black; */
            /* align-self: flex-start; */
            font-size: 14px;
            height: auto;
            page-break-inside: auto;
        }
        div.hijo1 {
            width: 50%;
            text-align: center;
            /* border: 1px solid black; */
        }

        div.hijo2 {
            width: 50%;
            margin-left: auto;
            text-align: center;
            /* border: 1px solid black; */
            /* align-self: flex-start; */

        }

        div.hijo3 {
            /* width: 100%;
            margin-right: auto;
            margin-left: auto;
            margin: auto;
            text-align: center;
            height: 3rem; */
            /* border: 1px solid black; */
            height: 50%;
            text-align: center;
        }
        div.hijo4{
            height: 50%;
            margin-top: auto;
            text-align: center;
        }
    </style>
</head>

<body>
    <?php
    $id_contrato = $_POST['id_cont'];
    $docente = $_POST['docente_id'];
    $dependencia = $_POST['docente_dep'];
    $ciclo = $_POST['docente_ciclo'];
    $tipo = $_POST['tipo'];

    include("./func_form2.php");
    // strtolower() - Convierte un string a minúsculas
    // ucfirst() - Convierte el primer caracter de una cadena a mayúsculas
    // ucwords() - Convierte a mayúsculas el primer caracter de cada palabra de una cadena
    // mb_strtoupper() - Convierte un string en mayúsculas
    ?>
    <header>
        <?php while ($row = pg_fetch_assoc($imagen)) : ?>
            <img src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/moca/img/<?php echo $row['logo'] ?>" class="d-inline-block" width="700px" height="80 px" alt="">
        <?php endwhile ?>

    </header>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 contenedor_incisos">
                    <p>
                        <b>CONTRATO DE TRABAJO POR TIEMPO DETERMINADO PARA CUBRIR INTERINATO</b> QUE CELEBRAN, POR UNA PARTE, <b>LA UNIVERSIDAD AUTÓNOMA DE CHIAPAS</b>,
                        REPRESENTADA POR EL C.P.C. <?php echo mb_strtoupper($nombrefunc4, "UTF-8"); ?>, <?php echo strtoupper($puestofunc4); ?>,
                        ASISTIDO POR LOS CC. LIC. <?php echo mb_strtoupper($nombrefunc2, "UTF-8"); ?>, <?php echo strtoupper($puestofunc2); ?>,
                        MTRO. <?php echo mb_strtoupper($nombrefunc3, "UTF-8") ?>, <?php echo strtoupper($puestofunc3); ?>,
                        MTRO. <?php echo mb_strtoupper($nombrefunc1, "UTF-8"); ?>, <?php echo strtoupper($puestofunc1); ?>
                        Y <?php echo strtoupper(gen_direc($generodirec)) . " " . strtoupper($titulodirec) . ". " . mb_strtoupper($nombredirec, "UTF-8") . ", " . mb_strtoupper($puestodirec) . " EN " . strtoupper(gen_depe($dirdir)); ?>
                        Y POR LA OTRA, <?php echo gen_C($genero) ?> <b>C. <?php echo mb_strtoupper($nombredoc, "UTF-8") . " " . mb_strtoupper($apellidodoc, "UTF-8") . ","; ?></b> A QUIENES EN LO SUCESIVO SE LES DENOMINARÁ <b>"LA UNACH"</b>
                        Y <b><?php echo genero_trabajador($genero); ?></b> RESPECTIVAMENTE, Y EN FORMA CONJUNTA COMO <b>"LAS PARTES"</b>, INSTRUMENTO QUE SE FORMALIZA AL TENOR DE LAS DECLARACIONES Y LAS CLAUSULAS SIGUIENTES:
                    </p>
                </div>
                <div>
                    <p id="titulo">
                        <b>
                            D E C L A R A C I O N E S
                        </b>
                    </p>
                </div>
                <div>
                    <p>
                        <b>I.</b> Declara <b>"LA UNACH"</b>, a través de su representante, que:
                    </p>
                </div>
                <div>
                    <p>
                        <b>I.1</b> De conformidad con el artículo 3 de su Ley Orgánica, es un organismo autónomo descentralizado,
                        reconocido constitucionalmente, de interés público, con personalidad jurídica y patrimonio propio, al servicio de los intereses de la Nación, del Estado y de la comunidad universitaria,
                        creado por Decreto número 98, de fecha 28 de septiembre de 1974, expedido por la Quincuagésima Segunda Legislatura Constitucional del Estado Libre y Soberano de Chiapas, publicado en el Periódico Oficial número 43, de fecha 23 de octubre de 1974, ante la necesidad de contar con una institución rectora de la vida académica en la Entidad.
                    </p>
                </div>
                <div>
                    <p>

                        <b>I.2</b> Conforme al numeral 4 de su precitada Ley Orgánica, tiene por objeto incidir en el desarrollo de Chiapas y de la nación, particularmente de la región sur-sureste del país así como de Centroamérica, a través de la enseñanza de la educación superior, de la investigación, de la construcción, extensión y socialización del conocimiento y la cultura, por medio de la formación y actualización de técnicos, profesionistas, profesores e investigadores con compromiso social que permita coadyuvar en el desarrollo social, económico, político y cultural.

                    </p>
                </div>
                <div>
                    <p>
                        <b>I.3</b> El C.P.C. <?php echo nombres_compuestos($nombrefunc4); ?>, en su carácter de <?php echo $puestofunc4; ?>, goza de facultades legales
                        para suscribir el presente contrato, de conformidad con lo establecido en el Artículo 23 de la Ley Orgánica y 605, fracción I, del
                        Estatuto General, acreditando su personalidad con nombramiento de fecha <?php echo fechaEs($fnomfunc4); ?>; el Lic. <?php echo $nombrefunc2; ?>, en su carácter de <?php echo $puestofunc2; ?>
                        y Representante Legal, goza de facultades legales para suscribir el presente contrato, de conformidad con lo establecido en el artículo 24 de la Ley Orgánica,
                        acreditando su personalidad como <?php echo $puestofunc2; ?> con nombramiento de fecha <?php echo fechaEs($fnomfunc2); ?>;
                        el Mtro. <?php echo $nombrefunc3; ?> acredita su personalidad como <?php echo nombres_compuestos($puestofunc3); ?>, con nombramiento de fecha <?php echo fechaEs($fnomfunc3); ?>,
                        Mtro. <?php echo $nombrefunc1; ?>, acredita su personalidad como <?php echo nombres_compuestos($puestofunc1); ?>, con nombramiento de fecha <?php echo fechaEs($fnomfunc1); ?> y
                        <?php echo gen_direc($generodirec) . " " . $titulodirec . ". " . nombres_compuestos($nombredirec) . ", acredita su personalidad como " . nombres_compuestos($puestodirec) . " en " . nombres_compuestos(gen_depe($dirdir));?>,
                        con nombramiento de fecha <?php echo fechaEs($focupardirec) . "." ?>

                    </p>
                </div>
                <div>
                    <p>
                        <b>I.4</b> Por las necesidades académicas en <?php echo nombres_compuestos(gen_depe($dirdir)) ?> contrata
                        <b>por tiempo determinado</b> en términos del artículo 37 fracción II de la Ley Federal del Trabajo los servicios de
                        <b><?php echo genero_trabajador($genero); ?></b>, en virtud de que se tiene por objeto substituir temporalmente
                        <?php if (pg_num_rows($desc_asig) == 1) {
                            foreach ($descargas as $key => $titu) {
                                if ($key === array_key_first($descargas)) {
                                    echo genero_titular($titu['genero']) . "<b>C. " . nombres_compuestos($titu['nombre']) . " " . nombres_compuestos($titu['apellido']) . "</b>. 
                                    Con motivo de cubrir las materias que venía impartiendo el antes aludido con adscripción en " . nombres_compuestos(gen_depe($depedoc)) . " de <b>\"LA UNACH\".</b>";
                                }
                            }
                        } elseif (pg_num_rows($desc_asig) == 2) {
                            foreach ($descargas as $key => $titu) {
                                if ($key === array_key_first($descargas)) {
                                    echo genero_titular($titu['genero']) . "<b>C. " . nombres_compuestos($titu['nombre']) . " " . nombres_compuestos($titu['apellido']) . "</b> ";
                                }
                                if ($key === array_key_last($descargas)) {
                                    echo "y, a " . genero_titular($titu['genero']) . "<b>C. " . nombres_compuestos($titu['nombre']) . " " . nombres_compuestos($titu['apellido']) . "</b>.
                                    Con motivo de cubrir las materias que venían impartiendo los antes aludidos con adscripción en " . nombres_compuestos(gen_depe($depedoc)) . " de <b>\"LA UNACH\".</b>";
                                }
                            }
                        } else {
                            echo "No hay descarga asignada o excede a lo permitido";
                        } ?>
                    </p>
                </div>
                <div>
                    <p>
                        <b>I.5</b> Requiere los servicios profesionales de <b>
                            <?php echo genero_trabajador($genero); ?>
                        </b> para la impartición de cátedras consistentes en
                        <?php echo $suma; ?> HSM, para ejecutarse en un término que va del
                        <?php echo fechaEs($periodo_ini) . " al " . fechaEs($periodo_fin) . " en"; ?>
                        <?php echo nombres_compuestos(gen_depe($depedoc)) . "." ?>

                    </p>
                </div>
                <div>
                    <p>
                        <b>I.6</b> Señala como domicilio legal el ubicado en Boulevard Dr. Belisario Domínguez, Km. 1081 sin número, Colina Universitaria, Edificio de Rectoría, Terán en la ciudad de Tuxtla Gutiérrez, Chiapas, C.P. 29050.
                    </p>
                </div>
                <div>
                    <p>
                        <b>II.</b> Declara <b><?php echo genero_trabajador($genero); ?></b> que:
                    </p>
                </div>
                <div>
                    <p>
                        <b>II.1</b> Es una persona física, mayor de edad, de nacionalidad
                        <?php echo $nacionalidad . ", de " . $edad . " años"; ?>
                        con clave única de registro de población <?php echo $curp; ?>
                        y con domicilio particular en calle <?php echo nombres_compuestos($calle) . ","; ?>
                        <?php echo nombres_compuestos($colonia) . ","; ?>
                        Código postal <?php echo $cp; ?>
                        en la ciudad de <?php echo nombres_compuestos($ciudad) . "."; ?>
                    </p>
                </div>
                <div>
                    <p>
                        <b>II.2</b> Posee título profesional de
                        <?php echo titulos_cedula($grado_acad); ?>
                        otorgado por
                        <?php echo nombres_compuestos($otorgado); ?>
                        de fecha
                        <?php echo fechaEs($f_otor); ?>
                        y cédula profesional
                        <?php echo ($cedula == 0) ? "en trámite, documento que se adjunta en fotocopia simple para que sea parte integrante del presente instrumento."
                        : "número ".$cedula . ", expedida por la Dirección General de Profesiones de la Secretaría de Educación Pública, documentos que se adjuntan en fotocopia simples para que sean parte integrante del presente instrumento."; ?>
                    </p>
                </div>
                <div>
                    <b>II.3</b> Tiene experiencia profesional y conocimientos en docencia para realizar el trabajo que se le encomienda y cuenta con los conocimientos técnicos que se requieren para desarrollarla satisfactoriamente.
                </div>
                <div>
                    <b>II.4</b> Para los fines legales, de este contrato, su domicilio es el que ha señalado en la declaración <b>II.1</b> de este apartado, el cual subsistirá hasta en tanto no proporcione otro distinto, por escrito, a <b>“LA UNACH”</b>.
                </div>
                <div>
                    <b>III.</b> Declaran <b>"LAS PARTES"</b> que:
                </div>
                <div>
                    <b>III.1</b> La relación de trabajo que se contrata se regula de conformidad a lo establecido por los artículos 353-J 353-K 353-L 353-M 353-N y 353-U de la
                    Ley Federal del Trabajo y consiste en la prestación eventual de los servicios personales de <b>
                        <?php echo genero_trabajador($genero); ?>
                    </b>para cubrir, por tiempo determinado, con fundamento en el artículo 37 fracción I de la Ley Federal del Trabajo.
                </div>
                <div>
                    <b>III.2</b> Admiten la personalidad jurídica que con la que se ostentan y que es su voluntad celebrar el presente contrato, siendo conformes en obligarse al tenor de las siguientes:
                </div>
                <div>
                    <p id="titulo">
                        <b>
                            C L Á U S U L A S
                        </b>
                    </p>
                </div>
                <div>
                    <b>PRIMERA:
                        <?php echo genero_trabajador($genero); ?>
                    </b>se obliga con <b>"LA UNACH"</b> para cubrir, de manera interina y por tiempo determinado en términos del artículo 37 fracción II de la Ley Federal del Trabajo
                    <?php
                    if (pg_num_rows($desc_asig) == 1) {
                        foreach ($titular as $key => $dato) {
                            if ($key === array_key_first($titular)) {
                                echo genero_titular($dato['genero']) . "<b>C. " . nombres_compuestos($dato['nombre']) . " " . nombres_compuestos($dato['apellido']) . "</b> quien solicitó permiso " . $dato['nombre_motivo'];
                            }
                        }
                    } elseif (pg_num_rows($desc_asig) == 2) {
                        foreach ($titular as $key => $dato) {
                            if ($key === array_key_first($titular)) {
                                echo genero_titular($dato['genero']) . "<b>C. " . nombres_compuestos($dato['nombre']) . " " . nombres_compuestos($dato['apellido']) . "</b> quien solicitó permiso " . $dato['nombre_motivo'] . " ";
                            }
                            if ($key === array_key_last($titular)) {
                                echo "y, " . genero_titular($dato['genero']) . "<b>C. " . nombres_compuestos($dato['nombre']) . " " . nombres_compuestos($dato['apellido']) . "</b> quien solicitó permiso " . $dato['nombre_motivo'];
                            }
                        }
                    } else {
                        echo "No hay descarga asignada o excede a lo permitido";
                    }
                    ?> Es decir, de manera temporal ocupará la plaza número <?php echo $plaza; ?> con categoría de <?php echo $categoria . ", " ?> la cual por derecho le corresponde al trabajador antes señalado,
                    lo anterior para impartir las materias que se mencionan en la cláusula siguiente, por lo que el presente contrato tendrá vigencia únicamente por el periodo comprendido del
                    <?php echo fechaEs($periodo_ini) . " al " . fechaEs($periodo_fin) . ","; ?>
                    <!-- periodos completos para todos? -->
                    con un total de <?php echo $suma; ?> HSM.

                </div>
                <div>
                    <p>
                        <b>SEGUNDA:</b> Las horas señaladas en la cláusula anterior, serán distribuidas de la siguiente forma:
                    </p>
                </div>
                <div>
                    <table class="" style="margin: 0 auto">
                        <thead class="">
                            <tr>
                                <th>PLAN DE ESTUDIOS</th>
                                <th>UNIDAD DE COMPETENCIA</th>
                                <th>GRADO Y GRUPO</th>
                                <th>H/S/M</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            while ($row = pg_fetch_assoc($asignaturas)) :
                                if ($row['total'] != 0) :
                            ?>
                                    <tr>
                                        <td id="alineado"><?php echo $row['plan_est'] ?> </td>
                                        <td id="alineado"><?php echo $row['asignatura'] ?></td>
                                        <td id="gyg"><?php echo $row['semestre'] . " " . "\" " . $row['grupo'] . "\""; ?></td>
                                        <td id="gyg"><?php echo $row['total'] ?></td>
                                    </tr>
                            <?php
                                endif;
                            endwhile;
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td id="total">TOTAL</td>
                                <td id="total"><?php echo $suma; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <p>
                        <b>
                            <?php echo genero_trabajador($genero); ?>
                        </b>ESTÁ DE ACUERDO QUE LAS HORAS EXCEDENTES <b>DE 25 Ó 10 HORAS</b>, SEGÚN SEA EL CASO, SERÁN IMPARTIDAS A <b>TÍTULO HONORÍFICO</b>,
                        DE A CUERDO A LOS TÉRMINOS DE LA LEGISLACIÓN UNIVERSITARIA Y A <b>"LOS LINEAMIENTOS GENERALES PARA LA PLANEACIÓN ACADÉMICA DOCENTE DE LA UNIVERSIDAD AUTÓNOMA DE CHIAPAS"</b>.
                    </p>
                </div>
                <div>
                    <p>
                        <b>TERCERA: "LAS PARTES"</b> contratantes otorgan su consentimiento para que, de conformidad con lo dispuesto por el artículo 59 de la Ley Federal del Trabajo y atendiendo a las necesidades del servicio,
                        <b>"LA UNACH"</b> modifique en cualquier tiempo el horario de trabajo establecido. Por consiguiente, <b>
                            <?php echo genero_trabajador($genero); ?>
                        </b> se obliga a cumplir y respetar los cambios de horarios
                        que le notifique <b>"LA UNACH"</b>.
                    </p>
                </div>
                <div>
                    <p>
                        <b>CUARTA:
                            <?php echo genero_trabajador($genero);
                            ?></b> se obliga a respetar los controles que <b>"LA UNACH"</b> establezca en materia de asistencia,
                        firmando las listas correspondientes o sometiéndose a cualquier otro medio que para tal fin se instituya,
                        presentándose puntualmente a sus labores en el horario de trabajo establecido en la cláusula que precede.
                        En caso de retraso o faltas de asistencia injustificadas, <b>"LA UNACH"</b> podrá imponer a <b><?php
                                                                                                                        echo genero_trabajador($genero);
                                                                                                                        ?></b> las sanciones disciplinarias que correspondan conforme a la Ley Federal del Trabajo,
                        la Legislación Universitaria vigente y aplicable.
                    </p>
                </div>
                <div>
                    <p>
                        <b>QUNTA: "LA UNACH"</b> contrata a <b><?php echo genero_trabajador($genero); ?></b>
                        por tiempo determinado, conforme a lo dispuesto artículo 35, 37 fracción I y demás relativos y aplicables de la Ley Federal del Trabajo y por un periodo, comprendido a partir del
                        <?php echo fechaEs($periodo_ini) . " con fecha de vencimiento al " . fechaEs($periodo_fin) . ","; ?>
                        vigencia que no se podrá prorrogar, lo anterior para impartir las materias mencionadas en la cláusula segunda del presente acuerdo de voluntades, del ciclo escolar
                        <?php echo $desc; ?>
                        en <?php echo nombres_compuestos(gen_depe($depedoc)) . " con " . $suma . " H/S/M."; ?>
                    </p>
                    <p>El presente contrato obliga a las partes a lo expresamente pactado, conforme lo dispuesto por el artículo 31
                        de la Ley Federal del Trabajo, ambas partes convienen en que, al vencimiento del término estipulado, este contrato
                        quedará terminado automáticamente sin responsabilidad para ninguna de las partes, sin necesidad de aviso, ni de ningún
                        otro requisito y cesarán todos sus efectos de acuerdo con la fracción III del artículo 53 de la Ley Federal del Trabajo,
                        es decir sin ninguna responsabilidad para <b>“LA UNACH”</b>, por ser de carácter temporal. </p>
                </div>
                <div>
                    <p>
                        <b>SEXTA: "LA UNACH"</b>, se obliga a pagar al <b>
                            <?php
                            echo genero_trabajador($genero);
                            ?>,
                        </b>los salarios devengados durante el periodo estipulado en la cláusula séptima de este contrato, de conformidad con el tabulador vigente,
                        dicho importe será liquidado consecutivamente los días 15 y 30 de cada mes, durante la vigencia del presente contrato, siendo conforme
                        <b>
                            <?php
                            echo genero_trabajador($genero);
                            ?>
                        </b>en que <b>"LA UNACH"</b> realice las deducciones legales correspondientes.
                    </p>
                </div>
                <div>
                    <p>
                        <b>SÉPTIMA:</b> La vigencia del presente contrato será por el período del <?php echo fechaEs($periodo_ini) . " al " . fechaEs($periodo_fin); ?> del ciclo escolar
                        <?php echo $desc . "."; ?>
                    </p>
                </div>
                <div>
                    <p>
                        Lo anterior es así, ya que la fuente de trabajo es una Institución educativa que se encuentra sujeta a las eventualidades y condiciones siguientes:
                        presupuesto asignado, demanda de servicio, modificación de planes educativos, adaptación de perfiles académicos acorde a los planes educativos,
                        evaluación al personal académico referente a ingreso, promoción y permanencia, acorde a su legislación interna, suspensión de impartición de materias.
                    </p>
                </div>
                <div>
                    <p>
                        En Consecuencia, ambas partes están plenamente conscientes y convienen que al vencimiento del término estipulado de este contrato automáticamente quedará terminado,
                        sin necesidad de aviso ni de ningún otro requisito y cesarán todos los efectos sin responsabilidad para <b>"LA UNACH"</b>.
                    </p>
                </div>
                <div>
                    <p>
                        <b>OCTAVA:
                            <?php echo genero_trabajador($genero); ?></b>
                        se obliga a realizar el trabajo contratado con el más alto profesionalismo, eficiencia, honestidad y honradez, así como a guardar absoluta confidencialidad
                        y discrecionalidad respecto a las actividades que realice, salvo aquélla información que sea considerada de dominio público, reconociendo como obligaciones
                        las que se establecen en este contrato, en la Ley Federal del Trabajo, la Legislación Universitaria vigente y aplicable.
                    </p>
                </div>
                <div>
                    <p>
                        <b>NOVENA:</b>
                        Este contrato podrá rescindirse o darse por terminado anticipadamente, sin responsabilidad para <b>"LA UNACH"</b>, en los casos siguientes:
                    </p>
                </div>
                <div>
                    <div class="contenedor_incisos">
                        <div class="incisos">
                            <p>a) Por incurrir <b>
                                    <?php echo genero_trabajador($genero); ?>
                                </b> en hechos sancionados por el artículo 47; por incumplir las obligaciones que señala el artículo 134 o por incurrir en las prohibiciones
                                que establece el artículo 135, todas las disposiciones de la Ley Federal del Trabajo.
                            </p>
                        </div>
                        <div class="incisos">
                            <p>b) Por las causas que señalan los artículos 53 y 434, o bien por el transcurso del término de este contrato, de conformidad con los
                                numerales 25, fracción II, 35 y 53, todos de la Ley Federal del Trabajo.
                            </p>
                        </div>
                        <div class="incisos">
                            <p>c) La violación por parte de
                                <b> <?php echo genero_trabajador($genero); ?> </b>
                                de cualquiera de las cláusulas de este contrato.
                            </p>
                        </div>
                        <div class="incisos">
                            <p>d) La suspensión o cancelación del contrato que se menciona en la cláusula cuarta de este instrumento.
                            </p>
                        </div>
                    </div>
                </div>
                <div>
                    <p>
                        <b>DÉCIMA:
                            <?php echo genero_trabajador($genero); ?></b>
                        está conforme con que <b>"LA UNACH"</b> queda en libertad de rescindir anticipadamente al término convenido, en el presente Contrato
                        por incumplimiento parcial o total de su contenido, o por ocurrir cualquiera de las causas previstas en el artículo 47 de la Ley Federal del Trabajo.
                    </p>
                </div>
                <div>
                    <p>
                        <b>DÉCIMA PRIMERA: "LAS PARTES"</b> convienen que todo lo no previsto en este contrato se regirá por las disposiciones de la Ley Federal
                        del Trabajo, la Legislación Universitaria vigente y aplicable, y que de surgir controversia con motivo de su interpretación o cumplimiento,
                        se someterán a la jurisdicción y competencia de los Tribunales que, en atención a la materia, sean competentes en la ciudad de Tuxtla Gutiérrez,
                        Chiapas, renunciando al fuero que pudiera corresponderles en razón de su domicilio presente o futuro, o por cualquier otra causa.
                    </p>
                </div>
                <div>
                    <p>
                        <b>DÉCIMA SEGUNDA: "LAS PARTES"</b> manifiestan que en la celebración de este contrato no ha existido engaño, dolo, error, mala fe o vicio alguno
                        del consentimiento que pueda invalidarlo, toda vez que se sustenta en la buena fe y en la equidad, aceptándolo en todas y cada una de sus partes.
                    </p>
                </div>
                <!-- <div style="page-break-after:always;">
                </div> -->
                <div>
                    <p>
                        Leído que fue el presente instrumento y enteradas <b>"LAS PARTES"</b> de su contenido y alcance legal de sus cláusulas, lo firman de conformidad,
                        por duplicado, en la ciudad de Tuxtla Gutiérrez, Chiapas, el día
                        <?php echo fechaEs($periodo_ini); ?>.
                    </p>
                </div>
            </div>


            <div>

               <p id="titulo">
                   <b>"POR LA UNACH"</b>
               </p>
                 
                <div class="contenedor">
                    <div class="hijo1">
                        <b><?php echo "C.P.C. " . mb_strtoupper($nombrefunc4, "UTF-8"); ?></b>
                        <br>
                        <?php echo strtoupper($puestofunc4); ?>
                    </div>
                    <div class="hijo2">
                        <b><?php echo "LIC. " . mb_strtoupper($nombrefunc2, "UTF-8"); ?></b>
                        <br>
                        <?php echo strtoupper($puestofunc2); ?>
                    </div>

                </div>
                <div class="contenedor2">
                    <div class="hijo1">
                        <b><?php echo "MTRO. " . mb_strtoupper($nombrefunc3, "UTF-8"); ?></b>
                        <br>
                        <?php echo strtoupper($puestofunc3); ?>
                    </div>

                    <div class="hijo2">
                        <b><?php echo "MTRO. " . mb_strtoupper($nombrefunc1, "UTF-8"); ?></b>
                        <br>
                        <?php echo strtoupper($puestofunc1); ?>
                    </div>
                </div>
                <div class="contenedor3">
                    <div class="hijo3">
                        <b><?php echo strtoupper($titulodirec) . ". " . mb_strtoupper($nombredirec, "UTF-8"); ?> </b>
                        <br>
                        <?php echo mb_strtoupper($puestodirec); ?>
                        <br>
                        EN <?php echo mb_strtoupper(gen_depe($dirdir)); ?>
                    </div>
                </div>
                <div class="contenedor4">
                    <div class="hijo3">
                    <b><?php echo mb_strtoupper($nombredoc, "UTF-8") . " " . mb_strtoupper($apellidodoc, "UTF-8"); ?></b> <br>
                       "<?php echo genero_trabajador_sin($genero) ?> INTERINO"
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="firmas">
                <p>
                    Las firmas que anteceden corresponden al <b>Contrato de Trabajo por Tiempo Determinado para Cubrir Interinato</b> que celebra la
                    <b>Universidad Autónoma de Chiapas</b> y el <b>C. <?php echo nombres_compuestos($nombredoc) . " " . nombres_compuestos($apellidodoc); ?></b> de fecha <?php echo fechaEs($periodo_ini) . ", "; ?>
                    instrumento que consta de <span id='page'></span> fojas útiles tamaño carta, escrito solo en el anverso, incluida la presente.
                </p>
            </div>
        </footer>
    </main>

</body>

</html>
<?php

$html = ob_get_clean();
// echo $html;

require_once '../../libreria/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;

$dompdf = new Dompdf();

// $dompdf->getOptions();
$options = new Options();
$options->set('isRemoteEnabled', true);

$dompdf->setOptions($options);

$dompdf->loadHtml($html);

// $dompdf->setPaper('letter');
$dompdf->setPaper('A4', 'portrait');

$dompdf->render();

//PARAMETROS
$x          = 505;
$y          = 790;
$text       = "Página {PAGE_NUM} de {PAGE_COUNT}";
$font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
$size       = 10;
$color      = array(0, 0, 0);
$word_space = 0.0;
$char_space = 0.0;
$angle      = 0.0;

$dompdf->getCanvas()->page_text(
    $x,
    $y,
    $text,
    $font,
    $size,
    $color,
    $word_space,
    $char_space,
    $angle
);


$dompdf->stream("contrato_interino_" . $nombredoc . " " . $apellidodoc . ".pdf", array("Attachment" => false));


?>
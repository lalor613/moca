<?php
require '../../administrador/config/bd.php';
    //FUNCIONARIOS//
    $nombrefunc1 = '';
    $dirfunc1 = '';
    $puestofunc1 = '';
    $fnomfunc1 = '';
    $DGDSE = pg_query($conn, "SELECT * FROM funcionarios WHERE id_func=1 ");
    while ($row = pg_fetch_assoc($DGDSE)) {

        // $nombrefunc1 = mb_convert_case(mb_convert_encoding($row['nombre'],"UTF-8"), MB_CASE_UPPER, "UTF-8");
        $nombrefunc1 = mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8");
        $dirfunc1 = mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $puestofunc1 = mb_convert_encoding(mb_convert_case($row['puesto'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $fnomfunc1 = mb_convert_encoding(mb_convert_case($row['f_nom'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
    }

    $nombrefunc2 = '';
    $dirfunc2 = '';
    $puestofunc2 = '';
    $fnomfunc2 = '';
    $AB = pg_query($conn, "SELECT * FROM funcionarios where id_func='2' ");
    while ($row = pg_fetch_assoc($AB)) {
        $nombrefunc2 = mb_convert_encoding(mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $dirfunc2 = mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $puestofunc2 = mb_convert_encoding(mb_convert_case($row['puesto'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $fnomfunc2 = $row['f_nom'];
    }

    $nombrefunc3 = '';
    $dirfunc3 = '';
    $puestofunc3 = '';
    $fnomfunc3 = '';
    $PPS = pg_query($conn, "SELECT * FROM funcionarios WHERE id_func='3' ");
    while ($row = pg_fetch_assoc($PPS)) {
        $nombrefunc3 = mb_convert_encoding(mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $dirfunc3 = mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $puestofunc3 = mb_convert_encoding(mb_convert_case($row['puesto'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $fnomfunc3 = $row['f_nom'];
    }


    $nombrefunc4 = '';
    $dirfunc4 = '';
    $puestofunc4 = '';
    $fnomfunc4 = '';
    $SA = pg_query($conn, "SELECT * FROM funcionarios WHERE id_func='4' ");
    while ($row = pg_fetch_assoc($SA)) {
        $nombrefunc4 = mb_convert_encoding(mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $dirfunc4 = mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $puestofunc4 = mb_convert_encoding(mb_convert_case($row['puesto'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $fnomfunc4 = $row['f_nom'];
    }


    //DIRECTIVO DEPE
    $nombredirec = '';
    $puestodirec = '';
    $dirdir = '';
    $focupardirec = '';
    $titulodirec = '';
    $generodirec = '';
    $DIREC = pg_query($conn, "SELECT * FROM vw_depe_dir WHERE clave_dir='$dependencia' ");
    while ($row = pg_fetch_assoc($DIREC)) {
        $nombredirec =  mb_convert_encoding(mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $puestodirec =  mb_convert_encoding(mb_convert_case($row['puesto'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $dirdir =  mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $titulodirec = $row['titulo'];
        $generodirec = $row['genero'];
        $focupardirec = $row['f_ocupar'];
    }

    function gen_direc($var)
    {
        if ($var == 'Masculino') {
            return "El";
        } elseif ($var == 'Femenino') {
            return "La";
        } else {
            "Falta Especificar";
        }
    }

    function gen_C($var)
    {
        if ($var == 'Masculino') {
            return "EL";
        } elseif ($var == 'Femenino') {
            return "LA";
        } else {
            "Falta Especificar";
        }
    }

    //FUNCION FECHA
    function fechaEs($fecha)
    {
        $fecha = substr($fecha, 0, 10);
        $numeroDia = date('d', strtotime($fecha));
        $dia = date('l', strtotime($fecha));
        $mes = date('F', strtotime($fecha));
        $anio = date('Y', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $meses_ES = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $numeroDia . " de " . $nombreMes . " de " . $anio;
    }
    function gen_depe($var)
    {
        $facu = 'Facultad';
        $esc = 'Escuela';
        $coord = 'Coordinacion';
        $centro = 'Centro';
        $inst = 'Instituto';
        if (!(strpos($var, $facu) === false) or !(strpos($var, $esc) === false) or !(strpos($var, $coord) === false)) {
            return "la $var ";
        } elseif (!(strpos($var, $centro) === false) or !(strpos($var, $inst) === false)) {
            return "el $var";
        } else {
            return "Algo está mal";
        }
    }

    function nombres_compuestos($var)
    {
        $conexiones = array("Del ", "Los ", "El ", "La ","Las ", "De ", "Y ", "En ");
        $sustituto = array("del ", "los ", "el ", "la ","las " ,"de ", "y " ,"en ");
        $nombrebien = str_replace($conexiones, $sustituto, $var);
    
        return $nombrebien;
    }
    
    function nombres_compuestos_escuelas($var)
    {
        $conexiones = array("Del ", "Los ", "El ", "La ","Las ", "De ", "Y ", "En ", "Ii","IIi","Iii","Iv","Vi","Vii","Viii","Ix");
        $sustituto = array("del ", "los ", "el ", "la ","las " ,"de ", "y " ,"en ","II","III","III","IV","VI","VII","VIII","IX");
        $nombrebien = str_replace($conexiones, $sustituto, $var);
    
        return $nombrebien;
    }

    function genero_trabajador($var)
    {

        if ($var == 'Masculino') {
            echo "\"EL TRABAJADOR ACADÉMICO\"";
        } elseif ($var == 'Femenino') {
            echo "\"LA TRABAJADORA ACADÉMICA\"";
        } else {
            echo "No especificado";
        }
    }
    function genero_trabajador_sin($var)
{

    if ($var == 'Masculino') {
        echo "EL TRABAJADOR ACADÉMICO";
    } elseif ($var == 'Femenino') {
        echo "LA TRABAJADORA ACADÉMICA";
    } else {
        echo "No especificado";
    }
}

    function genero_titular($var)
    {

        if ($var == "Masculino") {
            return "al trabajador ";
        } elseif ($var == "Femenino") {
            return "a la trabajadora ";
        } else {
            return "No especificado ";
        }
    }

    //DATOS DEL DOCENTE
    $nombredoc = '';
    $apellidodoc ='';
    $genero = '';
    $grado_acad = '';
    $cedula = 0;
    $otorgado = '';
    $f_otor = '';
    $depedoc = '';
    $desc = '';
    $desc_cort = '';
    $categoria = '';
    $plaza = '';
    $periodo_ini = '';
    $periodo_fin = '';

    $perfil = pg_query($conn, "SELECT * FROM vw_ads_doc WHERE clave_depe='$dependencia' AND idciclo='$ciclo' AND id_doc='$docente'");
    while ($row = pg_fetch_assoc($perfil)) {
        $nombredoc = mb_convert_encoding(mb_convert_case($row['nombre'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $apellidodoc = mb_convert_encoding(mb_convert_case($row['apellido'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $genero = $row['genero'];
        $grado_acad = mb_convert_encoding(mb_convert_case($row['grado_acad'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $cedula = $row['cedula'];
        $otorgado = mb_convert_encoding(mb_convert_case($row['otorgado'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $f_otor = $row['f_otor'];
        $depedoc = mb_convert_encoding(mb_convert_case($row['direccion'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $desc = $row['descripcion'];
        $desc_cort = $row['descripcion_cortado'];
        $periodo_ini = $row['periodo_ini'];
        $periodo_fin = $row['periodo_fin'];
        $plaza = $row['plaza'];
        $categoria= $row['categoria'];
    }

    $asignatura = '';
    $semestre = '';
    $grupo = '';
    $subg = '';
    $total = '';
    $plan_est = '';
    $hrs_t = '';
    $hrs_p = '';

    $suma = 0;

    $asignaturas = pg_query($conn, "SELECT * FROM vw_doc_mate WHERE id_doc='$docente' AND asig_depe='$dependencia' AND asig_ciclo='$ciclo' AND id_tipocarga='$tipo' ");
    $materias = pg_query($conn, "SELECT * FROM vw_doc_mate WHERE id_doc='$docente' AND asig_depe='$dependencia' AND asig_ciclo='$ciclo' AND id_tipocarga='$tipo' ");
    while ($row = pg_fetch_assoc($materias)) {
        $asignatura = mb_convert_encoding(mb_convert_case($row['asignatura'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $plan_est = mb_convert_encoding(mb_convert_case($row['plan_est'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $semestre = $row['semestre'];
        $grupo = $row['grupo'];
        $hrs_t = $row['hrs_t'];
        $hrs_p = $row['hrs_p'];
        $subg = $row['subgrupo'];
        $total = $row['total'];
        $suma = $suma + $total;
    }
    //*datos personales *//
    $curp = '';
    $edad = '';
    $nacionalidad = '';
    $colonia = '';
    $cp = '';
    $calle = '';
    $ciudad = '';
    $Dper = pg_query($conn, "SELECT * FROM d_per WHERE d_id='$docente' ");
    while ($row = pg_fetch_assoc($Dper)) {
        $curp = $row['curp'];
        $edad = $row['edad'];
        $nacionalidad = mb_convert_encoding(mb_convert_case($row['nacionalidad'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $colonia = mb_convert_encoding(mb_convert_case($row['colonia'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $cp = $row['cp'];
        $calle = mb_convert_encoding(mb_convert_case($row['calle'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
        $ciudad = mb_convert_encoding(mb_convert_case($row['ciudad'], MB_CASE_TITLE, "UTF-8"), "UTF-8");
    }

    //datos de la descarga
    $desc_asig = pg_query($conn, "SELECT nombre, apellido, genero, nombre_motivo FROM vw_desc_asig WHERE id_contrato='$id_contrato' ");
    $desc_cant = pg_num_rows($desc_asig);
    $descargas = array();
    $titular = array();
    while ($row = pg_fetch_assoc($desc_asig)) {
        $descargas[] = $row;
        $titular[] = $row;
    }
    
    

   
    //IMAGEN
    $imagen = pg_query($conn, "SELECT * FROM imagenes WHERE id='1' ");


    function titulos_cedula($grado_acad){
        $lic = 'Licenciatura';
        $ing = 'Ingeniería';
        $mtr = 'Maestría';
        $doc = 'Doctorado'; //Para evitar los acentos
        $grad = strpos($grado_acad,$lic);
        $gd_ing = strpos($grado_acad,$ing);
        $maes = strpos($grado_acad,$mtr);
        $gd_doc = strpos($grado_acad,$doc);
    
        if( $grad !== false || $gd_ing !== false)
        {
            echo "título profesional de ". nombres_compuestos($grado_acad);
        }else if( $maes!==false || $gd_doc !== false){
            echo "el grado de ". nombres_compuestos($grado_acad);
        }else{
            echo "<b>colocar bien el grado</b>";
        }
    }

<?php
$page_title = 'Historial Docente';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    header("Location: ../secciones/inicio.php");
} else {
    header("Location: ../index.php");
}
?>
<?php
require('../administrador/config/bd.php');
$docente = pg_query($conn, "SELECT id, nombre, apellido FROM docentes");
?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Buscar Docente</h4>
    </div>
    <div class="card-body">
        <form action="javascript:void(0);" method="post">
            <div class="row">
                <label for="doc">Ingresa el nombre</label>
                <select name="doc" id="doc" class="form-control-sm form-control js-example-basic-single">
                    <option value="0" selected>--Elige a un docente--</option>
                    <?php while ($row = pg_fetch_assoc($docente)) {
                        echo "<option value=" . $row['id'] . ">" . $row['apellido'] . " " . $row['nombre'] . "</option>";
                    } ?>
                </select>
            </div>
            <br>
            <div class="row">
                <button type="button" onclick="general();" class="btn btn-primary btn-md btn-block">Buscar</button>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="card-group">
                <div class="card">
                    <strong>

                        <div class="card-body">
                            <h4 class="card-title">Datos personales</h4>
                            <div class="row">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Nombre:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="nombre"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Genero:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="genero"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Edad:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="edad"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Curp:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="curp"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Nacionalidad:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="nacionalidad"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Ciudad:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="ciudad"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Colonia:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="colonia"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Calle:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="calle"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        CP:
                                    </div>
                                    <div class="col-sm-7">
                                        <div id="cp"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </strong>
                </div>
                <div class="card">
                    <strong>

                        <div class="card-body">
                            <h4 class="card-title">Datos Profesionales</h4>

                            <div class="row">
                                <div class="col-sm-3">
                                    Categoria:
                                </div>
                                <div class="col-sm-7">
                                    <div id="categoria"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Sindicato:
                                </div>
                                <div class="col-sm-7">
                                    <div id="sindicato"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Plaza
                                </div>
                                <div class="col-sm-7">
                                    <div id="plaza"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Perfil:
                                </div>
                                <div class="col-sm-7">
                                    <div id="grado_acad"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Cedula:
                                </div>
                                <div class="col-sm-7">
                                    <div id="cedula"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Otorgado por:
                                </div>
                                <div class="col-sm-7">
                                    <div id="otorgado"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    Obtenido:
                                </div>
                                <div class="col-sm-7">
                                    <div id="f_otor"></div>
                                </div>
                            </div>

                        </div>
                    </strong>
                </div>
            </div>

        </div>
        <div class="row">
            <di0v class="card-columns">
                <div class="card">
                    <div class="card-body table-wrapper-scroll-x my-custom-scrollbar">
                        <h4 class="card-title">Adscripcion</h4>
                        <table id="mostrar_adscripcion" class="table table-lg table-bordered table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Prog</th>
                                    <th>Dependencia</th>
                                    <th>Ciclo escolar</th>
                                    <th>periodo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body table-wrapper-scroll-x my-custom-scrollbar">
                        <h4 class="card-title">Contratos</h4>
                        <table id="mostrar_contratos" class="table table-lg table-bordered table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Prog</th>
                                    <th>Tipo</th>
                                    <th>Dependencia</th>
                                    <th>Ciclo escolar</th>
                                </tr>
                            </thead>
                            <tbody class='justify-content-center align-items-center'>
                                <tr>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>

</div>
</div>

<script src="../js//historial_d.js"></script>



<!-- Modal Cambio de Periodo Laboral-->
<div class="modal fade" id="modal_periodo" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <h5 class="modal-title">Periodo Laboral</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <strong>
                        <div id='unidad_acad'> </div>
                        <input type="number" hidden id='id_doc'>
                        <input type="number" hidden id='id_ciclo'>
                        <input type="number" hidden id='id_ua'>
                    </strong>
                    <div class="row">
                        <div class="col-md-6">
                             Docente: 
                             <small>
                                <div id='nom_doc' style="text-transform:uppercase;"> </div>
                             </small>
                        </div>
                        <div class="col-md-6">
                            Ciclo escolar:
                                <small>
                                    <div id='descripcion' style="text-transform:uppercase;">  </div>
                                </small>
                        </div>
                    </div>
                   
                    <hr class="my-2">
                
                    <br>
                    <form action="javascript:void(0);" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="periodo_ini"> Inicio del periodo laboral </label>
                                    <input type="date" class="form-control" name="periodo_ini" id="periodo_ini" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="periodo_fin"> Final del periodo laboral </label>
                                    <input type="date" class="form-control" name="periodo_fin" id="periodo_fin" >
                                </div>

                            </div>
                        </div>
                       

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            <button type="button" onclick='actualizarperiodo();' class="btn btn-success">Guardar</button>
                        </div>

                    </form>

                </div>
            </div>
           
        </div>
    </div>
</div>

modal

<?php
include('../templates/pie.php');
?>
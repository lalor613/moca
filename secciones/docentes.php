<?php
$page_title = 'Añadir docentes';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclos = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>
<link rel="stylesheet" href="../css/estilobarra.css">

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Añadir docentes</h4>
        <div class="progress" id="progress">
            <div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Progreso</div>
        </div>
    </div>
    <div class="card-body">

        <div class="col-md-12">
            <form action="javascript:void(0);" method="post">
                <div class="form-group">
                    <label for="excel-input">Selecciona el Excel</label>
                    <input type="file" class="form-control btn-secondary" name="excel-input" id="excel-input">
                    <small id="fileHelpId" class="form-text text-muted">El excel se extrae del SIPAD</small>
                </div>

                <div class="row m-1">
                    <div class="col form-group">
                        <label for="doc_ciclo">Ciclo escolar</label>
                        <select class="form-control form-control-sm" name="doc_ciclo" id="doc_ciclo">
                            <option value="0" selected>--Elige un ciclo escolar--</option>
                            <?php while ($row = pg_fetch_array($ciclos)) {
                                echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                            } ?>
                        </select>
                    </div>

                    <div class="col form-group" id="doc_depe">
                    </div>
                </div>
                <div class="form-group m-3">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="row">
                                <button type="submit" id="btn_guardar" class="btn btn-primary btn-md btn-block">Añadir</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                        <div class="row">
                            <button type="submit" id="btn_borrar" class="btn btn-danger btn-sm btn-block">
                                <span class="material-symbols-outlined">
                                    delete
                                </span>
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
            </form>

        </div>

    </div>
    <div class="card-footer text-muted justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
        <table class="table table-inverse table-inverse table-responsive" id="tabla-excel">
            <thead class="thead-inverse">
                <tr>
                    <th>N°</th>
                    <th>Id</th>
                    <th>Genero</th>
                    <th>Nombre</th>
                    <th>Plaza</th>
                    <th>Sindicato</th>
                    <th>HD contrato</th>
                    <th>HT contrato</th>
                    <th>categoria</th>
                    <th>HD</th>
                    <th>HT</th>
                    <th>HTD</th>
                    <th>HI</th>
                    <th>Grado Académico</th>
                </tr>
            </thead>
            <tbody>
                <tr>

                </tr>
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript" src="../js/docentes_excel.js"></script>

<?php include('../templates/pie.php'); ?>
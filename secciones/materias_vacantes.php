<?php
$page_title = 'Materias Vacantes';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php
require("../administrador/config/bd.php");

$ciclos = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Materias Vacantes</h4>
        <hr class="my-2">

        <form action="javascript:void(0);" method="POST">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ciclo_esc"></label>
                        <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                            <option selected value='0'>--Elige Ciclo Escolar--</option>
                            <?php
                            while ($row = pg_fetch_assoc($ciclos)) {
                                echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 form-group" id="id_depe">

                </div>
            </div>
            <div class="form-group m-3">
                <div class="row">
                    <button type="button" onclick="buscarMaterias();" class="btn btn-primary btn-md btn-block">Buscar</button>
                </div>
            </div>
        </form>

    </div>
    <div class="card-body">
        <ul>
            <li><input name="materias[]" type="checkbox" value=0 /></li>
            <li><input name="materias[]" type="checkbox" value=1 /></li>
        </ul>
    </div>
    <div class="card-footer">
        <table class="table table-striped table-responsive" id="Tabla_materias">
            <thead class="thead-inverse">
                <tr>
                    <th>PROG</th>
                    <th>Asignatura</th>
                    <th>Semestre</th>
                    <th>Grupo</th>
                    <th>Total</th>
                </tr>
            </thead>


            <div class="row">
                <div class="form-check form-check-inline">
                    <button type="button" onclick="mover();" class="btn btn-warning btn-md btn-block">Mover</button>
                </div>
            </div>

            <tbody>

            </tbody>


        </table>
    </div>
    <div id="cargando">
        <!-- Respuesta AJAX -->
    </div>
</div>



<!-- Modal-cambio de materias -->
<div class="modal fade" id="modal_materias" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mover materias</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javasript:void(0);" method="post">
                    <input type="number" id='id_mate' hidden>
                    <div class="row">
                        <div class="col-md-7">
                            Materias Seleccionadas:
                        </div>
                        <div class="col-md-3">
                            <div id='numates'> </div>
                            <br>
                            <div id="ciclo"></div>
                        </div>
                    </div>
                    <hr class="my-2">
                    <div class="form-group">
                        <label for="">Elige dependencia</label>
                        <select class="form-control form-control-sm" name="" id="">
                          
                        </select>
                        <small>Las Materias aparecerán como vacantes en la dependencia seleccionada</small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success">Mover</button>
            </div>
        </div>
    </div>
</div>


<script src="../js/materias_vacantes.js"></script>

<?php include('../templates/pie.php'); ?>
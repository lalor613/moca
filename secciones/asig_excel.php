<?php
$page_title = 'Subir Asignaturas';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    header("Location: ../secciones/inicio.php");
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>
<link rel="stylesheet" href="../css/estilobarra.css">

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Cargas académicas</h4>
        <div class="progress" id="progress">
            <div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Progreso</div>
        </div>
    </div>
    <div class="card-body">

        <form action="javascript:void(0);" method="post">
            <div class="form-group">
                <div class="form-group">
                    <label for="excel-input">Selecciona el Excel </label>
                    <input type="file" class="form-control" name="excel-input" id="excel-input">
                    <small id="fileHelpId" class="form-text text-muted">El excel se extrae del SIPAD</small>
                </div>
            </div>
            <div class="row m-1">

                <div class="col form-group">
                    <label for="ciclo_esc">Ciclo Escolar</label>
                    <select class="form-control form-control-sm" name="ciclo_esc" id="ciclo_esc">
                        <option selected value=0>--Elige un ciclo escolar--</option>
                        <?php
                        while ($row = pg_fetch_assoc($ciclo)) {
                            echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="col form-group" id="depe_asig">

                </div>
            </div>
            <div class="form-group m-3">
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <button type="submit" id="btn_guardar" class="btn btn-primary btn-md btn-block">Guardar</button>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <button type="submit" id="btn_borrar" class="btn btn-danger btn-sm btn-block">
                                <span class="material-symbols-outlined">
                                    delete
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <div class="card-footer text-muted">
        <table class="table" id="tabla-excel">
            <thead>
                <tr>
                    <th>Prog.</th>
                    <th>ID</th>
                    <th>Asignatura</th>
                    <th>Semestre</th>
                    <th>Grupo</th>
                    <th>SugG</th>
                    <th>HT</th>
                    <th>HP</th>
                    <th>Plan de Estudios</th>
                    <th>Docente Asignado</th>
                </tr>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
</div>

<script src="../js/asig_excel.js"></script>

<?php include('../templates/pie.php'); ?>
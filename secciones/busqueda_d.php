<?php
$page_title = 'Busqueda Docente';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    include('../templates/cabecera2.php');
} else {
    header("Location: ../index.php");
}
?>
<?php require '../administrador/config/bd.php';
$ciclo = pg_query($conn, "SELECT * FROM ciclos_escolares");
?>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Busqueda de docentes</h4>
    </div>
    <div class="card-body">

        <form action="javascript:void(0);" method="get">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ciclo_esc">Ciclo de búsqueda</label>
                        <select class="form-control form-control-sm js-example-basic-single" name="ciclo_esc" id="ciclo_esc">
                            <option value=0 selected>--Elegir Ciclo Escolar--</option>
                            <?php
                            while ($row = pg_fetch_assoc($ciclo)) {
                                echo "<option value=" . $row['id_ciclo'] . ">" . $row['descripcion'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 form-group" id="doc_depe">

                </div>
                <strong>
                    <div class="col-sm-12 m-3" id="direccion">

                    </div>
                </strong>
            </div>
            <div class="form-group m-3">
                <div class="row">
                    <button type="button" onclick="obtenerDocentes();" class="btn btn-primary btn-md btn-block">Buscar</button>
                </div>
            </div>

        </form>

    </div>
    <div class="card-footer text-muted justify-content-center table-wrapper-scroll-x my-custom-scrollbar">
        <table class="table table-striped table-inverse table-responsive" id="tabla_docentes">
            <thead class="thead-inverse">
                <tr>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Plaza</th>
                    <th>HD</th>
                    <th>HT</th>
                    <th>HTD</th>
                    <th>HI</th>
                    <th>Datos Profesionales</th>
                    <th>Datos Personales</th>
                </tr>
            </thead>
            <tbody>
                <tr>

                </tr>
            </tbody>
        </table>
    </div>
</div>


<!-- MODAL DE DATOS PROFESIONALES -->
<div class="modal fade" id="modal_edit_per" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DATOS ACADÉMICOS</h5>
                <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0);" method="post">
                <div class="modal-body">
                    <div class="container-fluid align-items-center">
                        <!-- <div class="row">
                            <div class="col-sm-3">
                                
                            </div>
                            <div class="col-sm-6">
                                Nombre del fulano
                            </div>
                            <div class="col-sm-3">
                                Plaza si es posible
                            </div>
                        </div> -->
                    </div>

                    <div class="form-group">
                        <label for="upnombre">Nombre</label>
                        <input type="text" class="form-control" name="upnombre" readonly id="upnombre">
                    </div>
                    <div class="container align-items-center">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="upid">ID</label>
                                <input type="number" class="form-control" id="upid" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label for="upplaza">Plaza</label>
                                <input type="number" class="form-control" id="upplaza" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="categoria">Categoría</label>
                                <input type="text" class="form-control" id="categoria" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="container align-items-center form-group">
                        <h5><span class="badge bg-secondary">Horas Cargadas</span></h5>
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="hd">HD</label>
                                <input type="number" class="form-control" readonly id="hd">
                            </div>
                            <div class="col-sm-3">
                                <label for="ht">HT</label>
                                <input type="number" class="form-control" readonly id="ht">
                            </div>
                            <div class="col-sm-3">
                                <label for="htd">HTD</label>
                                <input type="number" class="form-control" readonly id="htd">
                            </div>
                            <div class="col-sm-3">
                                <label for="hi">HI</label>
                                <input type="number" class="form-control" readonly id="hi">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upgen">Género</label>
                                    <input type="text" class="form-control" name="upgen" id="upgen" readonly>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="grado_acad">Perfil Académico</label>
                                <input type="text" class="form-control" id="grado_acad">
                            </div>
                            <div class="col-md-12">
                                <label for="">Expedido por:</label>
                                <input type="text" class="form-control" id="otorgado">
                            </div>
                            <div class="col-sm-5">
                                <label for="">Fecha de obtención</label>
                                <input type="date" class="form-control" id="f_otor">
                            </div>
                            <div class="col-sm-5">
                                <label for="cedula">Cedula Profesional</label>
                                <input type="number" class="form-control" id="cedula">
                                <small>Debe coincidir con el Perfil Académico</small>
                            </div>

                        </div>

                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="actualizar();" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal de datos Personales -->
<div class="modal fade" id="modal_docente_" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DATOS PERSONALES</h5>
                <button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0);" method="post">

                <div class="modal-body">
                    <div class="container-fluid align-items-start">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="number" hidden class="form-control" id="id_personal">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <label for="">Curp</label>
                                <input type="text" class="form-control" id="curp">
                            </div>
                            <div class="col-md-3 offset-md-2">
                                <label for="">Edad</label>
                                <input type="number" readonly class="form-control" id="edad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Nacionalidad</label>
                                <input type="text" class="form-control" id="nacionalidad">
                            </div>
                            <div class="col-md-8">
                                <label for="">Ciudad</label>
                                <input type="text" class="form-control" id="ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Colonia</label>
                                <input type="text" class="form-control" id="colonia">
                            </div>
                            <div class="col-md-8">
                                <label for="">Calle</label>
                                <input type="text" class="form-control" id="calle">
                                <small>Incluye número de casa</small>

                            </div>
                            <div class="col-md-6">
                                <label for="">Código Postal</label>
                                <input type="number" class="form-control" name="" id="cp">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="actualizarPers();" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="../js/docentes.js"></script>
<?php include('../templates/pie.php'); ?>
<?php
$page_title = 'Administrador';
session_start();
if ($_SESSION['tipo_id'] == 1) {
    include('../templates/cabecera.php');
} elseif ($_SESSION['tipo_id'] == 2) {
    header("Location: ../secciones/inicio.php");
} else {
    header("Location: ../index.php");
}
?>
<?php
require '../administrador/config/bd.php';
$depe = pg_query($conn, "SELECT * FROM dependencias");
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="card-title">Usuarios por dependencia</h4>
                <p class="card-text">Secretarios Académicos</p>
            </div>
            <div class="col-sm-4 m-3">
                <button type="button" class="btn btn-primary btn-md btn-block" data-toggle="modal" data-target="#modal_usuario">
                    Añadir
                </button>
            </div>
        </div>

    </div>
    <div class="card-body table-wrapper-scroll-x my-custom-scrollbar">
        <table class="table table-bordered table-responsive" id="tabla_usuarios">
            <thead class="thead-inverse">
                <tr>
                    <th>PROG</th>
                    <th>Usuario</th>
                    <th>Dependencia</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>

                </tr>
            </tbody>
        </table>
    </div>
    <div class="card-footer text-muted">

    </div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="modal_usuario" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Añadir Secretario Académico</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0)" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="depe">Dependencia A Cargo</label>
                                <input placeholder="--Elige dependencia--" require list="dependencias" name="depe" id="depe" class="form-control form-control-sm">
                                <datalist id="dependencias">
                                    <?php
                                    while ($row = pg_fetch_array($depe)) {
                                        echo "<option value=" . $row['clave'] . ">" . $row['direccion'] . "</option>";
                                    }
                                    ?>
                                </datalist>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_user">Nombre del Usuario</label>
                                <input type="text" name="nombre_user" id="nombre_user" class="form-control" placeholder="Nombre completo" aria-describedby="helpId">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="correo">Correo del Usuario</label>
                                <input type="text" name="correo" id="correo" require class="form-control" placeholder="Correo unach" aria-describedby="helpId">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label for="contra">Contraseña</label>
                            <input type="text" name="contraseña" id="contraseña" require class="form-control">
                            <small>Con esta contraseña accederá el Secretario/Auxiliar</small>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="numero">Numero de celular</label>
                                <input type="number" name="numero" id="numero" class="form-control" placeholder="Número de celular" aria-describedby="helpId">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btn_adduser" onclick="agregar_usuario();" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js//usuarios.js"></script>

<?php include('../templates/pie.php'); ?>
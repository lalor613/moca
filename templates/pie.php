</div>
</body>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>

<script>
    $(".alert").alert();
    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("navegador");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    const target = document.querySelector('p[id="titulo"] span');
    if (window.navigator.onLine) {
        setOnline();
    } else {
        setOffline();
    }

    window.addEventListener('online', () => {
        setOnline();
    });
    window.addEventListener('offline', () => {
        setOffline();
    });

    function setOnline() {
        target.textContent = 'Conectado';
        target.style.color = 'green';
    }

    function setOffline() {
        target.textContent = 'Desconectado';
        target.style.color = 'red';
    }

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

</script>

</html>
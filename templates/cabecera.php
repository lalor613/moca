<?php

if (isset($_SESSION['correo'])) {
} else {
    header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title ?></title>
    <script src="https://unpkg.com/read-excel-file@5.x/bundle/read-excel-file.min.js"></script>


    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,700,0,200" />
    <script src="../libreria/jquery-3.6.0.min.js"></script>
    <script src="../libreria/materialize.min.js"></script>
    <!-- <script src="../library/bootstrap-select.js"></script> -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    </script>

    <style>
        .cabecera {
            padding: 10px 16px;
            position: relative;
            z-index: 100;
        }

        .sticky {
            position: fixed;
            top: 0;
            width: 100%;
        }

        .sticky+.container-fluid {
            padding-top: 102px;
        }
    </style>
</head>

<body>

    <header class="p-3 text-white justify-content-center align-items-center" style="background-color: #18386B;">
        <div class="row">
            <div class="col-md-4">
                <div>
                    <img src="../img/cabecera_unach.jpeg" width="350px" height="110px" class="align-center">
                </div>
            </div>
            <div class="col-md-6 justify-content-center align-items-center">
                <div class="justify-content-center align-items-center text-center">
                    <span class="d-inline-block p-2 font-wight-bold">DIRECCIÓN GENERAL DE DOCENCIA Y SERVICIOS ESCOLARES</span>
                </div>
                <div class="justify-content-center align-items-center text-center">
                    <span class="d-inline-block p-2 font-wight-bold">MODULO DE CONTRATACIÓN ACADÉMICA</span>
                </div>

            </div>
            <div class="col-md-2">
                <div>
                    <img src="../img/DGDSE UNACH 3.png" width="100px" height="100px" class="align-middle">
                </div>
            </div>
        </div>
    </header>
    <?php $url = "http://" . $_SERVER['HTTP_HOST'] . "/moca/"; ?>
    <nav id="navegador" class="cabecera navbar navbar-dark navbar-expand-lg justify-content-center justify-text-center align-items-center" style="background-color: #d2a92d;">

        <a class="navbar-brand" href="#">MOCA</a>

        </div> <button class="navbar-toggler d-lg-none" style="background-color: #002E63;" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"></button>
        <div class="collapse navbar-collapse justify-content-center " id="collapsibleNavId">

            <a class="btn" type="button" style="color:black;" href="<?php echo $url; ?>secciones/inicio.php">Inicio</a>

            <div class="dropdown open">
                <button class="btn dropdown-toggle" style="color:black;" type="button" id="triggerId" data-toggle="dropdown">
                    Procesos
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <a href="<?php echo $url; ?>secciones/apertura_dep.php">
                        <button class="dropdown-item">Apertura de Ciclos</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/docentes.php">
                        <button class="dropdown-item">Agregar Docentes(excel)</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/asig_excel.php">
                        <button class="dropdown-item">Subir Materias(excel)</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/carga.php">
                        <button class="dropdown-item">Carga Académica </button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/descargas.php">
                        <button class="dropdown-item">Motivo Descarga </button>
                    </a>
                </div>
            </div>
            <div class="dropdown open">
                <button class="btn dropdown-toggle" style="color:black;" type="button" id="triggerId" data-toggle="dropdown">
                    Docentes
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <a href="<?php echo $url; ?>secciones/busqueda_d.php">
                        <button class="dropdown-item">Busqueda Docente</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/historial_d.php">
                        <button class="dropdown-item">Historial Docente</button>
                    </a>
                </div>
            </div>
            <a class="btn" type="button" style="color:black;" href="<?php echo $url; ?>secciones/contratos.php">Contratos</a>
            <div class="dropdown open">
                <button class="btn dropdown-toggle" style="color:black;" type="button" id="triggerId" data-toggle="dropdown">
                    Directorio
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <a href="<?php echo $url; ?>secciones/dependencias.php">
                        <button class="dropdown-item">Dependencias</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/directivos.php">
                        <button class="dropdown-item">Directivos</button>
                    </a>
                </div>
            </div>

            <div class="dropdown open">
                <button class="btn dropdown-toggle" style="color:black;" type="button" id="triggerId" data-toggle="dropdown">
                    Administrador
                </button>
                <div class="dropdown-menu" aria-labelledby="triggerId">
                    <a href="<?php echo $url; ?>secciones/administrador.php">
                        <button class="dropdown-item">Usuarios</button>
                    </a>
                    <a href="<?php echo $url; ?>secciones/materias_vacantes.php">
                        <button class="dropdown-item">Materias Vacantes</button>
                    </a>
                </div>
            </div>


            <div>
                <a class="btn" type="button" style="color:black;" href="<?php echo $url; ?>/cerrar.php">Salir</a>
            </div>
            <!-- <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> -->

        </div>
    </nav>
    <strong>

        <div class="row">
            <div class="col-sm-3">
                <p id="titulo">
                    Estas: <span></span>
                </p>
            </div>
            <div class="col-sm-3">
                <p class="mb-0"><?php echo $_SESSION['nombre_user']; ?></p>
            </div>
            <div class="col-sm-3">
                <p class="mb-0"><?php echo $_SESSION['correo']; ?></p>
                <input type="number" hidden name="tipo_user" id="tipo_user" value="<?php echo $_SESSION['tipo_id']; ?>">
            </div>
        </div>
    </strong>

    <div class="container-fluid">
        <br>
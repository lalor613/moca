var ac_d = [];
var url = "../administrador/config/apertura_dep.php";

$(document).ready(function () {
    obtenerAperturas();
});
function datostabla() {
    $('#tabla_aperturas').DataTable({
        "search":true
    });
}
datostabla();
function agregarApertura(ac_depe) {


    var ac_depe = $('#ac_depe').val();
    var ac_ciclo = $('#ac_ciclo').val();
    document.getElementById('btn_crear').innerHTML = 'Creando...';

    var datosEnviar = JSON.stringify({ ac_depe: ac_depe, ac_ciclo: ac_ciclo });
    $.ajax({
        url: url,
        type: "POST",
        data: datosEnviar,
        dataType: 'json',
        success: function (data, status) {
            swal.fire({
                'title': 'Creación exitosa',
                'type': 'success',
                'icon': 'success',
            })
            document.getElementById('btn_crear').innerHTML = 'Crear';
            $('#modal_depe').modal('hide');
            obtenerAperturas();
        },
        error: function (status) {
            swal.fire({
                'title': 'Creación denegada',
                'type': 'danger',
                'icon': 'error',
            })
            console.log(datosEnviar);
            console.log(status);
            document.getElementById('btn_crear').innerHTML = 'Crear';
        }
    });
}

function obtenerAperturas() {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data, status) {
            ac_d = data;
            llenarTablaApertura();
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })
}


function llenarTablaApertura() {
    document.querySelector('#tabla_aperturas tbody').innerHTML = '';
    for (let i = 0; i < ac_d.length; i++)
        document.querySelector('#tabla_aperturas tbody').innerHTML +=
            `
        <tr>
            <td>${ac_d[i].id_ciclo}</td>
            <td>${ac_d[i].direccion}</td>
            <td>${ac_d[i].clave}</td>
        </tr>
        `
}
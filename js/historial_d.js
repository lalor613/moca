function general(){
    contratos_obtener();
    adscripcion_obtener();
    datos_personales_obtener();
    datos_profesionales();
}
datos_p=[];
function datos_personales_obtener(){
    let doc = $('#doc').val();

    let datosEnviar = JSON.stringify({doc:doc});

    $.ajax({
        url:'../administrador/config/docentes.php' + '?mostrar=1',
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success:function(data){
            datos_p = data;
            llenardatos();
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });
}

function llenardatos(){
    document.querySelector('#nombre').innerHTML= datos_p[0].nombre;
    document.querySelector('#genero').innerHTML= datos_p[0].genero;
    
    document.querySelector('#categoria').innerHTML= datos_p[0].categoria;
    document.querySelector('#sindicato').innerHTML= datos_p[0].sindicato;
    document.querySelector('#plaza').innerHTML= datos_p[0].plaza;
    document.querySelector('#grado_acad').innerHTML= datos_p[0].grado_acad;
    document.querySelector('#cedula').innerHTML= datos_p[0].cedula;
    document.querySelector('#otorgado').innerHTML= datos_p[0].otorgado;
    document.querySelector('#f_otor').innerHTML= datos_p[0].f_otor;
    
}
datos=[];
function datos_profesionales(){
    let doc = $('#doc').val();
    let datosEnviar = JSON.stringify({doc:doc});

    $.ajax({
        url:'../administrador/config/datos_per.php' + '?mostrar=1',
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success:function(data){
            datos = data;
            llenardatos_per();
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });
}

function llenardatos_per(){
    document.querySelector('#curp').innerHTML= datos[0].curp;
    document.querySelector('#edad').innerHTML= datos[0].edad;
    document.querySelector('#nacionalidad').innerHTML= datos[0].nacionalidad;
    document.querySelector('#ciudad').innerHTML= datos[0].ciudad;
    document.querySelector('#colonia').innerHTML= datos[0].colonia;
    document.querySelector('#calle').innerHTML= datos[0].calle;
    document.querySelector('#cp').innerHTML= datos[0].cp;

}


contratos =[];
function contratos_obtener(){
    let doc = $('#doc').val();
    let datosEnviar = JSON.stringify({doc:doc});

    $.ajax({
        url:'../administrador/config/contratos.php' + '?mostrar=1',
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success:function(data){
            contratos = data;
            llenartablacontratos();
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });
}

function llenartablacontratos(){
    document.querySelector('#mostrar_contratos tbody').innerHTML = '';
    for(let i = 0; i < contratos.length; i++)
    document.querySelector('#mostrar_contratos tbody').innerHTML +=
    `
    <td> ${i+1} </td>
    <td> ${contratos[i].nombre_tipo} </td>
    <td> ${contratos[i].direccion} </td>
    <td> ${contratos[i].c_ciclo} </td>
    <td>
    <button class="btn btn-outline-danger btn-sm btn-block" onclick='borrarcontrato(${contratos[i].id_cont});' >
        <span class="material-symbols-outlined">
            delete
        </span>
    </button>
    </td>
    `;
}
adscripcion=[];
function adscripcion_obtener(){
    let doc = $('#doc').val();
    let datosEnviar = JSON.stringify({doc:doc});

    $.ajax({
        url:'../administrador/config/docentes_cicdep.php' + '?obtener=1',
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success:function(data){
            adscripcion = data;
            adscripcion_mostrar();
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });
}

function adscripcion_mostrar(){
    document.querySelector('#mostrar_adscripcion tbody').innerHTML = '';
    for(let i = 0; i < adscripcion.length; i++)
    document.querySelector('#mostrar_adscripcion tbody').innerHTML +=
    `
    <td> ${i+1} </td>
    <td> ${adscripcion[i].direccion} </td>
    <td> ${adscripcion[i].descripcion} </td>
    <td> ${adscripcion[i].periodo_ini} - ${adscripcion[i].periodo_fin}</td>
    <td> 
    <button id='mod_periodo' onclick='modificarperiodo(${adscripcion[i].id_doc}, ${adscripcion[i].id_ciclo}, ${adscripcion[i].clave_depe})' type='button' class="btn btn-outline-dark btn-sm btn-block" >
        <span class="material-symbols-outlined">
            edit_calendar
        </span>
    </button>
    </td>
    `;
}


function modificarperiodo(id, ciclo, depe){



    $.post('../administrador/config/docentes_cicdep.php', {id:id, ciclo_esc:ciclo, depe_ua:depe}, function(data, status){
        let doc = data;
        document.querySelector('#unidad_acad').innerHTML = doc.direccion;
        document.querySelector('#nom_doc').innerHTML = doc.nombre + " " + doc.apellido;
        document.querySelector('#descripcion').innerHTML = doc.descripcion;
        $('#periodo_ini').val(doc.periodo_ini);
        $('#periodo_fin').val(doc.periodo_fin);

        $('#id_doc').val(doc.id_doc);
        $('#id_ciclo').val(doc.idciclo);
        $('#id_ua').val(doc.clave_depe);
    });

    $('#modal_periodo').modal('show');


}

function actualizarperiodo()
{
    let id = $('#id_doc').val();
    let ciclo = $('#id_ciclo').val();
    let depe = $('#id_ua').val();
    let per_ini = $('#periodo_ini').val();
    let per_fin = $('#periodo_fin').val();

    $.post('../administrador/config/docentes_cicdep.php',{
        id_update:id,
        ciclo_update:ciclo,
        depe_update: depe,
        periodo_ini:per_ini,
        periodo_fin:per_fin,
    }, function(data,status){
        console.log(data);
        console.log(status);
        adscripcion_obtener();
    })
    .fail(function(data,status){
        swal.fire({
            'title': 'No se actualizó el periodo',
            'type': 'danger',
            'icon': 'error',
        })
        // alert("No se actualizó el periodo");
        console.log(data);
        console.log(status);
    })
    .always(function(){
        $('#modal_periodo').modal('hide');
    })
}

function borrarcontrato(id)
{
    // let  confirmation = confirm("¿Desea eliminar éste contrato?");
    // if(confirmation)
    // {
   
    // }else{
    //     alert("Acción cancelada");
    // }

    Swal.fire({
        title:'¿Desea eliminar éste contrato?',
        showCancelButton:true,
        confirmButtonColor:'#1FAB45',
        confirmButtonText:"Sí, eliminar",
        cancelButtonText:"Cancelar",
        buttonStyling: true
    }).then(function(){
        $.ajax({
            url:"../administrador/config/contratos.php" + `?borrar=${id}`,
            type: 'DELETE',
            dataType:'json',
            success: function(data, status)
            {
                Swal.fire({
                    'title': 'Se eliminó el contrato',
                    'type' : 'success',
                    'icon' : 'success'
                })
                contratos_obtener();
            },
            error: function(data, status)
            {
                Swal.fire({
                    'title': 'No se pudo eliminar',
                    'type' : 'danger',
                    'icon' : 'error'
                })
            }
        })
    })




}

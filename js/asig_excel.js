class Excel {
  constructor(content) {
    this.content = content;
  }
  header() {
    return this.content[1];
  }
  rows() {
    return new RowCollection(this.content.slice(2, this.content.length));
  }
}
class RowCollection {
  constructor(rows) {
    this.rows = rows;
  }
  get(index) {
    return new Row(this.rows[index]);
  }
  count() {
    return this.rows.length;
  }
}

class Row {
  constructor(row) {
    this.row = row;
  }
  id() {
    return this.row[1];
  }
  plan_est() {
    return this.row[2];
  }
  asignatura() {
    return this.row[3];
  }
  semestre() {
    return this.row[4];
  }
  grupo() {
    return this.row[5];
  }
  subgrupo() {
    return this.row[6];
  }
  hrs_t() {
    return this.row[8];
  }
  hrs_p() {
    return this.row[9];
  }

  id_doc() {
    return this.row[19];
  }
  nombre_titu() {
    return this.row[20];
  }
  id_inte() {
    return this.row[17];
  }
}

class ExcelPrinter {
  static print(tableId, excel) {
    const table = document.getElementById(tableId);

    table.querySelector("tbody").innerHTML = `<tr> </tr>`;
    ciclo_asig.addEventListener("change", function () {
      table.querySelector("tbody").innerHTML = `<tr> </tr>`;
      for (let index = 0; index < excel.rows().count(); index++) {
        let row = excel.rows().get(index);

        table.querySelector("tbody").innerHTML += `
                <tr>
                    <td> ${index + 1} </td>
                    <td class ="id" >${row.id()}</td>
                    <td class ="genero" >${row.asignatura()}</td>
                    <td class ="nombre" >${row.semestre()}</td>
                    <td class ="plaza" >${row.grupo()}</td>
                    <td class ="sindicato" >${row.subgrupo()}</td>
                    <td class ="hd_contrato" >${row.hrs_t()}</td>
                    <td class ="ht_contrato" >${row.hrs_p()}</td>
                    <td>${row.plan_est()}</td>
                    <td class ="ht_contrato" >${row.nombre_titu()}</td>
                </tr>
                `;
      }
      depe.addEventListener("change", function () {
        table.querySelector("tbody").innerHTML = `<tr> </tr>`;
        for (let index = 0; index < excel.rows().count(); index++) {
          let row = excel.rows().get(index);

          table.querySelector("tbody").innerHTML += `
                    <tr>
                        <td> ${index + 1} </td>
                        <td class ="id" >${row.id()}</td>
                        <td class ="genero" >${row.asignatura()}</td>
                        <td class ="nombre" >${row.semestre()}</td>
                        <td class ="plaza" >${row.grupo()}</td>
                        <td class ="sindicato" >${row.subgrupo()}</td>
                        <td class ="hd_contrato" >${row.hrs_t()}</td>
                        <td class ="ht_contrato" >${row.hrs_p()}</td>
                        <td>${row.plan_est()}</td>
                        <td class ="ht_contrato" >${row.nombre_titu()}</td>
                    </tr>
                    `;
        }
      });
    });
  }
}

var url = "../administrador/config/asig_excel.php";
const excelInput = document.getElementById("excel-input");
var ciclo_asig = document.getElementById("ciclo_esc");
var depe_asig = document.getElementById("depe_asig");

excelInput.addEventListener("change", async function () {
  const content = await readXlsxFile(excelInput.files[0]);
  const excel = new Excel(content);
  console.log(ExcelPrinter.print("tabla-excel", excel));
  var depe = document.getElementById("depe_asig").value;
  var ciclo = document.getElementById("ciclo_esc").value;
  console.log(depe);
  console.log(ciclo);

  ciclo_asig.addEventListener("change", async function () {
    var depe = document.getElementById("depe").value;
    var ciclo = document.getElementById("ciclo_esc").value;
    console.log(depe);
    console.log(ciclo);
    depe_asig.addEventListener("change", async function () {
      var depe = document.getElementById("depe").value;
      var ciclo = document.getElementById("ciclo_esc").value;
      console.log(depe);
      console.log(ciclo);
      document
      .getElementById("btn_guardar")
        .addEventListener("click", function () {
          for (let index = 0; index < excel.rows().count(); index++) {
            let row = excel.rows().get(index);
            var datosEnviar = JSON.stringify({
              id: row.id(),
              asignatura: row.asignatura(),
              semestre: row.semestre(),
              grupo: row.grupo(),
              subgrupo: row.subgrupo(),
              hrs_t: row.hrs_t(),
              hrs_p: row.hrs_p(),
              id_doc: row.id_doc(),
              plan_est: row.plan_est(),
              asig_depe: depe,
              asig_ciclo: ciclo,
              id_inte: row.id_inte(),
            });
            fetch(url + "?insertar=1", { method: "POST", body: datosEnviar })
              .then((respuesta) => respuesta.json())
              .then((datosRespuesta) => {
                console.log("Datos insertados: " + index);
                move();
              })
              .catch(console.log(datosEnviar));
          }
        });
      document
        .getElementById("btn_borrar")
        .addEventListener("click", function () {
          let confirmation = confirm("¿Desea eliminar las materias y a los docentes del ciclo" + ciclo + "?");
          if (confirmation) {

            for (let index = 0; index < excel.rows().count(); index++) {
              let row = excel.rows().get(index);
              var datosEnviar = JSON.stringify({
                id: row.id(),
                id_doc: row.id_doc(),
                id_inte: row.id_inte(),
                asig_depe: depe,
                asig_ciclo: ciclo,
              });
              fetch('../administrador/config/delete_excel.php' + "?borrar_mates=1", { method: "POST", body: datosEnviar })
                .then((respuesta) => respuesta.json())
                .then((datosRespuesta) => {
                  console.log("Materias borradas: " + index);
                  move();
                })
                .catch(console.log(datosEnviar));
            }
          }
        });
    });
  });
});
function move() {
  var elem = document.getElementById("progress-bar");
  var width = 0;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + "%";
      document.getElementById("progress-bar").innerHTML = width * 1 + "%";
    }
  }
}

$(document).ready(function () {
  recargarlista();

  $("#ciclo_esc").change(function () {
    recargarlista();
  });
});

function recargarlista() {
  $.ajax({
    url: "../administrador/config/select/depe_cicloesc.php",
    type: "POST",
    data: "ciclo=" + $("#ciclo_esc").val(),
    success: function (r) {
      $("#depe_asig").html(r);
    },
    error: function (status) {
      console.log(status);
    },
  });
}

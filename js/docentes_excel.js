class Excel {
    constructor(content) {
        this.content = content
    }

    header() {
        return this.content[1]
    }
    rows() {
        return new RowCollection(this.content.slice(2, this.content.length))
    }
}

class RowCollection {
    constructor(rows) {
        this.rows = rows
    }

    first() {
        return new Row(this.rows[0])
    }
    get(index) {
        return new Row(this.rows[index])
    }
    count() {
        return this.rows.length
    }
}

class Row {
    constructor(row) {
        this.row = row
    }

    id() {
        return this.row[0]
    }
    genero() {
        return this.row[2]
    }
    nombre() {
        return this.row[3]
    }
    apellido() {
        return this.row[4]
    }
    plaza() {
        return this.row[5]
    }
    sindicato() {
        return this.row[7]
    }
    categoria() {
        return this.row[8]
    }
    hd_contrato() {
        return this.row[9]
    }
    ht_contrato() {
        return this.row[10]
    }
    hd() {
        return this.row[15]
    }
    ht() {
        return this.row[16]
    }
    htd() {
        return this.row[17]
    }
    hi() {
        return this.row[18]
    }
    grado_acad() {
        return this.row[24]
    }
}

class ExcelPrinter {

    static print(tableId, excel) {

        const table = document.getElementById(tableId)

        // var doc_ciclo = document.getElementById('doc_ciclo');
        // var doc_depe = document.getElementById('doc_depe');
        table.querySelector("tbody").innerHTML = `<tr> </tr>`;

        doc_ciclo.addEventListener('change', function () {
            table.querySelector("tbody").innerHTML = `<tr> </tr>`
            for (let index = 0; index < excel.rows().count(); index++) {
                let row = excel.rows().get(index);
                var depe = document.getElementById('doc_depe').value;
                var ciclo = document.getElementById('doc_ciclo').value;
                table.querySelector("tbody").innerHTML += `
                <tr>
                    <td> ${(index + 1)} </td>
                    <td class ="id" >${row.id()}</td>
                    <td class ="genero" >${row.genero()}</td>
                    <td class ="nombre" >${row.nombre() + " " + row.apellido()} </td>
                    <td class ="plaza" >${row.plaza()}</td>
                    <td class ="sindicato" >${row.sindicato()}</td>
                    <td class ="hd_contrato" >${row.hd_contrato()}</td>
                    <td class ="ht_contrato" >${row.ht_contrato()}</td>
                    <td class ="categoria" >${row.categoria()}</td>
                    <td class ="hd" >${row.hd()}</td>
                    <td class ="ht" >${row.ht()}</td>
                    <td class ="htd" >${row.htd()}</td>
                    <td class ="hi" >${row.hi()}</td>
                    <td class ="grado_acad" >${row.grado_acad()}</td>
                </tr>
                `
            }
            doc_depe.addEventListener('change', function () {
                table.querySelector("tbody").innerHTML = `<tr> </tr>`
                for (let index = 0; index < excel.rows().count(); index++) {
                    let row = excel.rows().get(index);
                    var depe = document.getElementById('doc_depe').value;
                    var ciclo = document.getElementById('doc_ciclo').value;
                    table.querySelector("tbody").innerHTML += `
                    <tr>
                        <td> ${(index + 1)} </td>
                        <td class ="id" >${row.id()}</td>
                        <td class ="genero" >${row.genero()}</td>
                        <td class ="nombre" >${row.nombre() + " " + row.apellido()}</td>
                        <td class ="plaza" >${row.plaza()}</td>
                        <td class ="sindicato" >${row.sindicato()}</td>
                        <td class ="hd_contrato" >${row.hd_contrato()}</td>
                        <td class ="ht_contrato" >${row.ht_contrato()}</td>
                        <td class ="categoria" >${row.categoria()}</td>
                        <td class ="hd" >${row.hd()}</td>
                        <td class ="ht" >${row.ht()}</td>
                        <td class ="htd" >${row.htd()}</td>
                        <td class ="hi" >${row.hi()}</td>
                        <td class ="grado_acad" >${row.grado_acad()}</td>
                    </tr>
                    `
                }
            })
        })

    }
}

// var url = location.pathname + "administrador/config/docentes.php";

var url = "../administrador/config/docentes_excel.php";

const excelInput = document.getElementById('excel-input')
var ciclo = document.getElementById('doc_ciclo');
var depe = document.getElementById('doc_depe');
excelInput.addEventListener('change', async function () {

    const content = await readXlsxFile(excelInput.files[0])
    const excel = new Excel(content)
    console.log(ExcelPrinter.print('tabla-excel', excel)) //pinta en la tabla los valores encontrado en los índices



    var doc_depe = document.getElementById('doc_depe').value;
    var doc_ciclo = document.getElementById('doc_ciclo').value;
    console.log(doc_ciclo);
    console.log(doc_depe);

    ciclo.addEventListener('change', async function () {
        var doc_depe = document.getElementById('depe').value;
        var doc_ciclo = document.getElementById('doc_ciclo').value;
        console.log(doc_ciclo);
        console.log(doc_depe);

        depe.addEventListener('change', async function () {
            var doc_depe = document.getElementById('depe').value;
            var doc_ciclo = document.getElementById('doc_ciclo').value;
            console.log(doc_ciclo);
            console.log(doc_depe);

            document.getElementById("btn_guardar").addEventListener("click", function () {
                for (let index = 0; index < excel.rows().count(); index++) {
                    let row = excel.rows().get(index);

                    var datosEnviar = JSON.stringify({ id: row.id(), genero: row.genero(), nombre: row.nombre(), apellido: row.apellido(), plaza: row.plaza(), sindicato: row.sindicato(), hd_contrato: row.hd_contrato(), ht_contrato: row.ht_contrato(), categoria: row.categoria(), hd: row.hd(), ht: row.ht(), htd: row.htd(), hi: row.hi(), grado_acad: row.grado_acad(), doc_ciclo: doc_ciclo, doc_depe: doc_depe });
                    fetch(url + "?insertar=1", { method: "POST", body: datosEnviar })
                        .then(respuesta => respuesta.json())
                        .then((datosRespuesta) => {
                            console.log(datosRespuesta);
                            console.log("Datos insertados: " + index);
                            move();
                        })
                        .catch(console.log(datosEnviar))
                }

            });
            document.getElementById("btn_borrar").addEventListener("click", function () {
                for (let index = 0; index < excel.rows().count(); index++) {
                    let row = excel.rows().get(index);

                    var datosEnviar = JSON.stringify({ id: row.id(), genero: row.genero(), nombre: row.nombre(), apellido: row.apellido(), plaza: row.plaza(), sindicato: row.sindicato(), hd_contrato: row.hd_contrato(), ht_contrato: row.ht_contrato(), categoria: row.categoria(), hd: row.hd(), ht: row.ht(), htd: row.htd(), hi: row.hi(), grado_acad: row.grado_acad(), doc_ciclo: doc_ciclo, doc_depe: doc_depe });
                    fetch(url + "?borrar=1", { method: "POST", body: datosEnviar })
                        .then(respuesta => respuesta.json())
                        .then((datosRespuesta) => {
                            console.log(datosRespuesta);
                            console.log("Datos borrados: " + index);
                            move();
                        })
                        .catch(console.log(datosEnviar))
                }

            });
            function move() {
                var elem = document.getElementById("progress-bar");
                var width = 0;
                var id = setInterval(frame, 10);
                function frame() {
                    if (width >= 100) {
                        clearInterval(id);
                    } else {
                        width++;
                        elem.style.width = width + '%';
                        document.getElementById("progress-bar").innerHTML = width * 1 + '%';
                    }
                }
            }



        })
    })

})





$(document).ready(function () {
    recargarlista();

    $('#doc_ciclo').change(function () {
        recargarlista();
    });
});

function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#doc_ciclo').val(),
        success: function (r) {
            $('#doc_depe').html(r);
            // $('#depe').select2();
        },
        error: function (status) {
            console.log(status);
        }
    });
}


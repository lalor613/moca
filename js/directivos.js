var direc = [];
url = "../administrador/config/directivos.php";
function agregar_dir() {
    let nombre =    $('#nombre').val();
    let puesto =    $('#puesto').val();
    let f_ocupar =  $('#f_ocupar').val();
    let titulo =    $('#tit').val();
    let genero =    $('#genero').val();
    let clave_dir = $('#clave_dir').val();
    document.getElementById('btn_agregar').innerHTML = 'Agregando...';
    console.log(titulo);
    let datosEnviar = JSON.stringify({ 
        nombre: nombre, puesto: puesto, f_ocupar: f_ocupar, titulo: titulo, genero: genero, clave_dir: clave_dir 
    });
    $.ajax({
        url: url + `?insertar=1`,
        type: "POST",
        data: datosEnviar,
        dataType: 'json',
        success: function (status,data) {
            alert("Creación exitosa");
            document.getElementById('btn_agregar').innerHTML = 'Agregar';
            obtenerDirectivos();
            $('#modal_dir').modal('hide');
        },
        error: function (data, status) {
            alert("Registro erróneo");
            console.log(status);
            console.log(data);
            document.getElementById('btn_agregar').innerHTML = 'Agregar';
        }
    });
}

function obtenerDirectivos() {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data, status) {
            direc = data;
            llenarTablaDirec();
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })
}
obtenerDirectivos();

function borrarDirec(indice){
    let confirmation = confirm("¿Desea eliminar al directivo?");
    if(confirmation){
        $.ajax({
            url:url + `?id_del=${indice}`,
            type:'DELETE',
            dataType:'json',
            success: function (data, status){
                alert("Directivo eliminado");
                direc = data;
                obtenerDirectivos();
            }
        })
    }else{
        alert("Acción cancelada");
    }
}


function llenarTablaDirec() {
    document.querySelector('#tabla_dir tbody').innerHTML = '';
    for (let i = 0; i < direc.length; i++) {
        document.querySelector('#tabla_dir tbody').innerHTML +=
            `
        <tr>
        <td>${i + 1}</td>  
        <td>${direc[i].puesto}</td>
        <td>${direc[i].nombre}</td>
        <td>${direc[i].direccion}</td>
        <td>
            <button type="button" onclick="seldirec(${direc[i].id_dir})" class="btn btn-outline-light">
                Editar
            </button>
        </td>
        <td>
        <button type="button" onclick="borrarDirec(${direc[i].id_dir})" class="btn btn-outline-danger">
           Borrar
        </button>
    </td>
    </tr>
        `
    }
}

var dir = [];
function seldirec(indice) {
    $('#up_id').val(indice);
    var id = JSON.stringify(indice);
    $.post(url, { id: id }, function (data, status) {
        var dir = data;
        $('#upnombre').val(dir.nombre);
        $('#uppuesto').val(dir.puesto);
        $('#upf_ocupar').val(dir.f_ocupar);
        $('#uptitulo').val(dir.titulo);
        $('#upgenero').val(dir.genero);
        $('#upclavedir').val(dir.clave_dir);
    }).fail(function (data, status) {
        console.log(data);
        console.log(status);
    })
    $('#modal_editarDir').modal('show');
}
function actualizarDir() {
    let id = $('#up_id').val();
    let nombre = $('#upnombre').val();
    let puesto = $('#uppuesto').val();
    let f_ocupar = $('#upf_ocupar').val();
    let titulo = $('#uptitulo').val();
    let genero = $('#upgenero').val();

    $.post(url,{
        id_update: id,
        nombre: nombre,
        puesto: puesto,
        f_ocupar: f_ocupar,
        titulo: titulo,
        genero: genero,
    }, function(data, status){
        console.log(status);
        console.log(titulo)
        obtenerDirectivos();
    })
    .fail(function(data, status){
        alert("error");
        console.log(data);
        console.log(status);
    })
    .always(function(){
        $('#modal_editarDir').modal('hide');
    })
}
/////////
var url2 = "../administrador/config/funcionarios.php";
function agregar_fun() {
    let nombre = $('#nombre1').val();
    let direccion1 = $('#direccion1').val();
    let puesto1 = $('#puesto1').val();
    let f_nom = $('#f_nom').val();
    document.getElementById('btn_guardar_fun').innerHTML = 'Agregando...';
    datosEnviar = JSON.stringify({ nombre: nombre, direccion: direccion1, puesto: puesto1, f_nom: f_nom })

    $.ajax({
        url: url2,
        type: 'POST',
        data: datosEnviar,
        dataType: 'json',
        success: function (data, status) {
            console.log(data);
            console.log(status);
            alert("Creación exitosa");
            document.getElementById('btn_guardar_fun').innerHTML = 'Agregar';
        },
        error: function (status) {
            alert("Registro erróneo");
            console.log(status);
            console.log(datosEnviar)
        }
    });
}
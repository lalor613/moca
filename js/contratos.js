

$(document).ready(function () {
    recargarlista();

    $('#ciclo_esc').change(function () {
        recargarlista();
    });

    $('#btn_faculty').hide();

});

//HACEL EL SELECT DE LAS DEPENDENCIAS
function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#doc_depe').html(r);
            $('#depe').select2();
            depe_user();
        },
        error: function (status) {
            console.log(status);
        }
    });
}

//Hace el select de las descargas por ciclo y depe
function lista_desc(){
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    $.ajax({
        url: '../administrador/config/select/asig_descarga.php',
        type: 'POST',
        data: `ciclo=${ciclo}&depe=${depe}`,
        success: function (data, status) {
            $('#descarga').html(data);
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })
}

//HACE LA BUSQUEDA DE LOS DOCENTES PARA
//DESPUES HACER LA IMPRESION DE LOS CONTRATOS
var contratos = [];
url = '../administrador/config/contratos.php'
function listaContratos(){

    let ciclo = $('#ciclo_esc').val();
    let depe = $('#depe').val();
    let datosEnviar = JSON.stringify({ciclo:ciclo, depe:depe});
    $.ajax({
        url:url + `?obtener=1`,
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success: function(data, status){
            contratos=data;
            LlenarTablaContrato();
            $('#btn_faculty').show();
            document.getElementById('btn_faculty').setAttribute('href', 'http://'+ window.location.hostname+'/moca/administrador/config/ver_contrato_facu.php?period='+ciclo+'&faculty='+depe)
            // console.log(window.location.hostname);
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    })
}


function LlenarTablaContrato(){
    document.querySelector('#tabla_docente tbody').innerHTML = '<tr> </tr>';
    for (let i = 0; i < contratos.length; i++) {
        let titular ='';
        if(contratos[i].c_tipo == 2){
            titular = `
            <button id='btn_titular' type="button" onclick="desc_asig(${contratos[i].id_cont},${contratos[i].c_doc},${contratos[i].c_depe},${contratos[i].c_ciclo}, ${contratos[i].c_tipo},'${contratos[i].nombre}','${contratos[i].apellido}')" class="btn btn-outline-dark">
                Titular
            </button>
            `;
        }
    document.querySelector('#tabla_docente tbody').innerHTML += `
    <tr>
        <td style='visibility:collapse; display:none;'> ${contratos[i].id_cont} </td>
        <td>${i+1} </td>
        <td> ${contratos[i].nombre + " " + contratos[i].apellido} </td>
        <td> ${contratos[i].nombre_tipo}</td>
        <td>
            `+titular+`
        </td>
        <td> 
            <div class="form-group">     
                <a class='btn btn-danger' href='http://${window.location.hostname}/moca/administrador/config/ver_combinado.php?period=${contratos[i].c_ciclo}&faculty=${contratos[i].c_depe}&teacher=${contratos[i].c_doc}&type=${contratos[i].c_tipo}' target='_blank'>PDF </a>
            </div>
        </td>
    </td>
    </tr>
    `
    };
}

{/* <form action='./reportes/reporte_${contratos[i].c_ciclo + "_" +contratos[i].c_tipo}a.php' method='POST' target='_blank'></form> */}
{/* <input type='hidden' name='id_cont' value='${contratos[i].id_cont}'>
                    <input type='hidden' name='docente_ciclo' value='${contratos[i].c_ciclo}'>
                    <input type='hidden' name='docente_dep' value='${contratos[i].c_depe}'>
                    <input type='hidden' name='docente_id' value='${contratos[i].c_doc}'>
                    <input type='hidden' name='tipo' value='${contratos[i].c_tipo}'></input> */}
// function condicion_titular(){
//     for (let i = 0; i< contratos.length; i++){
//         if(contratos[i].c_tipo == 2){
//             console.log("Estoy imprimiendo esto: " + i + " Veces");
//         }else{
//             document.getElementById('btn_titular').style.display='none';
//         }
//     }
// }
//asignacion y vista de decargas

url_des = '../administrador/config/asig_descarga.php';
function desc_asig(indice, doc, depe,ciclo,tipo, nombre, apellido){
    $('#id_cont').val(indice);
    $('#id_doc').val(doc);
    $('#id_ciclo').val(ciclo);
    $('#id_depe').val(depe);
    $('#id_tipo').val(tipo);
    $('#nombre').val(nombre + ' ' + apellido);

    lista_desc(); //Muestra el select de las descargas disponibles en el boton TITULAR
    obtener_descargas(indice);

    $('#modal_asigdes').modal('show');
}

function asignar_descarga(){

    let cont = $('#id_cont').val();
    let desc = $('#desc').val();


    let id_doc = $('#id_doc').val();
    let id_ciclo = $('#id_ciclo').val();
    let id_depe = $('#id_depe').val();
    let id_tipo = $('#id_tipo').val();



    document.getElementById('btn_asig').innerHTML= 'Asignando...';

    let datosEnviar = JSON.stringify({
        id_desc:desc,
        id_cont:cont,
        id_doc:id_doc,
        id_ciclo: id_ciclo,
        id_depe:id_depe,
        id_tipo:id_tipo
    });
    $.ajax({
        url: url_des + `?insertar=1`,
        type:"POST",
        data: datosEnviar,
        dataType:'json',
        success: function(status){
            swal.fire({
                'title' : 'Se asignó correctamente',
                'type' : 'success',
                'icon' : 'success'
            })
            // alert("Se asignó correctamente");
            document.getElementById('btn_asig').innerHTML = 'Asignar';
            //funcion para recargar la(s) descargas hechas
            obtener_descargas(cont);
        },
        error:function(data,status){
            console.log(data);
            console.log(status);
            swal.fire({
                'title': 'No se pudo asignar',
                'type': 'danger',
                'icon': 'error',
            })
            // alert("Esta descarga no se puede asignar");
            document.getElementById('btn_asig').innerHTML = 'Asignar';
        }
    })


}

let descargas = [];
function obtener_descargas(indice){
    let id_cont = indice

    let datosEnviar = JSON.stringify({
        id_cont:id_cont
    })

    $.ajax({
        url:url_des +`?desc_asig=1`,
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success: function(data, status){
            descargas = data;
            llenarTablaDescargas();
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });

}

function borrar_descarga(indice1, indice2){
    // console.log("licencia "+indice1);
    // console.log("contrato " +indice2);

    document.getElementById('btn_elimdesc').innerHTML = 'Eliminando...';

    let id_desc = indice1;
    let id_cont =  indice2;

    let datosEnviar = JSON.stringify({
        id_desc:id_desc,
        id_cont:id_cont
    });

    let confirmation = confirm("¿Eliminar la descarga?");

    if(confirmation){
        $.ajax({
            url: url_des + `?delete_asig=1`,
            type:'POST',
            data: datosEnviar,
            dataType:'json',
            success: function(){
               obtener_descargas(id_cont);
                document.getElementById('btn_elimdesc').innerHTML='Eliminar';
            },
            error: function(data,status){
                document.getElementById('btn_elimdesc').innerHTML='Eliminar';
                console.log(data);
                console.log(status);
            }
        })
    } else{
        alert("Acción cancelada");
    }

}

function llenarTablaDescargas(){
    document.querySelector('#tabla_descargasasig tbody').innerHTML = '';
    for (let i =0;i<descargas.length;i++){
        document.querySelector('#tabla_descargasasig tbody'). innerHTML +=
        `
        <tr>
            <td> ${i+1}</td>
            <td> ${descargas[i].nombre + " " + descargas[i].apellido}</td>
            <td> ${descargas[i].nombre_motivo}</td>
            <td>
                <button type="button" id="btn_elimdesc" onclick="borrar_descarga(${descargas[i].id_descarga},${descargas[i].id_contrato})" class="btn btn-outline-danger">
                 Eliminar
                </button>
            </td>
        </tr>
        `
    }
}

function depe_user(){
    let depe_user = $('#depe_user').val();
    $('#depe').val(depe_user);
}
depe_user();

function tipo_user(){
    let tipo_user = $('#tipo_user').val();
    let depe_cargo = $('#nom_depe').val();

    if(tipo_user == 2){
        document.getElementById('doc_depe').style.display = 'none';
        document.getElementById('direccion').innerHTML = depe_cargo;
    }
}
tipo_user();
$(document).ready(function () {
    recargarlista();

    $('#ciclo_esc').change(function () {
        recargarlista();
    });


    // $('#doc_depe').change(function () {
    //     listadocentes();
    //     MateriasSinCargar();
    // })

    $('#docente').change(function () {
        MateriasCargadas();
        
        var doc = $('#doc').val();
        var depe = $('#depe').val();
        var ciclo = $('#ciclo_esc').val();

        // console.log(doc,depe,ciclo);
        colocar_carga(doc, ciclo, depe)

    })
    $('#tipo').change(function () {
        MateriasCargadas();
    })
});
function general() {
    listadocentes();
    MateriasSinCargar();
}
//HACEL EL SELEC DE LAS DEPENDENCIAS
function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#doc_depe').html(r);
            $('#depe').select2();
            depe_user();
        },
        error: function (status) {
            console.log(status);
        }
    });
}


//HACE EL SELECT DE LOS DOCENTES ADSCRITOS A ESA DEPENDENCIA 
//EN ESE CICLO ESCOLAR
function listadocentes() {

    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    $.ajax({
        url: '../administrador/config/select/asig_depeciclo.php',
        type: 'POST',
        data: `ciclo=${ciclo}&depe=${depe}`,
        success: function (data, status) {
            $('#docente').html(data);
            $('#doc').select2();
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })
}


//ME TRAE LAS MATERIAS (CICLO - DEP) QUE NO HAN SIDO CARGADAS
var mate = [];
function MateriasSinCargar() {

    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();


    $.ajax({
        url: '../administrador/config/select/mate_depeciclo.php' + `?ciclo=${ciclo}&depe=${depe}`,
        type: 'GET',
        dataType: 'json',
        success: function (data, status) {
            mate = data;
            llenarTablaMaterias();

        },
        error: function (data, status) {

            console.log(status);
            console.log(data);
        }
    });
}


function llenarTablaMaterias() {
    document.querySelector('#tabla_materias tbody').innerHTML = '<tr> </tr>';
    for (let i = 0; i < mate.length; i++) {
        let user = $('#tipo_user').val();
        let boton = '';
        if (user == 1) {
            boton = `
            <button id='btn_añadir' type="button" onclick="añadir(${mate[i].id})" class="btn btn-outline-success btn-sm btn-block">
            <span class="material-symbols-outlined">
            done
            </span>
            </button>
                    `;
        }
        document.querySelector('#tabla_materias tbody').innerHTML +=
            `
        <tr class='justify-content-center align-items-center'>
            <td>
                ${i + 1}
                `+ boton + `
                
            </td>
            <td>
                ${mate[i].asignatura}
            </td>
            <td>
                ${mate[i].semestre}
            </td>
            <td>
                ${mate[i].grupo}
            </td>
        
            <td>
                ${mate[i].total}
            </td>                  
        </tr>
        `
    }
}



//ME TRAE LAS MATERIAS ASIGNADAS AL DOCENTE SELECCIONADO
var docente = document.getElementById('docente');
var mate_asig = [];
function MateriasCargadas() {
    //BORRAR Y CREAR VISTA vw_doc_mate
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    var tipo = document.querySelector('[name=tipo]:checked').value;
    var doc = $('#doc').val();
    $.ajax({
        url: '../administrador/config/mate_asig_doc.php' + `?ciclo=${ciclo}&depe=${depe}&doc=${doc}&tipo=${tipo}`,
        type: 'POST',
        dataType: 'json',
        success: function (data, status) {
            mate_asig = data;
            llenarTablaMateAsig();
        },
        error: function (data, status) {
            console.log(status);
            console.log(data);
        }
    })



}

function llenarTablaMateAsig() {
    document.querySelector('#tabla_materias_asig tbody').innerHTML = '';
    for (let i = 0; i < mate_asig.length; i++) {
        let user = $('#tipo_user').val();
        let boton = '';
        let edit = '';
        if (user == 1) {
            boton = `
            <button id='btn_retirar' type="button" onclick="retirar(${mate_asig[i].id})" class="btn btn-outline-danger btn-sm btn-block">
                <span class="material-symbols-outlined">
                    delete
                </span>
            </button>`;
          
        }
        document.querySelector('#tabla_materias_asig tbody').innerHTML +=
            `
        <tr class='justify-content-center align-items-center'>
           <td>  
                ${i + 1}
                `+ boton + `     
           </td>
            <td>
                ${mate_asig[i].asignatura}
            </td>
            <td>
                ${mate_asig[i].semestre}
            </td>
            <td>
                ${mate_asig[i].grupo}
            </td>
            <td>
            <div class='row'>
                <div class='col-md-6'>
                    ${mate_asig[i].total}
                </div>
                <div class='col-md-6'>
                <button class='btn btn-sm btn-outline-dark' onclick='editarcarga(${mate_asig[i].id})'>
                <span class="material-symbols-outlined">
                edit
                </span>
                </button> 
            </div>
            </div>
            
            </td>              
        </tr>
        `
    }
}


function retirar(index) {
    var doc = $('#doc').val();
    const asig = index;
    var tipo = document.querySelector('[name=tipo]:checked').value;
    var datosEnviar = JSON.stringify({ doc: doc, id: asig, tipo: tipo })
    $.ajax({
        url: '../administrador/config/asig_excel.php',
        type: 'DELETE',
        data: datosEnviar,
        dataType: 'json',
        success: function (data, status) {
            console.log(data);
            console.log(status);
            MateriasSinCargar();
            MateriasCargadas();

        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })

}

function añadir(index) {
    var doc = $('#doc').val();
    const asig = index;
    var tipo = document.querySelector('[name=tipo]:checked').value;
    var datosEnviar = JSON.stringify({ doc: doc, id: asig, tipo: tipo });


    $.ajax({
        url: '../administrador/config/asig_excel.php',
        type: 'POST',
        data: datosEnviar,
        dataType: 'json',
        success: function () {
            MateriasSinCargar();
            MateriasCargadas();
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);

        }
    })
}

function crearContrato() {
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    var tipo = document.querySelector('[name=tipo]:checked').value;
    var doc = $('#doc').val();

    var datosEnviar = JSON.stringify({
        ciclo: ciclo,
        depe: depe,
        tipo: tipo,
        doc: doc
    })

    document.getElementById('btn_crear').innerHTML = "<div class='spinner-border text-light' role='status'></div>";

    $.ajax({
        url: '../administrador/config/contratos.php' + '?crear=1',
        type: 'POST',
        data: datosEnviar,
        dataType: 'json',
        success: function (data, status) {
            console.log(data);
            console.log(status);
            // alert("Contrato creado");
            swal.fire({
                'title': 'Contrato creado',
                'type': 'success',
                'icon': 'success',
            })
            document.getElementById('btn_crear').innerHTML ='Crear';
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
            // alert("No se pudo crear");
            swal.fire({
                'title': 'No se pudo crear',
                'type': 'danger',
                'icon': 'error',
                
            })
        }

    });
}

horas = [];
function editarcarga(indice){
    $('#id_mate').val(indice);
    let id_mate = JSON.stringify(indice);
    $.post('../administrador/config/mate_asig_doc.php', {id_mate:id_mate},
    function (data){
        let horas = data;
        document.querySelector('#asignatura').innerHTML = horas.asignatura;
        $('#hrs_t').val(horas.hrs_t);
        $('#hrs_p').val(horas.hrs_p);
        $('#total').val(horas.total);
    });

    $('#modal_editarmate').modal('show');
}

function modificarcarga(){
    var suma =0;
    let mate = $('#id_mate').val();
    var horas = $('#total').val();
    let hrs_t = $('#hrs_t').val();
    let hrs_p = $('#hrs_p').val();
    var suma = parseFloat(hrs_t) + parseFloat(hrs_p);

    if(horas <= suma){
        $.post('../administrador/config/mate_asig_doc.php',
        {
            id_update:mate,
            horas:horas
        },function(data, status){
            console.log(data);
            console.log(status);
            MateriasCargadas();
        })
        .fail(function(data, status){
            console.log(data);
            console.log(status);
        });
    }else{
        alert("Las horas colocadas son incorrectas");
        document.getElementById('total').value = null;
        $('#total').val(suma);
    }

}

function colocar_carga(id, ciclo, depe)
{
    $.post('../administrador/config/select/tipo_carga.php',
    {
        doc:id,
        ciclo: ciclo,
        depe:depe
    },
    function(data, status){
        console.log(data['hi'],data['htd']);
        llenar_horas(data['htd'],data['hi'])
        console.log(status);
    })
}

function llenar_horas(htd, hi)
{
    document.querySelector('#htd').innerHTML = htd;
    document.querySelector('#hi').innerHTML = hi;
}

function depe_user() {
    let depe_user = $('#depe_user').val();
    $('#depe').val(depe_user);
}
depe_user();

function tipo_user() {
    let tipo_user = $('#tipo_user').val();
    let depe_cargo = $('#nom_depe').val();

    if (tipo_user == 2) {
        document.getElementById('doc_depe').style.display = 'none';
        document.getElementById('direccion').innerHTML = depe_cargo;
    }
}
tipo_user();

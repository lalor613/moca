var url = '../administrador/config/docentes.php';
var doc = [];


function obtenerDocentes() {
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();

    $.ajax({
        url: url + `?doc_ciclo=${ciclo}&depe=${depe}`,
        type: 'GET',
        // data:datosEnviar,
        dataType: 'json',
        success: function (data) {
            doc = data;
            llenarTabla();
            
        },
        error: function (data, status) {
            console.log(status);
            console.log(data);

        }
    });
}

function llenarTabla() {
    document.querySelector('#tabla_docentes tbody').innerHTML = '';
    for (let i = 0; i < doc.length; i++) {
        document.querySelector('#tabla_docentes tbody').innerHTML +=
            `
    <tr>
        <td hidden>${doc[i].id}</td>
        <td>${doc[i].apellido+" "+doc[i].nombre}</td>
        <td>${doc[i].categoria}</td>
        <td>${doc[i].plaza}</td>
        <td>${doc[i].hd}</td>
        <td>${doc[i].ht}</td>
        <td>${doc[i].htd}</td>
        <td>${doc[i].hi}</td>
        <td>
            <button type="button" onclick="seleccionar(${doc[i].id})" class="btn btn-outline-light">
                Editar
            </button>
        </td>
        <td>
            <button type="button" onclick="datosPersonales(${doc[i].id})" class="btn btn-outline-light">
                Editar
            </button>
        </td>
        <td> </td>
    </tr>
    `;
    }
}


function seleccionar(indice) {
    $('#upid').val(indice);
    var id = JSON.stringify(indice);
    $.post(url, { id: id }, function (data, status) {
        var doc = data;
        $('#upgen').val(doc.genero);
        $('#upnombre').val(doc.nombre+" "+doc.apellido);
        $('#upplaza').val(doc.plaza);
        $('#upsindicato').val(doc.sindicato);
        $('#uphdc').val(doc.hd_contrato);
        $('#uphtc').val(doc.ht_contrato);
        $('#categoria').val(doc.categoria);
        $('#hd').val(doc.hd);
        $('#ht').val(doc.ht);
        $('#htd').val(doc.htd);
        $('#hi').val(doc.hi);
        $('#grado_acad').val(doc.grado_acad);
        $('#cedula').val(doc.cedula);
        $('#otorgado').val(doc.otorgado);
        $('#f_otor').val(doc.f_otor);
    });
    $('#modal_edit_per').modal('show');

}

function actualizar() {
    let id = $('#upid').val();
    let genero = $('#upgen').val();
    let htd = $('#htd').val();
    let hi = $('#hi').val();
    let grado_acad = $('#grado_acad').val();
    let cedula = $('#cedula').val();
    let otorgado = $('#otorgado').val();
    let f_otor = $('#f_otor').val();
    //falta el valor dei id
    $.post(url, {
        id_update: id,
        genero: genero,
        htd: htd,
        hi: hi,
        grado_acad: grado_acad,
        cedula: cedula,
        otorgado:otorgado,
        f_otor:f_otor,
    }, function (data, status) {

        console.log(data);
        console.log(status);
        obtenerDocentes();
    })
        .fail(function (data, status) {
            alert("No se actualizaron los datos");
            console.log(status);
            console.log(data);
        })
        .always(function () {
            $('#modal_edit_per').modal('hide');
        });
}

var url2 = "../administrador/config/datos_per.php";
function datosPersonales(indice) {
    $('#id_personal').val(indice);
    var id = JSON.stringify(indice);
    $.post(url2, { id: id }, function (data, status) {
        var doc = data;
        $('#curp').val(doc.curp);
        $('#edad').val(doc.edad);
        $('#nacionalidad').val(doc.nacionalidad);
        $('#cp').val(doc.cp);
        $('#colonia').val(doc.colonia);
        $('#calle').val(doc.calle);
        $('#ciudad').val(doc.ciudad);
    });
    $('#modal_docente_').modal('show');
}

function actualizarPers() {
    let id = $('#id_personal').val();
    let curp = $('#curp').val();
    let nacionalidad = $('#nacionalidad').val();
    let cp = $('#cp').val();
    let colonia = $('#colonia').val();
    let calle = $('#calle').val();
    let ciudad = $('#ciudad').val();

    $.post(url2, {
        id_update: id,
        curp: curp,
        nacionalidad: nacionalidad,
        cp: cp,
        colonia: colonia,
        calle: calle,
        ciudad: ciudad,
    }, function (data, status) {
        console.log(status);
        obtenerDocentes();
    })
        .fail(function (data, status) {
            console.log(data);
            console.log(status);
        })
        .always(function (data, status) {
            console.log(status);
            $('#modal_docente_').modal('hide');
        });
}

$(document).ready(function () {
    recargarlista();

    $('#ciclo_esc').change(function () {
        recargarlista();
    });

});

function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#doc_depe').html(r);
            $('#depe').select2();
            depe_user();
        },
        error: function (status) {
            console.log(status);
        }
    });
}
function depe_user(){
    let depe_user = $('#depe_user').val();
    console.log(depe_user);
    $('#depe').val(depe_user);
}
depe_user();

function tipo_user(){
    let tipo_user = $('#tipo_user').val();
    let depe_cargo = $('#nom_depe').val();

    if(tipo_user == 2){
        document.getElementById('doc_depe').style.display = 'none';
        document.getElementById('direccion').innerHTML = depe_cargo;
    }
}
tipo_user();

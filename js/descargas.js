var descarga = [];
url = '../administrador/config/descargas.php';

$(document).ready(function () {
    recargarlista();
    $('#ciclo_esc').change(function () {
        recargarlista();
        
    });

    $('#doc_depe').change(function() {
        listadocentes();
    });

    $('#depe').select2();
});

//HACEL EL SELEC DE LAS DEPENDENCIAS
function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#doc_depe').html(r);
            $('#depe').select2({
                dropdownParent:$('#modal_descarga')
            });
        },
        error: function (status) {
            console.log(status);
        }
    });
};

function listadocentes(){
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    $.ajax({
        url:'../administrador/config/select/doc_depeciclo.php',
        type:'POST',
        data:`ciclo=${ciclo}&depe=${depe}`,
        success:function(data, status){
            $('#docentes').html(data);
            $('#doc').select2({
                dropdownParent:$('#modal_descarga')
            });
        },
        error:function(data, status){
            console.log(data);
            console.log(status);
        }
    })
}

function obtenerdescargas() {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data, status) {
            descarga = data;
            llenarTablaDesc();
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    })
}
obtenerdescargas();

function llenarTablaDesc() {
    document.querySelector('#tabla_desc tbody').innerHTML = '';
    for (let i = 0; i < descarga.length; i++) {
        document.querySelector('#tabla_desc tbody').innerHTML +=
            `
        <tr>
        <td>${descarga[i].id_descciclo}</td>
        <td>${descarga[i].direccion}</td>
        <td>${descarga[i].nombre + " " + descarga[i].apellido}</td>
        <td>${descarga[i].nombre_motivo}</td>
        <td>
            <button type="button" onclick="eliminarDesc(${descarga[i].id_desc})" class="btn btn-outline-danger">
                Borrar
            </button>
    </td>
    </tr>
        `
    }
}


function CrearDescarga(){
    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    var doc = $('#doc').val();
    var moti = $('#motivo').val();

    var datosEnviar = JSON.stringify({
        depe:depe,
        ciclo:ciclo,
        doc:doc,
        moti:moti
    })
    $.ajax({
        url:url,
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success: function (data, status){
            obtenerdescargas();
            console.log(data);
            console.log(status);
            $('#modal_descarga').modal('hide');
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    });
   
}

function limpiar(){
    document.getElementById('ciclo_esc').value = null;
    document.getElementById('depe').value = null;
    document.getElementById('doc').value = null;
    document.getElementById('motivo').value = null;
}

function eliminarDesc(indice){
    let confirmation = confirm("¿Eliminar descarga?");
    if(confirmation){

        $.ajax({
            url:url+ `?id_del=${indice}`,
            type:'DELETE',
            dataType:'json',
            success:function(data, status){
                alert("Descarga eliminada");
                descarga=data;
                console.log(data);
                console.log(status);
                obtenerdescargas();
            },
            error: function(data, status){
                console.log(data);
                console.log(status);
            }
        })
    }else{
        alert("Acción cancelada");
    }
}
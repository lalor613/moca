url = '../administrador/config/usuarios.php';
user = [];
function agregar_usuario(){
    let nombre = $('#nombre_user').val();
    let correo = $('#correo').val();
    let contraseña = $('#contraseña').val();
    let numero = $('#numero').val();
    let depe  = $('#depe').val();

    let datosEnviar = JSON.stringify({
        nombre:nombre,
        correo:correo,
        contraseña:contraseña,
        numero:numero,
        depe:depe
    });

    document.getElementById('btn_adduser').innerHTML='Guardando...';

    $.ajax({
        url:url + `?insertar=1`,
        type:"POST",
        data:datosEnviar,
        dataType:'json',
        success:function(data, status){
            console.log(data);
            console.log(status);
            alert("Usuario creado");
            limpiar();
            document.getElementById('btn_adduser').innerHTML='Guardar';
            obtenerusuarios();
            $('#modal_usuario').modal('hide');
        },
        error:function(data, status){
            console.log(data);
            console.log(status);
            alert("No se puede crear");
            document.getElementById('btn_adduser').innerHTML='Guardar';
            obtenerusuarios();
        }
    })

}

function limpiar(){
    document.getElementById('nombre_user').value=null;
    document.getElementById('correo').value=null;
    document.getElementById('numero').value=null;
    document.getElementById('depe').value=null;
}

function obtenerusuarios(){
    $.ajax({
        url:url + `?obtener=1`,
        type:'POST',
        dataType:'json',
        success: function(data){
            setTimeout(()=>{
                user = data;
                llenarTablaUser();
            },2000);
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    })
}
obtenerusuarios();

function llenarTablaUser(){
    document.querySelector('#tabla_usuarios tbody').innerHTML='';
    for(let i = 0; i <user.length; i++)
    document.querySelector('#tabla_usuarios tbody').innerHTML += 
    `
    <tr>
        <td>${ i + 1}</td>
        <td>
            ${user[i].nombre_user} <br>
            <small>${user[i].correo} ${user[i].numero}  </small>
        </td>
        <td>${user[i].direccion}</td>
        <td>
        <button id='btn_deluser' type="button" onclick="borrarUser(${user[i].id_u})" class="btn btn-outline-danger">
           Borrar
        </button>
        </td>
    </tr>
    `
}

function borrarUser(indice){
    document.getElementById('btn_deluser').innerHTML='Borrando...';
    let confirmation = confirm("Eliminar Usuario");
    if(confirmation){
        $.ajax({
            url: url + `?id_del=${indice}`,
            type:'DELETE',
            dataType:'json',
            success: function(data, status){
                direct = data;
                obtenerusuarios();
                alert("Usuario Eliminado");
                document.getElementById('btn_deluser').innerHTML='Borrar';
            },
            error: function(data, status){
                console.log(data);
                console.log(status);
                console.log(indice);
                document.getElementById('btn_deluser').innerHTML='Borrar';
            }
        })
    }else{
        alert("Accion Cancelada");
        document.getElementById('btn_deluser').innerHTML='Borrar';
    }

}
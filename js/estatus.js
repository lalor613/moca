$(document).ready(function () {
    recargarlista();

    $('#ciclo_esc').change(function () {
        recargarlista();
    });

});

function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#doc_depe').html(r);
        },
        error: function (status) {
            console.log(status);
        }
    });
}
url = '../administrador/config/estatus.php';
$docentes = [];
function obtener_docentes(){

    var ciclo = $('#ciclo_esc').val();
    var depe = $('#depe').val();
    var datosEnviar = JSON.stringify({depe:depe, ciclo:ciclo});

    $.ajax({
        url:url+`?obtener=1`,
        type:'POST',
        data: datosEnviar,
        dataType:'json',
        success: function(data,status){
            console.log(data);
            console.log(status);
        },
        error: function(data, status){
            console.log(data);
            console.log(status)
        }
    })

}
materias = [];
$(document).ready(function () {
    recargarlista();
    $('#ciclo_esc').change(function () {
        recargarlista();
    });



});

function recargarlista() {
    $.ajax({
        url: '../administrador/config/select/depe_cicloesc.php',
        type: 'POST',
        data: "ciclo=" + $('#ciclo_esc').val(),
        success: function (r) {
            $('#id_depe').html(r);
        },
        error: function (status) {
            console.log(status);
        }
    });
}

function buscarMaterias() {

    // $('#Tabla_materias').LoadingOverlay("show",{image:"",text:"Cargando las Materias Vacantes..."})
    url = '../administrador/config/materias_vacantes.php';
    let ciclo = $('#ciclo_esc').val();
    let depe = $('#depe').val();
    data = JSON.stringify({ depe: depe, ciclo: ciclo });

    $.ajax({
        url: url + '?vacante=1',
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data, status) {
            console.log(status);
            materias = data;
            tabla_materiasvac();
        },
        error: function (data, status) {
            console.log(data)
            console.log(status);
        }
    });
};

function tabla_materiasvac() {
    document.querySelector('#Tabla_materias tbody').innerHTML = '';
    for (let i = 0; i < materias.length; i++) {
        document.querySelector('#Tabla_materias tbody').innerHTML +=
            `
        <tr>
            <td>
            <label class='btn bg-transparent row'>
            <div class='row'>
                    <div class='col-sm-5'>
                        ${i + 1}
                    </div>
                    <div class='col-sm-6'>
                            <input type='checkbox' name='materias[]' value='${materias[i].id}'>
                    </div>
                    </div>
                </label>
               
            </td>
            <td>
                <div class='row'>
                    <div class='col-md-12'>
                        ${materias[i].asignatura}
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-12'>
                        <strong>
                            <small>
                            ${materias[i].plan_est}
                            </small>
                        </strong>
                    </div>
                </div>
            </td>
            <td>${materias[i].semestre}</td>
            <td>${materias[i].grupo}</td>
            <td>${materias[i].total}</td>               
        </tr>
        `
    }
}

variable = [];
function mover() {

        //obtenemos datos.
        // var mate =JSON.stringify({materias: $('[name="materias[]"]').serialize()});
        // 
    // let ciclo = $('#ciclo_esc').val();
    let mates= $('[name="materias[]"]').serializeArray();
    // console.log(mates[0]['value']);
    // console.log(mates[0]['name']);
    // console.log(mates);
    //envía uno por uno, pero solo se queda el último; hacer un ciclo for (post) para cada uno de los que aparecen
    $.ajax({
        type:'POST',
        url: '../administrador/config/materias_vacantes.php' + '?cambio=1',
        data:  mates,
        dataType: 'json',
        success: function(data, status){
            console.log(data);
            console.log(status);
        },
        error: function(data, status){
            console.log(data);
            console.log(status);
        }
    })

    // for (let i = 0; i<mates.length; i++){
        // $.post(url, {})
    // }
    
    document.querySelector('#numates').innerHTML = mates.length;

    $('#modal_materias').modal('show');
    
}
// $.ajax({
    //     type: 'POST',
    //     url: '../administrador/config/materias_vacantes.php' + '?cambio=1',
    //     data: $('[name="materias[]"]').serializeArray(),
    //     dataType:'json',
    //     success: function (data, status) {
    //         console.log(status);
    //         console.log(data);
    //     },
    //     error: function(data, status){
    //         console.log(data);
    //         console.log(status);
    //     }
    // });
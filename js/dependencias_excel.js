
class Excel {
    constructor(content) {
        this.content = content
    }

    header() {
        return this.content[0]
    }
    rows() {
        return new RowCollection(this.content.slice(1, this.content.length))
    }
}

class RowCollection {
    constructor(rows) {
        this.rows = rows
    }

    first() {
        return new Row(this.rows[0])
    }
    get(index) {
        return new Row(this.rows[index])
    }
    count() {
        return this.rows.length
    }
}

class Row {
    constructor(row) {
        this.row = row
    }

    clave() {
        return this.row[0]
    }
    direccion() {
        return this.row[1]
    }
}

class ExcelPrinter {

    static print(tableId, excel) {
        const table = document.getElementById(tableId)

        table.querySelector("tbody").innerHTML = `<tr> </tr>`
        for (let index = 0; index < excel.rows().count(); index++) {
            let row = excel.rows().get(index);
            table.querySelector("tbody").innerHTML += `
                <tr>
                    <td class ="prog" >${index + 1}</td>
                    <td class ="direccion" >${row.direccion()}</td>
                    <td class ="clave" >${row.clave()}</td>
                </tr>
                `
        }


    }
}



var url = "../administrador/config/dependencias.php";

const excelInput = document.getElementById('excel-input')

excelInput.addEventListener('change', async function () {

    const content = await readXlsxFile(excelInput.files[0])
    const excel = new Excel(content)

    console.log(ExcelPrinter.print('tabla-excel', excel))

    this.agregar = function () {
        for (let index = 0; index < excel.rows().count(); index++) {
            let row = excel.rows().get(index);


            var datosEnviar = JSON.stringify({ clave: row.clave(), direccion: row.direccion()})
            fetch(url + "?insertar=1", { method: "POST", body: datosEnviar })
                .then(respuesta => respuesta.json())
                .then((datosRespuesta) => {
                    console.log("Datos insertados: " + index);
                })
                .catch(console.log)
        }
    }


})




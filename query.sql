create table ciclos_escolares(
	id_ciclo integer,
	descripcion varchar(255),
	descripcion_cortado varchar(255),
	
	primary key(id_ciclo)
);

create table dependencias(
	clave integer,
	direccion varchar(255),
	
	primary key(clave)
);

create table directivos(
	
	id_dir integer,
	nombre varchar(255),
	puesto varchar(255),
	f_ocupar date,
	titulo varchar(20),
	genero varchar(20),
	clave_dir integer,
	
	primary KEY(id_dir),
	constraint fk_dir_dep foreign key(clave_dir) references dependencias(clave),
	constraint uq_dir_dep UNIQUE (clave_dir)
);

create table apertura_ciclos(
	ac_depe integer,
	ac_ciclo integer,
	
	constraint uq_depe_ciclo UNIQUE(ac_depe,ac_ciclo),
	constraint fk_clave_depe foreign key(ac_depe) references dependencias(clave),
	constraint fk_id_ciclo foreign key(ac_ciclo) references ciclos_escolares(id_ciclo)
);

create table docentes(
	id integer,
	genero varchar(20),
	nombre varchar(255),
	plaza integer,
	sindicato varchar(50),
	hd_contrato integer,
	ht_contrato integer,
	categoria varchar(100),
	hd integer,
	ht integer,
	htd NUMERIC,
	hi NUMERIC,
	grado_acad varchar(255),
	cedula integer,
	otorgado varchar(255),
	f_otor date,
	
	primary key(id)

);

create table d_per(
	d_id integer,
	curp varchar(15),
	edad integer,
	nacionalidad varchar(255),
	colonia varchar(255),
	cp integer,
	calle varchar(255),
	ciudad varchar(255),
	
	PRIMARY KEY(d_id),
	Constraint fk_id_doc foreign key (d_id) references docentes (id)
);

create table adscripcion(
	clave_depe integer,
	id_doc integer,
	idciclo integer,
	periodo_ini date,
	periodo_fin date,

	constraint fk_ads_depe foreign key (clave_depe) references dependencias(clave),
	constraint fk_ads_doc foreign key(id_doc) references docentes(id),
	constraint fk_ads_ciclo foreign key(idciclo) references ciclos_escolares(id_ciclo),
	constraint uq_depe_doc unique(clave_depe, idciclo, id_doc)
);

create table asignaturas(
	id integer,
	asignatura varchar(255),
	semestre integer,
	grupo char(10),
	subgrupo integer,
	hrs_t numeric,
	hrs_p numeric,
	plan_est varchar(255),

	total numeric,
	
	asig_depe integer,
	asig_ciclo integer,
	
	primary key(id),
	
	constraint fk_asig_depe foreign key(asig_depe) references dependencias (clave),
	constraint fk_asig_ciclo foreign key (asig_ciclo) references ciclos_escolares(id_ciclo),
	constraint uq_a_depe_ciclo UNIQUE (id, asig_depe, asig_ciclo)
);
create table tipo_contratos(
	id_tc integer,
	nombre_tipo varchar(100),
	
	primary key(id_tc)
);
create table carga_asignaturas(

	id_doc integer,
	id_asig INTEGER,
	id_tipocarga integer,
	constraint fk_ca_asig foreign key(id_asig) references asignaturas(id),
	constraint fk_ca_doc foreign key(id_doc) references docentes(id),
	constraint fk_tcon foreign key(id_tipocarga) references tipo_contratos(id_tc),
	constraint uq_doc_asig UNIQUE(id_doc, id_asig)

);
create table contratos(
	id integer,
	c_doc integer,
	c_depe integer,
	c_ciclo integer,
	c_tipo integer,
	
	
	PRIMARY KEY(id),
	constraint uq_contrato UNIQUE(c_doc, c_depe, c_ciclo, c_tipo),
	
	constraint fk_c_docente foreign key(c_doc) references docentes(id),
	constraint fk_c_dependencia foreign key(c_depe) references dependencias(clave),
	CONSTRAINT fk_c_ciclo foreign key (c_ciclo) references ciclos_escolares(id_ciclo),
	constraint fk_id_tipoc foreign key (c_tipo) references tipo_contratos(id_tc)
	
);

create table motivo_desc(
	id_motivo integer,
	nombre_motivo varchar(255),

	primary key(id_motivo)
);

create table descargas(
	id_desc integer,
	id_moti integer,

	id_descdoc integer,
	clave_descdepe integer,
	id_descciclo integer,
	
	primary key(id_desc),
	constraint uq_descarga UNIQUE(id_descdoc, clave_descdepe, id_descciclo),

	constraint fk_id_motivo foreign key (id_moti) references motivo_desc(id_motivo),
	constraint fk_id_docentedesc foreign key (id_descdoc) references docentes(id),
	constraint fk_id_depedesc foreign key (clave_descdepe) references dependencias(clave),
	constraint fk_id_ciclodesc foreign key (id_descciclo) references ciclos_escolares(id_ciclo)
);
--  alter table en pgadmin


create table asig_descarga(
	id_descarga integer,
	id_contrato integer,

	constraint uq_desccont unique(id_descarga, id_contrato),
	constraint fk_asig_desc foreign key (id_descarga) references descarga(id_desc),
	constraint fk_asig_cont foreign key (id_contrato) references contratos(id)

);

create table funcionarios(
	id_func integer,
	direccion varchar(255),
	nombre  varchar(150),
	puesto varchar(100),
	f_nom date, 
	
	primary KEY (id_func)
);

create table imagenes(
	id integer,
	nombre varchar(255),
	logo varchar(1000),
	
	primary key(id)
);

--TABLAS DE USUARIOS

Create table tipo_user(
	id_tu integer,
	nombre_tipo varchar(50),

	primary key(id_tu)
);

create table usuarios(
	id_u integer,
	nombre_user varchar(255),
	contraseña varchar(255),
	numero varchar(15),
	correo varchar(255),

	tipo_id integer,

	primary key(id_u),

	constraint fk_tipo_user foreign key (tipo_id) references tipo_user(id_tu),
	constraint uq_user unique(correo)	
);

create table user_asig(
	id_user integer,
	clave_user integer,

	constraint fk_usuario_asig foreign key (id_user) references usuarios(id_u),
	constraint fk_depe_user foreign key (clave_user) references dependencias(clave),
	constraint uq_depe unique(clave_user)
);

create table passwords(
	id_ps integer,
	email varchar(255),
	token varchar(200),
	codigo integer,
	fecha TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	primary key(id_ps)
);


-- INSERCIONES DE CAJÓN
INSERT INTO ciclos_escolares VALUES (20221, 'enero - julio 2022', 'enero - junio 2022');
INSERT INTO ciclos_escolares VALUES (20222, 'agosto - diciembre 2022', 'agosto - diciembre 2022');
INSERT INTO tipo_contratos VALUES (1, 'Tiempo Determinado');
INSERT INTO tipo_contratos VALUES (2, 'Tiempo Determinado para Cubrir Interinato');

insert into motivo_desc(nombre_motivo) values('para el disfrute de una beca para realizar estudios de posgrado otorgada por esta institución.');
insert into motivo_desc(nombre_motivo) values('para el disfurte de una beca para realizar estudios de posgrado otorgada por una institución distinta a esta universidad.');
insert into motivo_desc(nombre_motivo) values('para desempeñar un cargo administrativo o académico en otra institución educativa.');
insert into motivo_desc(nombre_motivo) values('por haber sido designado o electo, para desempeñar un cargo en la administración pública.');
insert into motivo_desc(nombre_motivo) values('para desempeñar funciones administrativas o académicas, dentro de la propia Universidad.');
insert into motivo_desc(nombre_motivo) values('por motivos personales.');
insert into motivo_desc(nombre_motivo) values('por recomendación jurídica.');
insert into motivo_desc(nombre_motivo) values('por ocupar el cargo de delegado Sindical del SPAUNACH.');

insert into tipo_user(id_tu, nombre_tipo) values( 1 ,'Administrador');
insert into tipo_user(id_tu, nombre_tipo) values( 2 ,'Secretario Académico');

-- VISTAS EN POSTGRESQL
create view vw_depe_cicloesc AS
	select d.clave,d.direccion, c.id_ciclo, c.descripcion,ac.ac_depe, ac.ac_ciclo
	FROM apertura_ciclos as ac
	inner join dependencias as d
		on d.clave = ac.ac_depe
	inner join ciclos_escolares as c
		on c.id_ciclo = ac.ac_ciclo;
	
create view vw_depe_dir AS
	select *
	from directivos as dr
	inner join dependencias as d
		on d.clave = dr.clave_dir;

create view vw_ads_doc as
select * from adscripcion as ads
inner join ciclos_escolares as ce
	on ce.id_ciclo = ads.idciclo
inner join dependencias as d
	on d.clave = ads.clave_depe
inner join docentes as doc
	on doc.id = ads.id_doc;

create view vw_ads_doc_cortado as
select * from adscripcion as ads
inner join docentes as doc
	on doc.id = ads.id_doc;

create view vw_doc_mate AS
select * from carga_asignaturas as ca
	inner join asignaturas as asig
	on ca.id_asig=asig.id;

-- CREATE view vw_doc_mate_completo AS
-- 	SELECT d.id, d.nombre, d.apellido,d.hd_contrato, d.ht_contrato, d.categoria, a.asignatura,

create view vw_desc_doc as
select --VISTA DE DESCARGA DOCENTE Y MOTIVO

create view vw_cont_doctipo AS
select c.id_cont, c.c_doc, c.c_depe, c.c_ciclo , c.c_tipo, d.id, d.nombre,d.apellido, tc.id_tc, tc.nombre_tipo, dep.clave, dep.direccion
	from contratos as c
	inner join docentes as d
		on d.id = c.c_doc
	inner join tipo_contratos as tc
		on c.c_tipo = tc.id_tc 
	inner join dependencias as dep
		on c.c_depe = dep.clave;
		
create view vw_desc_docmoti as
select dg.id_desc, dg.id_descdoc, dg.clave_descdepe, dg.id_descciclo, dg.id_moti, d.id, d.nombre,d.apellido, md.id_motivo, md.nombre_motivo, dep.clave,dep.direccion
	from descargas as dg
	inner join motivo_desc as md
		on md.id_motivo = dg.id_moti
	inner join docentes as d
		on d.id = dg.id_descdoc
	inner join dependencias as dep
		on dep.clave = dg.clave_descdepe;

create view vw_desc_asig as
select d.id, d.nombre,d.apellido,d.genero,d.plaza,d.categoria, dg.id_desc, dg.id_descdoc, dg.clave_descdepe, dg.id_descciclo, dg.id_moti, md.id_motivo, md.nombre_motivo, ad.id_descarga, ad.id_contrato, c.id_cont, c.c_doc, c.c_depe, c.c_ciclo, c.c_tipo
	from asig_descarga as ad
		inner join descargas as dg
			on ad.id_descarga = dg.id_desc
		inner join contratos as c
			on ad.id_contrato = c.id_cont
		inner join motivo_desc as md
			on md.id_motivo = dg.id_moti
		inner join docentes as d
			on d.id = dg.id_descdoc

CREATE VIEW view_usuarios_tipo as 
	select tu.id_tu, tu.nombre_tipo, u.id_u, u.nombre_user, u.numero ,u.correo, u.tipo_id, d.clave, d.direccion
		from user_asig as ua
			inner join usuarios as u
				on u.id_u = ua.id_user
			inner join dependencias as d
				on d.clave = ua.clave_user
			inner join tipo_user as tu
				on tu.id_tu = u.tipo_id
				
delete from user_asig;
delete from adscripcion;
delete from d_per;
delete from asig_descarga;
delete from directivos;
delete from carga_asignaturas:
delete from contratos;
delete from descargas;
delete from docentes;
delete from asignaturas;
delete from motivo_desc;
delete from usuarios where tipo_id=2;


/////

alter table docentes add column apellidos varchar(255);

drop view vw_ads_doc;

create view vw_ads_doc as
select * from adscripcion as ads
inner join ciclos_escolares as ce
	on ce.id_ciclo = ads.idciclo
inner join dependencias as d
	on d.clave = ads.clave_depe
inner join docentes as doc
	on doc.id = ads.id_doc;
	
drop view vw_ads_doc_cortado;
create view vw_ads_doc_cortado as
select * from adscripcion as ads
inner join docentes as doc
	on doc.id = ads.id_doc;

drop view vw_desc_docmoti;
create view vw_desc_docmoti as
select dg.id_desc, dg.id_descdoc, dg.clave_descdepe, dg.id_descciclo, dg.id_moti, d.id, d.nombre, d.apellido, md.id_motivo, md.nombre_motivo, dep.clave,dep.direccion
	from descargas as dg
	inner join motivo_desc as md
		on md.id_motivo = dg.id_moti
	inner join docentes as d
		on d.id = dg.id_descdoc
	inner join dependencias as dep
		on dep.clave = dg.clave_descdepe;
